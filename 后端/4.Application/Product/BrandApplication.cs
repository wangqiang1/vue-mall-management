﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Model;
using Wq.Core.PlatAdminService;
using Wq.Core.PlatAdminService.Models;
using Wq.Core.ProductService;
using Wq.Core.ProductService.Models;

namespace Wq.Core.Application.Product
{
    /// <summary>
    /// 商品分类 
    /// </summary>
    public class BrandApplication : BaseApplication
    {
        private readonly BrandService brandService;
        private readonly CategoryService categoryService;
        private readonly LogService logService;

        public BrandApplication(BrandService brandService, CategoryService categoryService, LogService logService)
        {
            this.brandService = brandService;
            this.categoryService = categoryService;
            this.logService = logService;
        }
        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        public MessageModel List(PageModel<BrandQuery> pageModel)
        {
            int count = 0;
            return SuccessResult(brandService.List(pageModel, ref count), count);
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="tag"></param>
        public MessageModel Save(BrandEditor brand)
        {
            Verify(brand);
            var old = brandService.GetById(brand.Id);
            var info = brand.Map<BrandInfo>();
            if (brand.Id > 0)
                brandService.Update(info);
            else
                brandService.Insert(info);
            Log(old, brand);
            return SuccessResult();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MessageModel Remove(long id)
        {
            var old = brandService.GetById(id);
            brandService.DeleteById(id);
            Log(old, isDelete: true);
            return SuccessResult();
        }

        /// <summary>
        /// 修改排序
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Sequence"></param>
        public MessageModel UpdateSequence(long id, long sequence)
        {
            brandService.UpdateSequence(id, sequence);
            return SuccessResult();
        }

        /// <summary>
        /// 日志
        /// </summary>
        /// <param name="oldBrand"></param>
        /// <param name="brand"></param>
        /// <param name="isDelete"></param>
        public void Log(BrandInfo oldBrand, BrandEditor brand = null, bool isDelete = false)
        {
            string msg = string.Empty;
            if (isDelete)
                msg = $"删除商品品牌【{oldBrand.Name}】";
            else
            {
                if (brand.Id == 0)
                    msg = $"新增商品品牌【{brand.Name}】,关联分类【{categoryService.GetName(brand.CategoryId)}】";
                else
                {
                    msg = $"修改商品品牌【{oldBrand.Name}】";
                    if (brand.Name != oldBrand.Name)
                        msg += $" 名称由【{oldBrand.Name}】变为【{brand.Name}】";
                    if(oldBrand.CategoryId != brand.CategoryId)
                        msg += $" 关联分类由【{categoryService.GetName(oldBrand.CategoryId)}】变为【{categoryService.GetName(brand.CategoryId)}】";
                }
            }
            logService.Create(LogModule.ProductBrand, msg);
        }

        /// <summary>
        /// 验证
        /// </summary>
        /// <param name="tag"></param>
        public void Verify(BrandEditor brand)
        {
            if (string.IsNullOrWhiteSpace(brand.Name))
                throw new MyException("品牌名称不能为空");
            if (brandService.Count(p => p.Name == brand.Name && p.Id != brand.Id) > 0)
                throw new MyException("已经存在了相同名称的品牌");
            if(brand.CategoryId == 0)
                throw new MyException("请选择商品分类");
            if(categoryService.Count(p => p.Id == brand.CategoryId) == 0)
                throw new MyException("商品分类不存在");
            if(string.IsNullOrWhiteSpace(brand.Icon))
                throw new MyException("请上传logo");
        }
    }
}
