import { isTemplate } from 'element-plus/lib/utils'
import store from '../store'
const type = {
  view: { key: '1001', label: '查看' },
  add: { key: '1002', label: '添加' },
  edit: { key: '1003', label: '编辑' },
  delete: { key: '1004', label: '删除' },
  administer: { key: '1005', label: '管理' },
}
 
const plugin = {
  auth(permission: any): boolean {
    const paths = store.state.currentPage.trim().replace('/', '').split('/') 
    let show = <boolean>(false)
    let permissions = <any>([])
    const menus = store.state.menus;
    if (menus && menus.length > 0) {
      menus.forEach((one: any) => {
        if (paths.length > 0) {
          if (paths[0] === one.path) {

            if (paths.length == 1) {
              permissions = one.permissions
            }
            else {
              one.children.forEach((two: any) => {
                if (paths[1] === two.path) {

                  if (paths.length == 2) {
                    permissions = two.permissions
                  }
                  else {
                    two.children.forEach((three: any) => {
                      if (paths[2] === three.path) {

                        permissions = three.permissions
                      }
                    })
                  }
                }
              })
            }
          }

        }
      })


      permissions.forEach((res: any) => {
        if (res.id === parseInt(permission)) show = true
      })
    }
    return show
  },
  type
}

export default plugin
