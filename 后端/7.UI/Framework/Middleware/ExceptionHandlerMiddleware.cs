﻿using System;
using System.Threading.Tasks;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Wq.Core.Extensions;

namespace Framework.Middleware
{
    /// <summary>
    /// 全局异常处理 中间件
    /// </summary>
    public class ExceptionHandlerMiddleware
    { 
        private static readonly ILog log = LogManager.GetLogger(typeof(ExceptionHandlerMiddleware));
        private readonly RequestDelegate next;
        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            this.next = next;
        }
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandlerAsync(context, ex);
            }
        }
        private async Task HandlerAsync(HttpContext context, Exception ex)
        {
            ex = GetInnerExcetion(ex);
            context.Items["Exception"] = ex;
            if (ex is UnAuthorizedException unAuthorized)
            {
                await WriteResult(context, StatusCodes.Status401Unauthorized, new
                {
                    success = false,
                    msg = unAuthorized.Message,
                    code = unAuthorized.ErrorCode
                });
            }
            else if (ex is AccessForbiddenException forbidden)
            {
                await WriteResult(context, StatusCodes.Status403Forbidden, new
                {
                    success = false,
                    msg = forbidden.Message,
                    code = forbidden.ErrorCode
                });
            }
            else if (ex is MyException exception)
            {
                await WriteResult(context, StatusCodes.Status417ExpectationFailed, new
                {
                    success = false,
                    msg = exception.Message,
                    code = exception.ErrorCode,
                    data = exception.ErrorData
                });
            }
            else
            {
                var igrone = false;
                if (ex is BadHttpRequestException)
                    igrone = true;

                if (!igrone)
                    log.Error( ex.Message);
                await WriteResult(context, StatusCodes.Status500InternalServerError, new
                {
                    success = false,
                    msg = "系统错误，请联系管理员",
                });
            }
        }

        private async Task WriteResult(HttpContext context, int statusCode, object result)
        {
            context.Response.Clear();
            context.Response.Headers["content-type"] = "application/json; charset=utf-8";
            context.Response.StatusCode = statusCode;
            var options = context.RequestServices.GetService<IOptions<MvcNewtonsoftJsonOptions>>().Value;
            await context.Response.WriteAsync(JsonConvert.SerializeObject(result, options.SerializerSettings));
        }

        private Exception GetInnerExcetion(Exception ex)
        {
            if (ex.InnerException != null)
                return GetInnerExcetion(ex.InnerException);
            return ex;
        }
    }
}
