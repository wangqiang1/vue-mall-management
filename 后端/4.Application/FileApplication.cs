﻿using System;
using System.IO;
using Wq.Core.Extensions;
using Wq.Core.Extensions.Assembly.Aliyun;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.FileService;
using Wq.Core.FileService.Models;
using Wq.Core.FileService.Models.DTO;
using Wq.Core.Model;

namespace Wq.Core.Application
{
    public class FileApplication : BaseApplication
    {
        private readonly OssFileService ossFileService;
        private readonly CategoryService categoryService;
        public FileApplication(OssFileService ossFileService, CategoryService categoryService)
        {
            this.ossFileService = ossFileService;
            this.categoryService = categoryService;
        }


        /// <summary>
        /// 获取分页列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public MessageModel List(PageModel<FileQuery> pageModel)
        {
            int count = 0;
            return SuccessResult(ossFileService.List(pageModel, ref  count), count);
        }

        /// <summary>
        /// 图片库 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public MessageModel UploadImage(UploadImageCommand command)
        {
            var fileName = command.File.FileName;
            if (!AllowExtensionByImage(fileName))
                throw new MyException("不支持的文件格式");
            string extension = Path.GetExtension(fileName).ToLower();
            var ossPath = OssManagement.File_Image(Guid.NewGuid().ToString("N"), extension);
            var stream = command.File.OpenReadStream();

            if (stream.Length > 0x300000)//3M
                throw new MyException("文件大小不能超过3M");

            var eTag = AliyunOSSHelper.PutObject(ossPath, stream);
            var editor = new FileEditor() {
                CategoryId =command.Category,
                Name = command.File.FileName,
                Type = FileType.Image,
                Size = stream.Length,
                ETag = eTag,
                SupplierId = 0,
                Path = ossPath
            };
            ossFileService.Create(editor);
            return SuccessResult(ossPath);
        }

        /// <summary>
        /// 是否为可用文件扩展名
        /// </summary>
        public bool AllowExtensionByImage(string fileName)
        {
            var extension = Path.GetExtension(fileName).ToLower();
            var allow = ".jpg|.gif|.png|.bmp|.jpeg|.webp|.jfif";
            return allow.Contains(extension);
        }

        /// <summary>
        /// 上传视频
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public MessageModel UploadVideo(UploadVideoCommand command)
        {
            if (command.Video == null)
                throw new MyException("请选择上传视频文件");
            var videoStream = command.Video?.OpenReadStream();
            var videoFileName = command.Video?.FileName;

            if (!AllowExtensionByVideo(videoFileName))
                throw new MyException("不支持的视频文件格式");

            var size = videoStream.Length / 1024 / 1024;
            if (size > 30)
                throw new MyException("视频大小不能超过30M");

            string videoExtension = Path.GetExtension(videoFileName).ToLower();
            var videoOssPath = OssManagement.File_Video(Guid.NewGuid().ToString("N"), videoExtension);
            var eTag = AliyunOSSHelper.PutObject(videoOssPath, videoStream);

            if (command.Cover == null)
                throw new MyException("请选择上传封面文件");
            var coverStream = command.Cover?.OpenReadStream();
            var coverFileName = command.Cover?.FileName;

            if (!AllowExtensionByImage(coverFileName))
                throw new MyException("]不支持的封面文件格式");
            if (coverStream.Length > 0x300000)//3M
                throw new MyException("文件大小不能超过3M");

            string coverExtension = Path.GetExtension(coverFileName).ToLower();
            var coverOssPath = OssManagement.File_Image(Guid.NewGuid().ToString("N"), coverExtension);
             AliyunOSSHelper.PutObject(coverOssPath, coverStream);

            var editor = new FileEditor()
            {
                CategoryId = command.Category,
                Name = videoFileName,
                Type = FileType.Video,
                Size = videoStream.Length,
                ETag = eTag,
                SupplierId = 0,
                Path = videoOssPath,
                Cover = coverOssPath
            };
            ossFileService.Create(editor);
            return SuccessResult(new {
                Video = videoOssPath,
                Cover = coverOssPath
            });
        }

        /// <summary>
        /// 允许的文件扩展名
        /// </summary>
        public bool AllowExtensionByVideo(string fileName)
        {
            var extension = Path.GetExtension(fileName);
            var allow = ".mp4|.3gp";
            return allow.Contains(extension);
        }

        /// <summary>
        /// 修改文件名称
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public MessageModel SetImageName(long id, string name)
        {
            ossFileService.SetName(id, name);
            return SuccessResult();
        }
        /// <summary>
        /// 批量分组
        /// </summary>
        /// <param name="edior"></param>
        public MessageModel SetGroup(BatchEdior edior)
        {
            ossFileService.SetGroup(edior);
            return SuccessResult();
        }

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="id"></param>
        public MessageModel RemoveImage(BatchEdior edior)
        {
            ossFileService.Remove(edior);
            return SuccessResult();
        }



        #region 分类
        public MessageModel CategoryList(FileType type) => SuccessResult(categoryService.List(type)) ;


        /// <summary>
        /// 文件分类
        /// </summary>
        /// <param name="editor"></param>
        /// <returns></returns>
        public MessageModel CategorySave(CategoryEditor category)
        {
            Verify(category);
            if (category.Id > 0)
                categoryService.Save(category);
            else
            {
                var info = categoryService.GetById(category.Id);
                var mode = category.Map<CategoryInfo>();
                if (info != null)
                    mode.FileCount = info.FileCount + 1;
                categoryService.Insert(mode);
                categoryService.RemoveCache(category.Type);
            }
            return SuccessResult();
        }

        /// <summary>
        /// 验证
        /// </summary>
        /// <param name="editor"></param>
        private MessageModel Verify(CategoryEditor editor)
        {
            if (string.IsNullOrWhiteSpace(editor.Name))
                throw new MyException("分类名称不能为空");
            if (categoryService.Count(p => p.Name == editor.Name && p.Type == (int)editor.Type) > 0)
                throw new MyException("已经存在了相同名称的分类");
            return SuccessResult();
        }

        /// <summary>
        /// 修改文件分类名称
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public MessageModel SetCategoryName(long id, string name)
        {
            categoryService.SetName(id, name);
            return SuccessResult();
        }

        /// <summary>
        /// 删除文件分类
        /// </summary>
        /// <param name="id"></param>
        public MessageModel RemoveCategory(long id)
        {
            categoryService.Remove(id);
            return SuccessResult();
        }
        #endregion

    }
}
