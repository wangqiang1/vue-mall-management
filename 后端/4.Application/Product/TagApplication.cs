﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Model;
using Wq.Core.PlatAdminService;
using Wq.Core.PlatAdminService.Models;
using Wq.Core.ProductService;
using Wq.Core.ProductService.Models;

namespace Wq.Core.Application.Product
{
    /// <summary>
    /// 商品标签
    /// </summary>
    public class TagApplication : BaseApplication
    {
        private readonly TagService tagService;
        private readonly LogService logService;
        public TagApplication(TagService tagService, LogService logService)
        {
            this.tagService = tagService;
            this.logService = logService;
        }

        

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        public MessageModel List(PageModel<TagQuery> pageModel)
        {
            int count = 0;
            return SuccessResult(tagService.List(pageModel, ref count), count);
        }

        /// <summary>
        /// 全部标签
        /// </summary>
        /// <returns></returns>
        public MessageModel Tags() => SuccessResult(tagService.Tags());

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="tag"></param>
        public MessageModel Save(TagEditor tag)
        {
            Verify(tag);
            var old = tagService.GetById(tag.Id);
            if (tag.Id > 0)
                tagService.Save(tag);
            else
            {
                var info = tag.Map<TagInfo>();
                info.CreateTime = DateTime.Now;
                tagService.Insert(info);
            }
            Log(old, tag);
            tagService.RemoveCache();
            return SuccessResult();
        }
         
        /// <summary>
        /// 删除标签
        /// </summary>
        /// <param name="id"></param>
        public MessageModel Remove(long id)
        {
            var old = tagService.GetById(id);
            tagService.DeleteById(id);
            Log(old, isDelete: true);
            tagService.RemoveCache();
            return SuccessResult();
        }

        /// <summary>
        /// 验证
        /// </summary>
        /// <param name="tag"></param>
        public void Verify(TagEditor tag)
        {
            if (string.IsNullOrWhiteSpace(tag.Name))
                throw new MyException("标签名称不能为空");
            if (tagService.Count(p => p.Name == tag.Name && p.Id != tag.Id) > 0)
                throw new MyException("已经存在了相同名称的标签");
        }


        /// <summary>
        /// 写日志
        /// </summary>
        /// <param name="oldTag"></param>
        /// <param name="newTag"></param>
        /// <param name="isDelete"></param>
        public void Log(TagInfo oldTag, TagEditor newTag = null, bool isDelete = false)
        {
            string msg = string.Empty;
            if (isDelete)
                msg = $"删除商品分类【{oldTag.Name}】";
            else
            {

                if (newTag.Id == 0)
                    msg = $"新增商品分类【{newTag.Name}】";
                else
                {
                    msg = $"修改商品分类【{newTag.Name}】";
                    if (newTag.Name != oldTag.Name)
                        msg += $" 名称由【{oldTag.Name}】变为【{newTag.Name}】";
                }
            }

            logService.Create(LogModule.ProductTag, msg);
        }


        /// <summary>
        /// 修改排序
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Sequence"></param>
        public MessageModel UpdateSequence(long id, long sequence)
        {
            tagService.UpdateSequence(id, sequence);
            return SuccessResult();
        }

         
    }
}
