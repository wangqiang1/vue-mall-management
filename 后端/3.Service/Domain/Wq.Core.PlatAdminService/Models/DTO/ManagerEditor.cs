﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.PlatAdminService.Models
{
    /// <summary>
    /// 管理员编辑模型
    /// </summary>
    public class ManagerEditor
    {
        /// <summary>
        /// 管理员标识
        /// </summary>
        public long Id
        {
            get; set;
        }
        /// <summary>
        /// 用户名
        /// </summary>
        public string Username
        {
            get; set;
        }
        /// <summary>
        /// 角色ID
        /// </summary>
        public long RoleId
        {
            get; set;
        }

        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName
        {
            get; set;
        }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password
        {
            get; set;
        }
        /// <summary>
        /// 密码加盐
        /// </summary>
        public string PasswordSalt
        {
            get; set;
        }
    }
}
