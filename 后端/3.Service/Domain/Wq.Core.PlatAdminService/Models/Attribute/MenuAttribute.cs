﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.PlatAdminService.Models
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
    internal class MenuAttribute : Attribute
    {

        /// <summary>
        /// 一级菜单定义
        /// </summary>
        /// <param name="firstName">菜单名</param>
        /// <param name="path">默认路径</param>
        public MenuAttribute(string firstName, string path)
        {
            FirstName = firstName;
            Depth = 1;
            Path = path;
        }

        /// <summary>
        /// 二级菜单定义
        /// </summary>
        public MenuAttribute(string firstName, string secondName, string path)
        {
            FirstName = firstName;
            SecondName = secondName;
            Depth = 2;
            Path = path;
        }
        /// <summary>
        /// 三级菜单定义
        /// </summary>
        public MenuAttribute(string firstName, string secondName, string thirdName, string path)
        {
            FirstName = firstName;
            SecondName = secondName;
            ThirdName = thirdName;
            Depth = 3;
            Path = path;
        }

        /// <summary>
        /// 一级菜单名
        /// </summary>
        public string FirstName
        {
            get;
        }
        /// <summary>
        /// 二级菜单名
        /// </summary>
        public string SecondName
        {
            get;
        }
        /// <summary>
        /// 三级菜单名
        /// </summary>
        public string ThirdName { get; }

        /// <summary>
        /// 页面路径
        /// </summary>
        public string Path
        {
            get;
        }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sequence
        {
            get; set;
        }
        
        /// <summary>
        /// 深度
        /// </summary>
        public int Depth { get; set; }

        /// <summary>
        /// 是否需要权限控制
        /// </summary>
        public bool Permission { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        public string Icon
        {
            get; set;
        }


    }
}
