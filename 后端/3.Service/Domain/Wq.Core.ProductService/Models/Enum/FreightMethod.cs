﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService
{
    /// <summary>
    /// 计费方式
    /// </summary>
    public enum FreightMethod
    {
        /// <summary>
        /// 按件
        /// </summary>
        [Description("按件")]
        ByPiece = 1,
        /// <summary>
        /// 按重
        /// </summary>
        [Description("按重")]


        ByWeight = 2


    }
}