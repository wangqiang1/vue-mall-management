﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    public class AttributeEditor
    {
        /// <summary>
        /// 属性Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 属性名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// 分类Id
        /// </summary>
        public List<long> Categorys { get; set; }
    }
}
