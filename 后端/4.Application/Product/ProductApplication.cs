﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.Model;
using Wq.Core.PlatAdminService;
using Wq.Core.PlatAdminService.Models;
using Wq.Core.ProductService;
using Wq.Core.ProductService.Models;

namespace Wq.Core.Application.Product
{
    /// <summary>
    /// 商品
    /// </summary>
    public class ProductApplication : BaseApplication
    {
        private readonly Wq.Core.ProductService.ProductService productService;
        private readonly AttributeService attributeService;
        private readonly AttributeItemService attributeItemService;
        private readonly LogService logService;

        /// <summary>
        /// 商品
        /// </summary>
        public ProductApplication(Wq.Core.ProductService.ProductService productService, 
            AttributeService attributeService, 
            AttributeItemService attributeItemService, 
            LogService logService)
        {
            this.productService = productService;
            this.attributeService = attributeService;
            this.attributeItemService = attributeItemService;
            this.logService = logService;
        }

        /// <summary>
        /// 添加编辑商品
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public MessageModel Save(ProductEditor product)
        {
            Verify(product);
            ProductEditor oldProduct = null; 
            if (product.Id > 0)
            {
                oldProduct = productService.Details(product.Id);
                productService.Save(product);
                product = productService.Details(product.Id);
            }
            else
                productService.Create(product);
            product.Id = 0;
            Log(oldProduct, product);
            return SuccessResult();
        }
        /// <summary>
        /// 验证
        /// </summary>
        /// <param name="editor"></param>
        public void Verify(ProductEditor editor)
        {
            
        }

        /// <summary>
        /// 日志
        /// </summary>
        /// <param name="oldProduct"></param>
        /// <param name="product"></param>
        /// <param name="isDelete"></param>
        private void Log(ProductEditor oldProduct, ProductEditor product = null, bool isDelete = false)
        {
            string msg = string.Empty;
            if (isDelete)
                msg = $"删除商品【{oldProduct.Name}】";
            else
            {

                if (product.Id == 0)
                    msg = $"新增商品【{product.Name}】";
                else
                {
                    msg = $"修改商品【{product.Name}】";
                    if (product.Name != oldProduct.Name)
                        msg += $" 名称由【{oldProduct.Name}】变为【{product.Name}】";

                    #region 规格
                    product.Skus.ForEach(item => {
                        var oldSku = oldProduct.Skus.Where(p => p.Id == item.Id).FirstOrDefault();
                        if (oldSku != null)
                        {
                            if (item.SalePrice != oldSku.SalePrice || item.Stock != oldSku.Stock)
                            {
                                msg += $"修改规格【{item.SkuDesc}】";
                                if (item.SalePrice != oldSku.SalePrice)
                                    msg += $"价格由【{oldSku.SalePrice}】变为【{item.SalePrice}】";
                                if (item.Stock != oldSku.Stock)
                                    msg += $"库存由【{oldSku.Stock}】变为【{item.Stock}】";
                            }
                        }
                        else
                            msg += $"新增规格【{item.SkuDesc}】：价格【{item.SalePrice}】库存【{item.Stock}】";
                    });

                    var skuIds = product.Skus.Select(p => p.Id).ToList();
                    var deleteSkuDesc = oldProduct.Skus.Where(p =>  !skuIds.Contains(p.Id)).Select(p => p.SkuDesc).ToList();
                    if (deleteSkuDesc.Count > 0)
                        msg += $"删除规格【{string.Join(";", deleteSkuDesc)}】";
                    #endregion

                    #region 属性
                    if (product.Attributes.Count > 0)
                    {

                        string oldAttributeName = attributeService.GetById(oldProduct.AttributeId)?.Name;
                        string attributeName = attributeService.GetById(product.AttributeId)?.Name;


                        if (product.AttributeId != oldProduct.AttributeId)
                        {
                            msg += $"商品属性由【{oldAttributeName}】变成【{attributeName}】";
                        }

                        product.Attributes.ForEach(item => {

                            var oldattributeItem = oldProduct.Attributes.Where(p => p.AttributeItemId == item.AttributeItemId).FirstOrDefault();
                            if (oldattributeItem != null)
                            {
                                if (item.Value != oldattributeItem.Value)
                                    msg += $"属性【{attributeName}】修改项【{attributeItemService.GetById(item.AttributeItemId)?.Name}】规格由值【{attributeItemService.GetValue(oldattributeItem.TypeId, oldattributeItem.Value)}】变成【{attributeItemService.GetValue(item.TypeId, item.Value)}】";
                            }
                            else
                            {

                                msg += $"属性【{attributeName}】新增项【{attributeItemService.GetById(item.AttributeItemId)?.Name}】规格值【{attributeItemService.GetValue(item.TypeId, item.Value)}】";
                            }

                        });
                        var AttributeItemIds = product.Attributes.Select(p => p.AttributeItemId).ToList();

                        var deleteAttributeItem = oldProduct.Attributes.Where(p => !AttributeItemIds.Contains(p.AttributeItemId)).Select(p => p.AttributeItemId).ToList();
                        if (deleteSkuDesc.Count > 0)
                            msg += $"删除属性【attributeName】项【{string.Join(";", attributeItemService.GetItemName(deleteAttributeItem))}】";
                    }
                    #endregion

                    if(oldProduct.Freight != product.Freight)
                        msg += $" 邮费由【{oldProduct.Freight}】变为【{product.Freight}】";

                }
            }

            logService.Create(LogModule.ProductCategory, msg);
        }


        /// <summary>
        /// 商品列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public MessageModel List(PageModel<ProductQuery> pageModel)
        {
            int count = 0;
            return SuccessResult(productService.List(pageModel, ref count), count);
        }
    }
}
