﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Common;

namespace Wq.Core.Extensions.ServiceExtensions
{
    /// <summary>
    /// Swagger 启动服务
    /// </summary>
    public static class SwaggerSetup
    {
        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("manager", new OpenApiInfo { Title = "管理员接口", Version = "v1" });
                c.SwaggerDoc("product", new OpenApiInfo { Title = "商品接口", Version = "v1" });
                c.SwaggerDoc("file", new OpenApiInfo { Title = "文件接口", Version = "v1" });
                c.OrderActionsBy(x => x.RelativePath);
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, Path.Combine(AppContext.BaseDirectory, "Wq.Core.Api.xml")), true);
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, Path.Combine(AppContext.BaseDirectory, "Wq.PlatAdmin.APi.xml")), true);
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, Path.Combine(AppContext.BaseDirectory, "Wq.product.APi.xml")), true);
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, Path.Combine(AppContext.BaseDirectory, "Wq.File.APi.xml")), true);
                c.OperationFilter<AddResponseHeadersFilter>();
                c.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
                c.OperationFilter<SecurityRequirementsOperationFilter>();
                c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme() { 
                    Description ="请在输入时添加Bearer和一个空格",
                    Name="Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });
            });
        }

        public static void UseMySwagger(this IApplicationBuilder app)
        {
            app.UseSwagger();
           app.UseSwaggerUI(c => {
               c.SwaggerEndpoint("/swagger/manager/swagger.json", "管理员接口");
               c.SwaggerEndpoint("/swagger/product/swagger.json", "商品接口");
               c.SwaggerEndpoint("/swagger/file/swagger.json", "文件接口");
           });
        }
    }
   
  
}
