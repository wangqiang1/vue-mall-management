﻿using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Common;
using Wq.Core.Common.Helper;
using Wq.Core.Extensions.Assembly.RabbitMQ;

namespace Wq.Core.Extensions.ServiceExtensions
{
    public static class RabbitMQSetup
    {
      
        public static void AddRabbitMQSetup(this IServiceCollection services) {
            var handlers = RuntimeHelper.GetProjectTypes()
                  .Where(p => !p.IsInterface && !p.IsAbstract && !p.IsSealed)
                  .Where(p => typeof(RabbitListener).IsAssignableFrom(p))
                  .ToList();

            foreach (var handler in handlers)
                services.AddSingleton(typeof(RabbitListener), handler);
        }
    }
}
