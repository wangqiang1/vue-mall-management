﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.Common.Helper
{
  public  class DateHelper
    {
        public static TimeSpan GetHoursTimeSpan(int Hours)
        {
            DateTime clearTime = DateTime.Now.AddHours(Hours);
            TimeSpan ts1 = new TimeSpan(clearTime.Ticks);
            TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
            TimeSpan ts = ts1.Subtract(ts2);
            return ts;
        }
    }
}
