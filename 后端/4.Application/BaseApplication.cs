﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.Model;

namespace Wq.Core.Application
{
   public class BaseApplication: IRegister
    {
        /// <summary>
        /// 返回成功结果数据
        /// </summary>
        protected MessageModel SuccessResult(object data = null, int? count = null) => (new MessageModel { Success = true, Data = data, Count = count });

        /// <summary>
        /// 返回失败
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        protected MessageModel FailResult(string msg) => (new MessageModel { Success = false, Msg = msg });
    }
}
