﻿using System;
namespace Wq.Core.Extensions
{
   public class MyException : Exception
    {
        /// <summary>
     /// 异常代码
     /// </summary>
        public string ErrorCode
        {
            get;
        }
        /// <summary>
        /// 异常关联数据
        /// </summary>
        public object ErrorData
        {
            get; private set;
        }


        public MyException(string message) : base(message)
        {
        }
        public MyException(string code, string message) : base(message)
        {
            ErrorCode = code;
        }

        public MyException(string code, string message, object data) : base(message)
        {
            ErrorCode = code;
            ErrorData = data;
        }

        public MyException SetData(object data)
        {
            ErrorData = data;
            return this;
        }
    }
}
