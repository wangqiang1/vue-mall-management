﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    /// <summary>
    /// 商品服务查询
    /// </summary>
    public class ServiceQuery
    {
        /// <summary>
        /// 服务名称
        /// </summary>
        public string Name { get; set; }
    }
}
