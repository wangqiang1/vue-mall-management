﻿using System.ComponentModel;

namespace Wq.Core.ProductService.Models
{
    public enum OnSaleOperation
    {
       
        /// <summary>
        /// 立即上架
        /// </summary>
        [Description("立即上架")]
        Immediately = 1,
        /// <summary>
        /// 制定时间上架
        /// </summary>
        [Description("制定时间上架")]
        Timing = 2,
        /// <summary>
        /// 进入仓库
        /// </summary>
        [Description("进入仓库")]
        InWarehouse = 3,
    }
}
