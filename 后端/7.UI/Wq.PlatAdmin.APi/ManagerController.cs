﻿using System;
using System.Collections.Generic;
using Framework.Base;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Wq.Core.Application.Manager;
using Wq.Core.Extensions;
using Wq.Core.Model;
using Wq.Core.PlatAdminService.Models;
using Wq.Core.Extensions.Assembly.Jwt;
using Framework;

namespace Wq.PlatAdmin.APi
{
    /// <summary>
    /// 权限管理
    /// </summary>
    [Route("admin")]
    public class ManagerController : PlatAdminController
    {
        private readonly ManagerApplication managerApplication;

        /// <summary>
        /// 权限管理
        /// </summary>
        public ManagerController(ManagerApplication managerApplication)
        {
            this.managerApplication = managerApplication;
         
        }

        #region 权限组
        /// <summary>
        /// 添加权限组
        /// </summary>
        /// <param name="role">权限实体</param>
        /// <returns></returns>
        [HttpPost("role/save")]
        public MessageModel SaveRole(RoleEditor role) => managerApplication.SaveRole(role);

        /// <summary>
        /// 获取权限组
        /// </summary>
        /// <param name="id">权限Id</param>
        /// <returns></returns>

        [HttpGet("role/details")]
        public MessageModel GetRole(long id) => managerApplication.Details(id);


        /// <summary>
        /// 获取全部权限组
        /// </summary>
        /// <returns></returns>
        [HttpGet("role/list")]
        public MessageModel GetRoles() => managerApplication.GetRoles();


        /// <summary>
        /// 获取用户导航列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("role/menus")]
        public MessageModel GetUsableMenus() => managerApplication.GetUsableMenus(CurrentManager);


        /// <summary>
        /// 删除权限组
        /// </summary>
        /// <param name="id">管理员Id</param>
        /// <returns></returns>
        [HttpGet("role/delete")]
        public MessageModel DeleteRole(long id) => managerApplication.DeleteRole(id);
        #endregion

        #region 管理员

        /// <summary>
        /// 管理员列表
        /// </summary>
        /// <param name="pageModel">查询条件</param>
        /// <returns></returns>
        [HttpPost("manager/list")]
        public MessageModel List(PageModel<ManagerQuery> pageModel) => managerApplication.List(pageModel);

        /// <summary>
        /// 添加编辑管理员
        /// </summary>
        /// <param name="manager">管理员实体</param>
        /// <returns></returns>
        [HttpPost("manager/save")]
       
        public MessageModel SaveManager(ManagerEditor manager) => managerApplication.SaveManager(manager);

        /// <summary>
        /// 删除管理员
        /// </summary>
        /// <param name="id">管理员Id</param>
        /// <returns></returns>
        [HttpGet("manager/remove")]
        public MessageModel RemoveManager(long id) => managerApplication.RemoveManager(id);
        #endregion

    }
}