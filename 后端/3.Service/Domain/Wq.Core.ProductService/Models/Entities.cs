﻿
using System;
using SqlSugar;

namespace Wq.Core.ProductService.Models
{
	/// <summary>
	/// Wq_Attribute
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_Attribute")]
	public class AttributeInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 名称
		/// </summary>
	    public string  Name {get; set;}
		/// <summary>
		/// 序号
		/// </summary>
	    public int  Sequence {get; set;}
	}

	/// <summary>
	/// Wq_AttributeCategory
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_AttributeCategory")]
	public class AttributeCategoryInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 属性Id
		/// </summary>
	    public long  AttributeId {get; set;}
		/// <summary>
		/// 分类Id
		/// </summary>
	    public long  CategoryId {get; set;}
	}

	/// <summary>
	/// Wq_AttributeItem
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_AttributeItem")]
	public class AttributeItemInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 项名称
		/// </summary>
	    public string  Name {get; set;}
		/// <summary>
		/// 序号
		/// </summary>
	    public int  Sequence {get; set;}
		/// <summary>
		/// 属性Id
		/// </summary>
	    public long  AttributeId {get; set;}
		/// <summary>
		/// 类型 单选 多选 文本
		/// </summary>
	    public int  TypeId {get; set;}
	}

	/// <summary>
	/// Wq_AttributeValue
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_AttributeValue")]
	public class AttributeValueInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 属性Id
		/// </summary>
	    public long  AttributeId {get; set;}
		/// <summary>
		/// 项Id
		/// </summary>
	    public long  ItemId {get; set;}
		/// <summary>
		/// 值
		/// </summary>
	    public string  Value {get; set;}
	}

	/// <summary>
	/// Wq_Brand
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_Brand")]
	public class BrandInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 名称
		/// </summary>
	    public string  Name {get; set;}
		/// <summary>
		/// 分类图标
		/// </summary>
	    public string  Icon {get; set;}
		/// <summary>
		/// 序号
		/// </summary>
	    public int  Sequence {get; set;}
		/// <summary>
		/// CategoryId
		/// </summary>
	    public long  CategoryId {get; set;}
	}

	/// <summary>
	/// Wq_Category
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_Category")]
	public class CategoryInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 名称
		/// </summary>
	    public string  Name {get; set;}
		/// <summary>
		/// 分类图标
		/// </summary>
	    public string  Icon {get; set;}
		/// <summary>
		/// 序号
		/// </summary>
	    public int  Sequence {get; set;}
		/// <summary>
		/// 上级ID
		/// </summary>
	    public long  ParentId {get; set;}
		/// <summary>
		/// 是否显示
		/// </summary>
	    public bool  Display {get; set;}
		/// <summary>
		/// 深度
		/// </summary>
	    public int  Depth {get; set;}
	}

	/// <summary>
	/// Wq_FreightGroup
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_FreightGroup")]
	public class FreightGroupInfo
	{
		/// <summary>
		/// 分组ID
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public int  Id {get; set;}
		/// <summary>
		/// 运费模板ID
		/// </summary>
	    public int  TemplateId {get; set;}
		/// <summary>
		/// 首件/重
		/// </summary>
	    public decimal  DefaultNumber {get; set;}
		/// <summary>
		/// 续件/重
		/// </summary>
	    public decimal  AddNumber {get; set;}
		/// <summary>
		/// 首费
		/// </summary>
	    public decimal  DefaultPrice {get; set;}
		/// <summary>
		/// 续费
		/// </summary>
	    public decimal  AddPrice {get; set;}
	}

	/// <summary>
	/// Wq_FreightGroupRegion
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_FreightGroupRegion")]
	public class FreightGroupRegionInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public int  Id {get; set;}
		/// <summary>
		/// 运费模板ID
		/// </summary>
	    public int  TemplateId {get; set;}
		/// <summary>
		/// 分组ID
		/// </summary>
	    public int  GroupId {get; set;}
		/// <summary>
		/// 区域Id
		/// </summary>
	    public int  RegionId {get; set;}
		/// <summary>
		/// 排除的地区
		/// </summary>
	    public bool  Exclude {get; set;}
	}

	/// <summary>
	/// Wq_FreightTemplate
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_FreightTemplate")]
	public class FreightTemplateInfo
	{
		/// <summary>
		/// 运费模板ID
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public int  Id {get; set;}
		/// <summary>
		/// 运费模板名称
		/// </summary>
	    public string  Name {get; set;}
		/// <summary>
		/// 计费方式,1为计件，2为计重
		/// </summary>
	    public int  Method {get; set;}
		/// <summary>
		/// 最后修改时间
		/// </summary>
	    public DateTime  ModifyDate {get; set;}
	}

	/// <summary>
	/// Wq_Product
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_Product")]
	public class ProductInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 商品名称
		/// </summary>
	    public string  Name {get; set;}
		/// <summary>
		/// 短名称
		/// </summary>
	    public string  ShortName {get; set;}
		/// <summary>
		/// 商品编码
		/// </summary>
	    public string  Code {get; set;}
		/// <summary>
		/// 分享描述
		/// </summary>
	    public string  SharingDescription {get; set;}
		/// <summary>
		/// 首张主图(冗余)
		/// </summary>
	    public string  Thumbnail {get; set;}
		/// <summary>
		/// 主视频
		/// </summary>
	    public string  Video {get; set;}
		/// <summary>
		/// 主视频封面
		/// </summary>
	    public string  VideoCover {get; set;}
		/// <summary>
		/// 是否有规格
		/// </summary>
	    public bool  HasSku {get; set;}
		/// <summary>
		/// 销售价
		/// </summary>
	    public decimal  SalePrice {get; set;}
		/// <summary>
		/// 划线价
		/// </summary>
	    public decimal  MarkingPrice {get; set;}
		/// <summary>
		/// 成本价
		/// </summary>
	    public decimal  CostPrice {get; set;}
		/// <summary>
		/// 商品状态
		/// </summary>
	    public int  Status {get; set;}
		/// <summary>
		/// 是否启用会员价
		/// </summary>
	    public bool  IsMemberPrice {get; set;}
		/// <summary>
		/// 运费模板
		/// </summary>
	    public int  FreightTemplateId {get; set;}
		/// <summary>
		/// 固定运费
		/// </summary>
	    public decimal  Freight {get; set;}
		/// <summary>
		/// 创建时间
		/// </summary>
	    public DateTime  CreateTime {get; set;}
		/// <summary>
		/// 商品总库存
		/// </summary>
	    public int  Stock {get; set;}
		/// <summary>
		/// 真实销量
		/// </summary>
	    public int  SaleCount {get; set;}
		/// <summary>
		/// 虚拟销量
		/// </summary>
	    public int  VirtualSaleCount {get; set;}
		/// <summary>
		/// 定时上架时间
		/// </summary>
	    public DateTime?  OnSaleTime {get; set;}
		/// <summary>
		/// 上架操作
		/// </summary>
	    public int?  Operation {get; set;}
		/// <summary>
		/// 排序序号
		/// </summary>
	    public int  Sequence {get; set;}
		/// <summary>
		/// 是否已删除
		/// </summary>
	    public bool  IsDelete {get; set;}
		/// <summary>
		/// 属性模板
		/// </summary>
	    public long?  AttributeId {get; set;}
		/// <summary>
		/// 单位
		/// </summary>
	    public string  Unit {get; set;}
		/// <summary>
		/// 限购类型
		/// </summary>
	    public int?  LimitType {get; set;}
		/// <summary>
		/// 限购数量
		/// </summary>
	    public int?  LimitNum {get; set;}
		/// <summary>
		/// 最小购买数量
		/// </summary>
	    public int?  MinBuy {get; set;}
		/// <summary>
		/// 定时下架时间
		/// </summary>
	    public DateTime?  OffSaleTime {get; set;}
	}

	/// <summary>
	/// Wq_ProductAttribute
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_ProductAttribute")]
	public class ProductAttributeInfo
	{
		/// <summary>
		/// 主键ID(自增)
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public int  Id {get; set;}
		/// <summary>
		/// ProductId
		/// </summary>
	    public long  ProductId {get; set;}
		/// <summary>
		/// 属性Id
		/// </summary>
	    public long  AttributeId {get; set;}
		/// <summary>
		/// 属性Id
		/// </summary>
	    public long  AttributeItemId {get; set;}
		/// <summary>
		/// 类型 单选 多选 文本
		/// </summary>
	    public int  TypeId {get; set;}
		/// <summary>
		/// 值
		/// </summary>
	    public string  Value {get; set;}
		/// <summary>
		/// 序号
		/// </summary>
	    public int  Sequence {get; set;}
	}

	/// <summary>
	/// Wq_ProductCategory
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_ProductCategory")]
	public class ProductCategoryInfo
	{
		/// <summary>
		/// 主键ID(自增)
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public int  Id {get; set;}
		/// <summary>
		/// ProductId
		/// </summary>
	    public long  ProductId {get; set;}
		/// <summary>
		/// 分类
		/// </summary>
	    public long  CategoryId {get; set;}
	}

	/// <summary>
	/// Wq_ProductService
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_ProductService")]
	public class ProductServiceInfo
	{
		/// <summary>
		/// 主键ID(自增)
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public int  Id {get; set;}
		/// <summary>
		/// ProductId
		/// </summary>
	    public long  ProductId {get; set;}
		/// <summary>
		/// 服务id
		/// </summary>
	    public long  ServiceId {get; set;}
	}

	/// <summary>
	/// Wq_ProductSku
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_ProductSku")]
	public class ProductSkuInfo
	{
		/// <summary>
		/// 主键ID
		/// </summary>
		[SugarColumn(IsPrimaryKey = true)]
	    public string  Id {get; set;}
		/// <summary>
		/// 商品ID
		/// </summary>
	    public long  ProductId {get; set;}
		/// <summary>
		/// 货号/编码
		/// </summary>
	    public string  Code {get; set;}
		/// <summary>
		/// 规格图
		/// </summary>
	    public string  Thumbnail {get; set;}
		/// <summary>
		/// 成本价
		/// </summary>
	    public decimal  CostPrice {get; set;}
		/// <summary>
		/// 销售价
		/// </summary>
	    public decimal  SalePrice {get; set;}
		/// <summary>
		/// 库存
		/// </summary>
	    public int  Stock {get; set;}
		/// <summary>
		/// 重量
		/// </summary>
	    public decimal  Weight {get; set;}
		/// <summary>
		/// 销量
		/// </summary>
	    public int  SaleCount {get; set;}
		/// <summary>
		/// 序号
		/// </summary>
	    public int  Sequence {get; set;}
		/// <summary>
		/// 是否已删除
		/// </summary>
	    public bool  IsDelete {get; set;}
	}

	/// <summary>
	/// Wq_ProductTag
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_ProductTag")]
	public class ProductTagInfo
	{
		/// <summary>
		/// 主键ID(自增)
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public int  Id {get; set;}
		/// <summary>
		/// ProductId
		/// </summary>
	    public long  ProductId {get; set;}
		/// <summary>
		/// 分类
		/// </summary>
	    public long  TagId {get; set;}
	}

	/// <summary>
	/// Wq_ProductThumbnail
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_ProductThumbnail")]
	public class ProductThumbnailInfo
	{
		/// <summary>
		/// 主键ID(自增)
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 商品ID
		/// </summary>
	    public long  ProductId {get; set;}
		/// <summary>
		/// 图片路径
		/// </summary>
	    public string  FilePath {get; set;}
	}

	/// <summary>
	/// Wq_Service
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_Service")]
	public class ServiceInfo
	{
		/// <summary>
		/// 主键ID(自增)
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 名称
		/// </summary>
	    public string  Name {get; set;}
		/// <summary>
		/// 图标
		/// </summary>
	    public string  Icon {get; set;}
		/// <summary>
		/// 描述
		/// </summary>
	    public string  Description {get; set;}
		/// <summary>
		/// 创建时间
		/// </summary>
	    public DateTime  CreateTime {get; set;}
	}

	/// <summary>
	/// Wq_SkuSpec
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_SkuSpec")]
	public class SkuSpecInfo
	{
		/// <summary>
		/// 主键ID
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 商品ID
		/// </summary>
	    public long  ProductId {get; set;}
		/// <summary>
		/// skuid
		/// </summary>
	    public string  SkuId {get; set;}
		/// <summary>
		/// 规格Id
		/// </summary>
	    public long  SpecId {get; set;}
		/// <summary>
		/// 规格项Id
		/// </summary>
	    public long  SpecValueId {get; set;}
		/// <summary>
		/// 规格名
		/// </summary>
	    public string  Name {get; set;}
		/// <summary>
		/// 规格值
		/// </summary>
	    public string  Value {get; set;}
		/// <summary>
		/// 附加项
		/// </summary>
	    public string  Data {get; set;}
		/// <summary>
		/// 展示形式
		/// </summary>
	    public int  TypeId {get; set;}
	}

	/// <summary>
	/// Wq_Spec
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_Spec")]
	public class SpecInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 规格名称
		/// </summary>
	    public string  Name {get; set;}
		/// <summary>
		/// Alias
		/// </summary>
	    public string  Alias {get; set;}
		/// <summary>
		/// 商品数量
		/// </summary>
	    public int  ProductCount {get; set;}
	}

	/// <summary>
	/// Wq_SpecValue
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_SpecValue")]
	public class SpecValueInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 规格Id
		/// </summary>
	    public long  SpecId {get; set;}
		/// <summary>
		/// 值
		/// </summary>
	    public string  Value {get; set;}
	}

	/// <summary>
	/// Wq_Tag
	/// </summary>
	[TenantAttribute("product")]
	[SugarTable("Wq_Tag")]
	public class TagInfo
	{
		/// <summary>
		/// 主键ID(自增)
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public int  Id {get; set;}
		/// <summary>
		/// 名称
		/// </summary>
	    public string  Name {get; set;}
		/// <summary>
		/// 创建时间
		/// </summary>
	    public DateTime  CreateTime {get; set;}
		/// <summary>
		/// 序号
		/// </summary>
	    public int  Sequence {get; set;}
	}

}