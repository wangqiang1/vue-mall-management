﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.PlatAdminService.Models
{
    public class ManagerQuery
    {
        /// <summary>
        /// 管理员名称
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 权限组
        /// </summary>
        public long? RoleId { get; set; }
    }
}
