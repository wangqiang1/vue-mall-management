﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.FileService.Models;

namespace Wq.Core.FileService
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<FileEditor, Wq.Core.FileService.Models.FileInfo>().ReverseMap();
            CreateMap<CategoryEditor, CategoryInfo>().ReverseMap();
        }
    }
}
