﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;
using Wq.Core.Common;

namespace Wq.Core.Extensions.Assembly.SqlSugar
{
    public class SqlSugarBase
    {
        public SqlSugarBase()
        {
            
            Db = new SqlSugarScope(new List<ConnectionConfig>()
            {
               new ConnectionConfig(){
                ConfigId = "platadmin",
                ConnectionString = Appsettings.app("ConnectionStrings:ManagerEntities"),
                DbType = DbType.MySql,
                IsAutoCloseConnection = true,
                InitKeyType = InitKeyType.Attribute
               },
               new ConnectionConfig(){
                ConfigId = "product",
                ConnectionString = Appsettings.app("ConnectionStrings:ProductEntities"),
                DbType = DbType.MySql,
                IsAutoCloseConnection = true,
                InitKeyType = InitKeyType.Attribute
               },
                new ConnectionConfig(){
                ConfigId = "file",
                ConnectionString = Appsettings.app("ConnectionStrings:File"),
                DbType = DbType.MySql,
                IsAutoCloseConnection = true,
                InitKeyType = InitKeyType.Attribute
               },

            }, 
            db =>
            {
                //里面可以循环
                db.GetConnection("platadmin").Aop.OnLogExecuting = (sql, p) =>
                {
                    Console.WriteLine(sql);
                };
                db.GetConnection("product").Aop.OnLogExecuting = (sql, p) =>
                {
                    Console.WriteLine(sql);
                };
                db.GetConnection("file").Aop.OnLogExecuting = (sql, p) =>
                {
                    Console.WriteLine(sql);
                };
            }
            );
        }

        public SqlSugarScope Db;
    }
}
