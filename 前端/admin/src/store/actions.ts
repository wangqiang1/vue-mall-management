import { RoleMenus,GetSetting } from '@/service/common'
import { ActionTree } from "vuex";
import { State } from './state'


const actions: ActionTree<State, any> = {
  getMenus({ commit }) {
    return RoleMenus().then((res: any) => {
      if(res.success)
        commit('SET_MEUNS', res.data);
    })
  },
  getSetting({ commit }) {
    return GetSetting().then((res: any) => {
      if(res.success)
        commit('SET_SETTING', res.data); 
    })
  },
  
  //getMenus({ commit, state: States }, data: any[]) {
  //  return RoleMenus().then((res: any) => {
  //  commit('SET_MEUNS', res.data);
  // })
  // },

}

export default actions
