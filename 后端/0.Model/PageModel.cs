﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.Model
{
    public class PageRequest
    {
        public PageRequest()
        {
            this.Index = 1;
            this.Size = 10;
            this.Num = 1;
        }

        public int Index { get; set; }

        public int Size { get; set; }

        public int Num { get; set; }
    }

    public class PageModel
    {

        public PageRequest Page { get; set; }

    }

    /// <summary>
    /// 分页模型
    /// </summary>
    public class PageModel<T> : PageModel
    {
        /// <summary>
        /// 会员字段（作为查询条件）
        /// </summary>
        public T Entity { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public string OrderbyName { get; set; } = "Id";

        /// <summary>
        /// 是否升序
        /// </summary>
        public bool IsAsc { get; set; }

        /// <summary>
        /// 获取排序字符串
        /// </summary>
        /// <returns></returns>
        public string GetOrderbyString()
        {
            if (!string.IsNullOrEmpty(OrderbyName))
            {
                if (IsAsc)
                    OrderbyName += " ASC ";
                else
                    OrderbyName += " DESC ";
            }
            return OrderbyName;
        }

        /// <summary>
        /// 起始时间
        /// </summary>
        public DateTime? BeginDateTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime? EndDateTime { get; set; }

    }
}
