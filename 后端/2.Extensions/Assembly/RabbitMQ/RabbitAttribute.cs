﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.Extensions.Assembly.RabbitMQ
{
    /// <summary>
    /// 队列设置
    /// </summary>
    public class RabbitAttribute : Attribute
    {

        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name
        {
            get; set;
        }

        /// <summary>
        /// 是否延迟
        /// </summary>
        public bool Delay
        {
            get; set;
        }

        /// <summary>
        /// RoutingKey
        /// </summary>
        public string RoutingKey
        {
            get; set;
        }

        /// <summary>
        /// 队列设置
        /// </summary>
        /// <param name="name">队列名称</param>
        /// <param name="routingKey">Key唯一</param>
        /// <param name="delay">是否延迟</param>
        public RabbitAttribute(string name, string routingKey, bool delay = false)
        {
            this.Name = name;
            this.Delay = delay;
            this.RoutingKey = routingKey;
        }
    }
}
