﻿using System;
using System.Collections.Generic;
using Framework.Base;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Wq.Core.Application.Manager;
using Wq.Core.Extensions;
using Wq.Core.Model;
using Wq.Core.PlatAdminService.Models;

namespace Wq.PlatAdmin.APi
{
    /// <summary>
    /// 访问凭证操作
    /// </summary>
    [ApiExplorerSettings(GroupName = "manager")]
    [Route("admin/accessToken")]
    public class AccessTokenController : BaseController
    {
        private readonly ManagerApplication managerApplication;
        public AccessTokenController(ManagerApplication managerApplication)
        {
            this.managerApplication = managerApplication;

        }

        /// <summary>
        /// 获取Token
        /// </summary>
        /// <param name="name">用户名</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        [Route("gettoken")]
        [HttpGet]
        public MessageModel GetToken(string name, string password) => managerApplication.GetToken(name, password);

        /// <summary>
        /// 刷新token
        /// </summary>
        /// <param name="token">凭证token</param>
        /// <returns></returns>
        [Route("refreshToken")]
        [HttpGet]
        public MessageModel RefreshToken(string token) => managerApplication.RefreshToken(token);


    }
}
