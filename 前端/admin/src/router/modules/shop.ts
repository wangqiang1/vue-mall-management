import { RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
    {
      path: '/shop/library',
      name: 'ShopLibrary',
      component: () => import('@/views/shop/Library.vue'),
      meta: {
        title: '素材中心'
      }
    },
    {
      path: '/shop/setting',
      name: 'ShopSetting',
      component: () => import('@/views/shop/Setting.vue'),
      meta: {
        title: '店铺信息'
      }
    },
    {
      path: '/shop/mobile/microPage',
      name: 'ShopMicroPage',
      component: () => import('@/views/shop/mobile/MicroPage.vue'),
      meta: {
        title: '手机微页面'
      }
    },
    {
      path: '/shop/mobile/category',
      name: 'ShopCategory',
      component: () => import('@/views/shop/mobile/Category.vue'),
      meta: {
        title: '手机分类'
      }
    }
  ]
  
  export default routes