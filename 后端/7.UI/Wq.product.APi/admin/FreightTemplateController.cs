﻿using Framework;
using Microsoft.AspNetCore.Mvc;
using Wq.Core.Application.Product;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;

namespace Wq.product.APi.admin
{
    /// <summary>
    /// 运费模板
    /// </summary>
    [Route("admin/product/freightTemplate")]
    public class FreightTemplateController : ProductAdminController
    {

        private readonly FreightTemplateApplication application;
        /// <summary>
        /// 运费模板
        /// </summary>
        public FreightTemplateController(FreightTemplateApplication application)
        {
            this.application = application;
        }

         /// <summary>
         /// 运费模板列表
         /// </summary>
         /// <param name="pageModel"></param>
         /// <returns></returns>
        [HttpPost("list")]
        public MessageModel List(PageModel<FreightTemplateQuery> pageModel) => application.List(pageModel);

        /// <summary>
        /// 编辑运费模板
        /// </summary>
        /// <param name="editor"></param>
        /// <returns></returns>
        [HttpPost("save")]
        public MessageModel Save(FreightTemplateEditor editor) => application.Save(editor);
        
        /// <summary>
        /// 删除模板
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("remove")]
        public MessageModel Remove(int id) => application.Remove(id);

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("details")]
        public MessageModel Details(int id) => application.Details(id);

        /// <summary>
        /// 全部运费
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("FreightTemplates")]
        public MessageModel FreightTemplates(int id) => application.FreightTemplates();
    }
}
