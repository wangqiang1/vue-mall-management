﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.Extensions
{
    public class UnAuthorizedException : MyException
    {
        public UnAuthorizedException() : base("unauthorized", "无登录信息或登录信息已失效，请重新登录。")
        {

        }
    }
}
