﻿using System.ComponentModel;

namespace Wq.Core.ProductService.Models
{
    /// <summary>
    /// 限购类型
    /// </summary>
    public enum LimitType
    {
        /// <summary>
        /// 不限购
        /// </summary>
        [Description("不限购")]
        Unlimited = 0,
        /// <summary>
        /// 单次限购
        /// </summary>
        [Description("单次限购")]
        Single = 1,
        /// <summary>
        /// 长期限购
        /// </summary>
        [Description("长期限购")]
        Everlasting = 2
    }
}
