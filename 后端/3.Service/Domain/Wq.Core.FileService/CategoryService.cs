﻿using SqlSugar;
using System.Collections.Generic;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.Extensions.Assembly.SqlSugar;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.FileService.Models;
using Wq.Core.Model;

namespace Wq.Core.FileService
{
    /// <summary>
    /// 图片分类
    /// </summary>
    public class CategoryService : BaseRepository<CategoryInfo>, IRegister
    {

        private readonly BaseRepository redisRepository;
        public CategoryService(BaseRepository redisRepository)
        {
            this.redisRepository = redisRepository;
        }


        /// <summary>
        /// 分类
        /// </summary>
        /// <returns></returns>
        public List<CategoryEditor> List(FileType type) => redisRepository.Get(Model.CacheKey.FileCategory((int)type), () => {

            var list = new List<CategoryEditor>();
            list.Add(new CategoryEditor() { Id = 0, Name = "未分组", FileCount = Context.Queryable<FileInfo>().Count(p => p.CategoryId == 0) });
            list.AddRange(Context.Queryable<CategoryInfo>().Where(p => p.Type == (int)type).Select<CategoryEditor>().ToList());
            var dictionary = Context.Queryable<FileInfo>().Where(p => p.Type == (int)type).GroupBy(p => p.CategoryId).ToDictionary(p => p.CategoryId, p => SqlFunc.AggregateCount(p.CategoryId));
            list.ForEach(item => {
                if (dictionary.ContainsKey(item.Id.ToString()))
                    item.FileCount = int.Parse(dictionary[item.Id.ToString()].ToString());
                else
                    item.FileCount = 0;
            });
            return list;
        }).Result;

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="category"></param>
        public void Save(CategoryEditor category)
        {
            RemoveCache(category.Type);
            Context.Updateable<CategoryInfo>().SetColumns(p => p.Name == category.Name).Where(p => p.Id == category.Id).ExecuteCommand();
        }

        /// <summary>
        /// 更新分类名称
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public void SetName(long id, string name)
        {
            Context.Updateable<CategoryInfo>().SetColumns(p => p.Name == name).Where(p => p.Id == id).ExecuteCommand();
            var info = Context.Queryable<CategoryInfo>().First(p => p.Id == id);
            RemoveCache((FileType)info.Type);
        }

        public void Remove(long id)
        {
            Context.Ado.UseTran(() => {
                var info = Context.Queryable<CategoryInfo>().First(p => p.Id == id);
                Context.Updateable<FileInfo>().SetColumns(p => p.CategoryId == 0).Where(p => p.CategoryId == id).ExecuteCommand();
                Context.Deleteable<CategoryInfo>(p => p.Id == id).ExecuteCommand();
                RemoveCache((FileType)info.Type);
            });
        }

        /// <summary>
        /// 移除缓存
        /// </summary>
        /// <param name="type"></param>
        public void RemoveCache(FileType type) => redisRepository.Remove(Model.CacheKey.FileCategory((int)type));

    }
}
