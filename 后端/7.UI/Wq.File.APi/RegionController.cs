﻿using System;
using System.Collections.Generic;
using Framework;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Wq.Core.Application.Manager;
using Wq.Core.Extensions;
using Wq.Core.Model;
using Wq.Core.PlatAdminService.Models;
using Wq.Core.Application;
using Wq.Core.FileService.Models.DTO;
using Wq.Core.FileService.Models;


namespace Wq.File.APi
{
    /// <summary>
    /// 地区管理
    /// </summary>
    [ApiExplorerSettings(GroupName = "file")]
    [Route("admin/file/region")]
    public class RegionController : FileAdminController
    {
        private readonly RegionApplication application;

        /// <summary>
        /// 地区管理
        /// </summary>
        public RegionController(RegionApplication application)
        {
            this.application = application;

        }

        /// <summary>
        /// 地区列表 
        /// </summary>
        /// <param name="id">上级Id 0表示一级</param>
        /// <param name="level">深度</param>
        /// <returns></returns>
        [HttpGet("list")]
        public MessageModel List(long id = 0, int level = 4) => application.List(id,level);

        /// <summary>
        /// 获取下级集合
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        [HttpGet("subs")]
        public MessageModel Subs(int parentId) => application.Subs(parentId);

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="name">地区名称</param>
        /// <param name="parentId">上级Id</param>
        /// <returns></returns>
        [HttpGet("create")]
        public MessageModel Create(string name, int parentId) => application.Create(name, parentId);
      
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="name">地区名称</param>
        /// <returns></returns>
        [HttpGet("save")]
        public MessageModel Save(int id, string name) => application.Save(id, name);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        [HttpGet("remove")]
        public MessageModel Remove(long id) => application.Remove(id);

        /// <summary>
        /// 地址文件
        /// </summary>
        /// <returns></returns>
        [HttpGet("file")]
        public MessageModel GetFile() => application.GetFile();
    }
}
