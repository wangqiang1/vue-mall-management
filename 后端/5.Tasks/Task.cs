﻿using Hangfire;
using Hangfire.Dashboard.BasicAuthorization;
using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Application.Manager;
using Wq.Core.Common;

namespace Wq.Tasks
{
    //任务调度中间件
    public static class Task
    {
        public static void UseTask(this IApplicationBuilder app)
        {
            if (app == null) throw new ArgumentNullException(nameof(app));
            app.UseHangfireServer(); //配置服务//ConfigureOptions()
            app.UseHangfireDashboard(Appsettings.app("HangFire", "pathMatch"), HfAuthor()); //配置面板
            HangfireService(); //配置各个任务
        }

        /// <summary>
        ///     配置账号模板信息
        /// </summary>
        /// <returns></returns>
        public static DashboardOptions HfAuthor()
        {
            var filter = new BasicAuthAuthorizationFilter(
                new BasicAuthAuthorizationFilterOptions
                {
                    SslRedirect = false,
                    RequireSsl = false,
                    LoginCaseSensitive = false,
                    Users = new[]
                    {
                        new BasicAuthAuthorizationUser
                        {
                            Login = Appsettings.app("HangFire", "Login"), //可视化的登陆账号
                            PasswordClear = Appsettings.app("HangFire", "PasswordClear") //可视化的密码
                        }
                    }
                });
            return new DashboardOptions
            {
                Authorization = new[] { filter }
            };
        }


        #region 配置服务

        public static void HangfireService()
        {
            //RecurringJob.AddOrUpdate<ManagerApplication>((app) => app.HangfireTest(), Cron.Minutely());
          //  RecurringJob.AddOrUpdate<ManagerApplication>(_ => _.HangfireTest(), "0 * * * * ? ", TimeZoneInfo.Local);

        }

        #endregion
    }
}
