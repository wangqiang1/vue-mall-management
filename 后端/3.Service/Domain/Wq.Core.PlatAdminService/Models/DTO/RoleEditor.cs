﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.PlatAdminService.Models
{
    /// <summary>
    /// 权限组 编辑模板
    /// </summary>
    public class RoleEditor
    {

        /// <summary>
        /// 权限Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 权限组名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        public List<RolePrivilege> Privileges { get; set; }

        /// <summary>
        /// 权限列表
        /// </summary>
        public class RolePrivilege
        {
            /// <summary>
            /// Id
            /// </summary>
            public long Id { get; set; }

            /// <summary>
            /// 权限组
            /// </summary>
            public long RoleId { get; set; }

            /// <summary>
            /// 页面Id
            /// </summary>
            public int Route { get; set; }

            /// <summary>
            /// 权限Id
            /// </summary>
            public int Privilege { get; set; }
        }
    }
}
