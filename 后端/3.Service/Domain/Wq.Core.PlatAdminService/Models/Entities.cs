﻿

using System;
using SqlSugar;

namespace Wq.Core.PlatAdminService.Models
{
	/// <summary>
	/// Wq_Manager
	/// </summary>
	[TenantAttribute("platadmin")]
	[SugarTable("Wq_Manager")]
	public class ManagerInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 权限组
		/// </summary>
	    public long  RoleId {get; set;}
		/// <summary>
		/// 管理员名称
		/// </summary>
	    public string  UserName {get; set;}
		/// <summary>
		/// 密码
		/// </summary>
	    public string  Password {get; set;}
		/// <summary>
		/// 加密盐
		/// </summary>
	    public string  PasswordSalt {get; set;}
		/// <summary>
		/// 创建时间
		/// </summary>
	    public DateTime  CreateDate {get; set;}
	}

	/// <summary>
	/// Wq_OperationLog
	/// </summary>
	[TenantAttribute("platadmin")]
	[SugarTable("Wq_OperationLog")]
	public class OperationLogInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 发生时间
		/// </summary>
	    public DateTime  Time {get; set;}
		/// <summary>
		/// 模块
		/// </summary>
	    public int  Module {get; set;}
		/// <summary>
		/// 操作人
		/// </summary>
	    public long  Operator {get; set;}
		/// <summary>
		/// 操作人名称
		/// </summary>
	    public string  OperatorName {get; set;}
		/// <summary>
		/// IP
		/// </summary>
	    public string  OperatorAddress {get; set;}
		/// <summary>
		/// 消息
		/// </summary>
	    public string  Message {get; set;}
	}

	/// <summary>
	/// Wq_Role
	/// </summary>
	[TenantAttribute("platadmin")]
	[SugarTable("Wq_Role")]
	public class RoleInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 权限名称
		/// </summary>
	    public string  Name {get; set;}
		/// <summary>
		/// 备注
		/// </summary>
	    public string  Remark {get; set;}
		/// <summary>
		/// 管理员人数
		/// </summary>
	    public int?  Number {get; set;}
	}

	/// <summary>
	/// Wq_RolePrivilege
	/// </summary>
	[TenantAttribute("platadmin")]
	[SugarTable("Wq_RolePrivilege")]
	public class RolePrivilegeInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 权限Id
		/// </summary>
	    public long  RoleId {get; set;}
		/// <summary>
		/// 页面
		/// </summary>
	    public int  Route {get; set;}
		/// <summary>
		/// 操作权限（查看 添加 删除）
		/// </summary>
	    public int  Privilege {get; set;}
	}

}