import { RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/setting/system/role',
    name: 'SystemRole',
    component: () => import('@/views/setting/system/Role.vue'),
    meta: {
      title: '角色设置'
    }
  },
  {
    path: '/setting/system/role/create',
    name: 'SystemRoleCreate',
    component: () => import('@/views/setting/system/RoleCreate.vue'),
    meta: {
      title: '添加角色'
    }
  },
  {
    path: '/setting/system/manager',
    name: 'SystemManager',
    component: () => import('@/views/setting/system/Manager.vue'),
    meta: {
      title: '管理员'
    }
  },
  {
    path: '/setting/system/area',
    name: 'SystemArea',
    component: () => import('@/views/setting/system/Area.vue'),
    meta: {
      title: '地区管理'
    }
  }
]

export default routes