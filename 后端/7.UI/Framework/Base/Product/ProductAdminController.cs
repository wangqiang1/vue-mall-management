﻿using Framework.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SqlSugar.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework
{
    /// <summary>
    /// 商品后端接口
    /// </summary>
    [Authorize("Manager")]
    [ApiExplorerSettings(GroupName = "product")]

    public class ProductAdminController : AdminController
    {
        
    }
}
