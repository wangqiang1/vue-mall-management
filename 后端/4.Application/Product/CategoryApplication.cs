﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.Model;
using Wq.Core.PlatAdminService;
using Wq.Core.PlatAdminService.Models;
using Wq.Core.ProductService;
using Wq.Core.ProductService.Models;

namespace Wq.Core.Application.Product
{
    public class CategoryApplication : BaseApplication
    {

        private readonly CategoryService categoryService;
        private readonly LogService logService;

        public CategoryApplication(CategoryService categoryService, LogService logService)
        {
            this.categoryService = categoryService;
            this.logService = logService;
        }


        /// <summary>
        /// 添加编辑商品
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public MessageModel Save(CategoryEditor category)
        {
           
            Verify(category);
            var oldCategory = categoryService.GetById(category.Id);
            if (category.Id > 0)
                categoryService.Save(category);
            else {
                var info = category.Map<CategoryInfo>();
                info.Display = true;
                if (info.ParentId == 0) info.Depth = 1;
                else {
                   var parent =  categoryService.GetById(info.ParentId);
                    if (parent == null)
                        throw new MyException("上级分类不存在");
                    info.Depth = parent.Depth + 1;
                }
                categoryService.Insert(info); 
            }
            categoryService.RemoveCache();
            CategoryLog(oldCategory, category);
            return SuccessResult();
        }

        /// <summary>
        /// 属性日志
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="oldAttribute"></param>
        public void CategoryLog(CategoryInfo oldCategory, CategoryEditor category = null, bool isDelete = false)
        {
            string msg = string.Empty;
            if (isDelete)
                msg = $"删除商品分类【{oldCategory.Name}】";
            else
            {

                if (category.Id == 0)
                    msg = $"新增商品分类【{category.Name}】";
                else
                {
                    msg = $"修改商品分类【{category.Name}】";
                    if (category.Name != oldCategory.Name)
                        msg += $" 名称由【{oldCategory.Name}】变为【{category.Name}】";
                }
            }

            logService.Create(LogModule.ProductCategory, msg);
        }


        /// <summary>
        /// 验证
        /// </summary>
        /// <param name="editor"></param>
        public void Verify(CategoryEditor editor)
        {
            if(string.IsNullOrWhiteSpace(editor.Name))
                throw new MyException("分类名称不能为空");
            if (categoryService.Count(p => p.Name == editor.Name && p.ParentId == editor.ParentId && p.Id != editor.Id) > 0)
                throw new MyException("已经存在了相同名称的分类");
        }

        /// <summary>
        /// 移除分类
        /// </summary>
        /// <param name="id"></param>
        public MessageModel Remove(long id)
        {
           var category = categoryService.GetById(id);
            CategoryLog(category,isDelete:true);
            if (categoryService.Count(p => p.ParentId == id) > 0)
                throw new MyException("分类存在下级不能删除");
            categoryService.DeleteById(id);
            categoryService.RemoveCache();
            return SuccessResult();
        }

        /// <summary>
        /// 修改是否显示
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="display">是否显示</param>
        public MessageModel Display(long id, bool display)
        {
            var category = categoryService.GetById(id);
            logService.Create(LogModule.ProductCategory, $"隐藏商品分类【{category.Name}】");
            categoryService.Display(id, display);
            return SuccessResult();
        }

        /// <summary>
        /// 修改排序
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sequence"></param>
        public MessageModel Sequence(long id, int sequence) 
        {
            categoryService.Sequence(id, sequence);
           return SuccessResult();
        } 

        /// <summary>
        /// 查询分类
        /// </summary>
        /// <param name="categorId">父Id</param>
        /// <param name="Depth">深度</param>
        /// <returns></returns>
        public List<Category> Categorys(long categorId = 0, int depth =int.MaxValue)
        {
            var categorys = categoryService.List();
            var list = categorys.Where(p => p.ParentId == categorId).OrderByDescending(p => p.Sequence).ToList();
            foreach (var item in list)
            {
                if (item.Depth <= depth)
                {
                    if(categorys.Where(p => p.ParentId == item.Id).Count() > 0)
                        item.Items = Categorys(item.Id, depth);
                }
            }
            return list;
        }

        /// <summary>
        /// 分类
        /// </summary>
        /// <param name="categorId"></param>
        /// <param name="depth"></param>
        /// <returns></returns>
        public MessageModel List(long categorId = 0, int depth = int.MaxValue) => SuccessResult(Categorys(categorId, depth));



        /// <summary>
        /// 查询详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Category Details(long id) => categoryService.Details(id);

       

    }
}
