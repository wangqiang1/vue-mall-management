import request from '@/utils/request'

//角色列表
export const RoleList = (): any => request({ url: 'role/list' })
//获取角色菜单
export const RoleDetails = (data: { id: number }): any => request({ url: 'role/details', data, loading: true })
//保存角色
export const SaveRole = (data: any): any => request({ url: 'role/save', data, loading: true, method: 'POST' })

//删除角色
export const DeleteRole = (data: { id: number }): any => request({ url: 'role/delete', data, loading: true })  