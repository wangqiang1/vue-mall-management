using Framework.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wq.Core.Common;
using Wq.Core.Common.Helper;
using Wq.Core.Extensions.Assembly;

using Wq.Core.Extensions.ServiceExtensions;
using Wq.Tasks;

namespace Wq.Core.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(new Appsettings(Configuration));
            services.AddControllers();
            services.AddSwagger();
            services.AddFramework();
            services.AddLogDashboardSetup();
            services.AddAutoMapperSetup();
            services.AddRedisCacheSetup();
            services.AddNewtonsoftJsonSetup();
            services.AddEasyQuartzSetup();
            services.AddRabbitMQSetup();
            services.AddSqlSugar();
            services.AddCorsSetup();
            services.AddJwtSetup();
            services.AddHttpContext();
            services.AddHCAPSetup();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                
            }
            app.UseMySwagger();
            app.UseStaticFiles();
            app.UseFileServer();
            app.UseLogMyDashboard();
            app.UseCors(Appsettings.app(new string[] { "Startup", "Cors", "PolicyName" }));
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication();//身份验证
            app.UseAuthorization();
            app.UseHttpContext();
            app.UseMiddleware(typeof(ExceptionHandlerMiddleware));
            app.UseTask();
            //启动任务框架
            DefaultContainer.SetRoot(app.ApplicationServices);
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        
    }
}
