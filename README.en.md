# vue商城管理

#### Description
个人学习

#### Software Architecture
Software architecture description

#### Installation

1.  无需任何其他安装，下载开箱即用
2.  前端代码安装和运行在在前端有单独说明

#### Instructions

1.  注意如果要用到Rabbit的延迟队列，需要先去Rabbit官网下载dll并更新到对应文件夹，Rabbit默认安装时不带延迟队列的


#### Contribution

测试账号：admin 密码：123456

部分截图
![输入图片说明](%E6%88%AA%E5%9B%BE/%E9%80%89%E6%8B%A9%E7%B4%A0%E6%9D%90%E7%BB%84%E4%BB%B6.png)
![输入图片说明](%E6%88%AA%E5%9B%BE/%E7%B4%A0%E6%9D%90.png)
![输入图片说明](%E6%88%AA%E5%9B%BE/%E6%A6%82%E5%86%B5.png)
![输入图片说明](%E6%88%AA%E5%9B%BE/%E6%9D%83%E9%99%90%E7%BB%84.png)
![输入图片说明](%E6%88%AA%E5%9B%BE/%E5%95%86%E5%93%81%E5%88%86%E7%B1%BB.png)


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
