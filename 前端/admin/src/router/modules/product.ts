import { RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/product/list',
    name: 'ProductList',
    component: () => import('@/views/product/List.vue'),
    meta: {
      title: '商品列表'
    }
  },
  {
    path: '/product/category',
    name: 'ProductCategory',
    component: () => import('@/views/product/Category.vue'),
    meta: {
      title: '商品分类'
    }
  },
  {
    path: '/product/spec',
    name: 'ProductSpec',
    component: () => import('@/views/product/Spec.vue'),
    meta: {
      title: '商品规格'
    }
  },
  {
    path: '/product/attribute',
    name: 'ProductAttribute',
    component: () => import('@/views/product/Attribute.vue'),
    meta: {
      title: '商品属性'
    }
  },
  {
    path: '/product/attribute/item',
    name: 'ProductAttributeItem',
    component: () => import('@/views/product/AttributeItem.vue'),
    meta: {
      title: '商品属性'
    }
  },
  {
    path: '/product/tag',
    name: 'ProductTag',
    component: () => import('@/views/product/Tag.vue'),
    meta: {
      title: '商品标签'
    }
  },
  {
    path: '/product/service',
    name: 'ProductService',
    component: () => import('@/views/product/Service.vue'),
    meta: {
      title: '商品服务'
    }
  },
  {
    path: '/product/brand',
    name: 'ProductBrand',
    component: () => import('@/views/product/Brand.vue'),
    meta: {
      title: '商品品牌'
    }
  },
  {
    path: '/product/create',
    name: 'CreateProduct',
    component: () => import('@/views/product/CreateProduct.vue'),
    meta: {
      title: '创建商品'
    }
  },
  {
    path: '/product/freight',
    name: 'FreightTemplate',
    component: () => import('@/views/product/FreightTemplate.vue'),
    meta: {
      title: '运费模板'
    }
  },
  {
    path: '/product/freight/Create',
    name: 'CreateFreightTemplate',
    component: () => import('@/views/product/CreateFreightTemplate.vue'),
    meta: {
      title: '创建运费模板'
    }
  },
]
export default routes