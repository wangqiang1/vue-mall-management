﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.Model
{

   
    /// <summary>
    /// 缓存键 索引
    /// </summary>
    public static class CacheKey
    {
       static string Site = "wq";
        /// <summary>
        /// 全部菜单缓存
        /// </summary>
        public static string Menu = $"{Site}:menu";

        /// <summary>
        /// 本地Token 
        /// </summary>
        public static string RefreshToken(string token) => $"{Site}:refreshtoken{token}";

        /// <summary>
        /// 商品分类
        /// </summary>
        public static string Categories = $"{Site}:categories";


        /// <summary>
        /// 文件分组
        /// </summary>
        public static string FileCategory(int type) => $"{Site}:filecategory{type}";

        /// <summary>
        /// 商品标签
        /// </summary>
        public static string Tags = $"{Site}:tags";

        /// <summary>
        /// 商品规格
        /// </summary>
        public static string Specs = $"{Site}:Specs";

        /// <summary>
        /// 地区
        /// </summary>
        public static string Regions(int maxLevel) => $"{Site}:regions{maxLevel}";

        /// <summary>
        /// 层级关系
        /// </summary>
        /// <param name="maxLevel"></param>
        /// <returns></returns>
        public static string RegionToLevel(int level) => $"{Site}:regionstolevel{level}";


        /// <summary>
        /// 运费模板
        /// </summary>
        public static string FreightTemplate = $"{Site}:freighttemplate";

        /// <summary>
        /// 商品属性
        /// </summary>
        public static string Attribute = $"{Site}:attribute";

        /// <summary>
        /// 商品服务
        /// </summary>
        public static string Service = $"{Site}:service";
    }
}
