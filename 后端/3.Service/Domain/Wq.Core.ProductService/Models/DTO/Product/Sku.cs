﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    /// <summary>
    /// 商品SKU
    /// </summary>
    public class Sku 
    {
        /// <summary>
        /// SKUID
        /// </summary>
        public string Id
        {
            get; set;
        }

        /// <summary>
        /// 规格图
        /// </summary>
        public string Thumbnail
        {
            get; set;
        }
        /// <summary>
        /// 规格货号
        /// </summary>
        public string Code
        {
            get; set;
        }
        /// <summary>
        /// 成本价
        /// </summary>
        public decimal CostPrice
        {
            get; set;
        }
        /// <summary>
        /// 销售价
        /// </summary>
        public decimal SalePrice
        {
            get; set;
        }

        /// <summary>
        /// 重量
        /// </summary>
        public decimal Weight
        {
            get; set;
        }

        /// <summary>
        /// 规格描述
        /// </summary>
        public string SkuDesc { get; set; }

        /// <summary>
        /// 库存
        /// </summary>
        public int Stock { get; set; }



    }
}
