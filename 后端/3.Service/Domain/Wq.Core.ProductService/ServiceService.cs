﻿
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.Extensions.Assembly.SqlSugar;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;
using Wq.Core.ProductService.Models.DTO.Attribute;

namespace Wq.Core.ProductService
{
   /// <summary>
   /// 商品服务
   /// </summary>
    public class ServiceService : BaseRepository<ServiceInfo>, IRegister
    {
        private readonly BaseRepository redisRepository;

        public ServiceService(BaseRepository redisRepository) => this.redisRepository = redisRepository;

        /// <summary>
        /// 编辑服务
        /// </summary>
        /// <param name="tag"></param>
        public void Save(ServiceEditor service)
        {
            Context.Updateable<ServiceInfo>()
            .SetColumns(p => p.Name == service.Name)
            .SetColumns(p => p.Icon == service.Icon)
            .SetColumns(p => p.Description == service.Description)
            .Where(p => p.Id == service.Id).ExecuteCommand();
            RemoveCache();
        }
            

        public List<ServiceView> List(PageModel<ServiceQuery> pageModel, ref int count) =>
           Context.Queryable<ServiceInfo>()
           .WhereIF(SqlFunc.IsNullOrEmpty(pageModel.Entity.Name), p => p.Name.Contains(pageModel.Entity.Name))
           .Mapper(p => { })
           .Select<ServiceView>()
           .ToPageList(pageModel.Page.Index, pageModel.Page.Size, ref count);

        /// <summary>
        /// 获取全部服务
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<long, string>> Services() => redisRepository.Get(Model.CacheKey.Service, () => base.GetList().ToDictionary(p => p.Id, v => v.Name).ToList()).Result;

        /// <summary>
        /// 清除缓存
        /// </summary>
        public void RemoveCache() => redisRepository.Remove(Model.CacheKey.Service);
    }
}
