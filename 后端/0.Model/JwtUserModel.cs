﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.Model
{
    /// <summary>
    /// 用户Model
    /// </summary>
    public class JwtUserModel
    {
        /// <summary>
        /// UserId
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 角色
        /// </summary>
        public string UserRole { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime UserBirthDay { get; set; }
    }
}
