﻿using DotNetCore.CAP;
using System;
using Wq.Core.Application;
using Wq.Core.Extensions.ServiceExtensions.Framework;

namespace Wq.Handler
{
    public class RegionHandler : ICapSubscribe, IRegister
    {


        private readonly RegionApplication application;

        public  RegionHandler(RegionApplication application) =>this.application =application;

        /// <summary>
        /// 创建文件
        /// </summary>
        [CapSubscribe("save.region.file")]
        public void SaveFile(string content = "") => application.SaveFile();
    }
}
