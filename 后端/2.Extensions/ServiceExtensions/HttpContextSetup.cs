﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions.Assembly;

namespace Wq.Core.Extensions.ServiceExtensions
{
    /// <summary>
    /// 上下文注册
    /// </summary>
    public static class HttpContextSetup
    {
        public static void AddHttpContext(this IServiceCollection services)
        {
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        public static void UseHttpContext(this IApplicationBuilder app)
        {
            HttpContextHelper.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>()); //配置HttpContext上下文
        }
    }

}
