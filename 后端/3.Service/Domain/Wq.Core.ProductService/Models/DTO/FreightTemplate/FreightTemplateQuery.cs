﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    /// <summary>
    /// 运费模板
    /// </summary>
    public class FreightTemplateQuery
    {
        /// <summary>
        /// 运费模板名称
        /// </summary>
        public string Name { get; set; }
    }
}
