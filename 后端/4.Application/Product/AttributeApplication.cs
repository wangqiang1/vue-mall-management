﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Common.Helper;
using Wq.Core.Extensions;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.Model;
using Wq.Core.PlatAdminService;
using Wq.Core.PlatAdminService.Models;
using Wq.Core.ProductService;
using Wq.Core.ProductService.Models;
using Wq.Core.ProductService.Models.DTO;
using Wq.Core.ProductService.Models.DTO.Attribute;

namespace Wq.Core.Application.Product
{
    public class AttributeApplication : BaseApplication
    {
        private readonly BaseRepository redisRepository;
        private readonly AttributeService attributeService;
        private readonly AttributeItemService attributeItemService;
        private readonly LogService logService;
        private readonly CategoryService categoryService;
        public AttributeApplication(
               AttributeService attributeService,
               AttributeItemService attributeItemService,
               LogService logService,
               CategoryService categoryService,
               BaseRepository redisRepository
             )
        {
            this.attributeService = attributeService;
            this.attributeItemService = attributeItemService;
            this.logService = logService;
            this.categoryService = categoryService;
            this.redisRepository = redisRepository;
        }
        #region 属性
        /// <summary>
        /// 添加商品属性
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public MessageModel Save(AttributeEditor attribute)
        {
           var old = attributeService.Details(attribute.Id);
            Verify(attribute);
            if (attribute.Id > 0)
                attributeService.Save(attribute);
            else
                attributeService.Create(attribute);
            AttributeLog(old,attribute);
  
            return SuccessResult();
        }
       
        /// <summary>
        /// 验证商品属性
        /// </summary>
        /// <param name="editor"></param>
        public void Verify(AttributeEditor editor)
        {
            if (editor.Id == 0 && attributeService.Count(p => p.Name == editor.Name) > 0)
                throw new MyException("属性已经存在");
            if (editor.Id > 0 && attributeService.Count(p => p.Name == editor.Name && p.Id != editor.Id) > 0)
                throw new MyException("属性已经存在");
            if(editor.Categorys.Count > 0 && categoryService.Count(p => editor.Categorys.Contains(p.Id)) < editor.Categorys.Distinct().Count())
                throw new MyException("分类不存在");
        }

        /// <summary>
        /// 删除属性
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MessageModel Remove(long id)
        {
            var old = attributeService.Details(id);
            attributeService.Delete(id);
            return SuccessResult();
        }

        /// <summary>
        /// 属性日志
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="oldAttribute"></param>
        public void AttributeLog(AttributeEditor oldAttribute, AttributeEditor attribute = null, bool isDelete = false)
        {
            string msg = string.Empty;
            if (isDelete)
                msg = $"删除商品属性【{oldAttribute.Name}】";
            else
            {

                if (attribute.Id == 0)
                {
                    msg = $"新增商品属性【{attribute.Name}】";
                    if (attribute.Categorys.Count > 0)
                        msg += $",关联分类【{string.Join(",", attribute.Categorys)}】";
                }
                else
                {
                    msg = $"修改商品属性【{oldAttribute.Name}】";
                    if (attribute.Name != oldAttribute.Name)
                        msg += $" 名称由【{oldAttribute.Name}】变为【{attribute.Name}】";
                    if (attribute.Categorys.Count > 0)
                    {
                        if(string.Join(",", attribute.Categorys) != string.Join(",", oldAttribute.Categorys))
                            msg += $" 关联分类由【{categoryService.GetName(oldAttribute.Categorys)}】变为【{categoryService.GetName(attribute.Categorys)}】";
                    }
                    else {
                        msg += $",关联分类被删除";
                    }
                }
            }

            logService.Create(LogModule.ProductAttribute, msg);
        }

        /// <summary>
        /// 属性详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MessageModel Details(long id) => SuccessResult(attributeService.Details(id));

        public MessageModel List(PageModel<AttributeQuery> pageModel)
        {
            int count = 0;
            return SuccessResult(attributeService.List(pageModel, ref count), count);
        }

        /// <summary>
        /// 获取属性列表
        /// </summary>
        /// <returns></returns>
        public MessageModel Attributes() => SuccessResult(attributeService.Attributes());
      
        /// <summary>
        /// 修改排序
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Sequence"></param>
        public MessageModel UpdateSequence(long id, long sequence) {
            attributeService.UpdateSequence(id, sequence);
              return SuccessResult();
        } 
        #endregion

        #region 属性项

        /// <summary>
        /// 属性项添加
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public MessageModel Save(AttributeItemEditor attribute)
        {
           var old = attributeItemService.Details(attribute.Id);
            Verify(attribute);
            if (attribute.Id > 0)
                attributeItemService.Save(attribute);
            else
                attributeItemService.Create(attribute);

            AttributeItemLog(old, attribute);
            attributeService.RemoveCache();
            return SuccessResult();
        }

        /// <summary>
        /// 项验证
        /// </summary>
        /// <param name="editor"></param>
        public void Verify(AttributeItemEditor editor)
        {

            if (editor.Id == 0 && attributeItemService.Count(p => p.Name == editor.Name) > 0)
                throw new MyException("属性项已经存在");
            if (editor.Id > 0 && attributeItemService.Count(p => p.Name == editor.Name && p.Id != editor.Id) > 0)
                throw new MyException("属性项已经存在");
            if (editor.Values?.Count > 0)
            {
                if (editor.Values.Distinct().Count() != editor.Values.Count())
                    throw new MyException("存在相同的属性值");
            }
        }

        /// <summary>
        /// 删除属性项
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MessageModel DeleteItem(long id)
        {
           var old = attributeItemService.Details(id);
            attributeItemService.Delete(id);
            AttributeItemLog(old, isDelete: true);
            attributeService.RemoveCache();
            return SuccessResult();
        }

        /// <summary>
        /// 属性日志
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="oldAttribute"></param>
        public void AttributeItemLog(AttributeItemView oldAttribute, AttributeItemEditor attribute = null, bool isDelete = false)
        {
            string msg = string.Empty;
            if (isDelete)
                msg = $"删除商品属性项【{oldAttribute.Name}】";
            else
            {

                if (attribute.Id == 0)
                    msg = $"新增商品属性项【{attribute.Name}】";
                else
                {
                    msg = $"修改商品属性项【{oldAttribute.Name}】";
                    if (attribute.Name != oldAttribute.Name)
                        msg += $" 名称由【{oldAttribute.Name}】变为【{attribute.Name}】";
                    if(attribute.TypeId != oldAttribute.TypeId)
                        msg += $" 类型由【{oldAttribute.TypeId.ToDescription()}】变为【{attribute.TypeId.ToDescription()}】";
                    if(JsonConvert.SerializeObject(oldAttribute.Values) != JsonConvert.SerializeObject(attribute.Values))
                        msg += $" 值由【{JsonConvert.SerializeObject(oldAttribute.Values)}】变为【{JsonConvert.SerializeObject(attribute.Values)}】";
                }
            }

            logService.Create(LogModule.ProductAttributeItem, msg);
        }

        /// <summary>
        /// 属性项列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public MessageModel List(PageModel<AttributeItemQuery> pageModel)
        {
            int count = 0;
            return SuccessResult(attributeItemService.List(pageModel, ref count), count);
        }

        /// <summary>
        /// 获取详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MessageModel ItemDetails(long id) => SuccessResult(attributeItemService.Details(id));

        /// <summary>
        /// 修改排序
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Sequence"></param>
        public MessageModel UpdateItemSequence(long id, long sequence)
        {
            attributeItemService.UpdateSequence(id, sequence);
            attributeService.RemoveCache();
            return SuccessResult();
        }

       /// <summary>
       /// 删除规格值
       /// </summary>
       /// <param name="itemId"></param>
       /// <param name="value"></param>
       /// <returns></returns>
        public MessageModel DeleteValue(long itemId, string value)
        {
            var info = attributeItemService.GetById(itemId);
            logService.Create(LogModule.ProductAttributeItem, $"删除商品属性项【{info.Name}】的值:【{value}】");
            attributeItemService.DeleteValue(itemId, value);
            attributeService.RemoveCache();
            return SuccessResult();
        }

        /// <summary>
        /// 通过属性Id获取属性列表
        /// </summary>
        /// <param name="attributeId"></param>
        /// <returns></returns>
        public MessageModel AttributeItems(long attributeId) => SuccessResult(attributeItemService.AttributeItems(attributeId));
        #endregion
    }
}
