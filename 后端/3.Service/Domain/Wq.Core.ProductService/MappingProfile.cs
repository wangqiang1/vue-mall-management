﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.ProductService.Models;
using Wq.Core.ProductService.Models.DTO.Attribute;

namespace Wq.Core.ProductService
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            //商品属性
            CreateMap<AttributeEditor, AttributeInfo>().ReverseMap();
            CreateMap<AttributeItemEditor, AttributeItemInfo>().ReverseMap();
            //商品分类
            CreateMap<CategoryEditor, CategoryInfo>().ReverseMap();
            //商品标签
            CreateMap<TagEditor, TagInfo>().ReverseMap();
            //商品规格
            CreateMap<SpecEditor, SpecInfo>().ReverseMap();
            //商品服务
            CreateMap<ServiceEditor, ServiceInfo>().ReverseMap();
            //商品品牌
            CreateMap<BrandEditor, BrandInfo>().ReverseMap();
            ///运费模板
            CreateMap<FreightTemplateEditor, FreightTemplateInfo>().ReverseMap();
            //运费模板组
            CreateMap<FreightTemplateEditor.FreightTemplateGroupEditor, FreightGroupInfo>().ReverseMap();

            //商品
            CreateMap<ProductEditor, ProductInfo>().ReverseMap();
            CreateMap<SkuEditor, ProductSkuInfo>().ReverseMap();
            CreateMap<ProductAttribute, ProductAttributeInfo>().ReverseMap();
            CreateMap<SkuSpec, SkuSpecInfo>().ReverseMap();



        }
    }
}
