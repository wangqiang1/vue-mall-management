﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.FileService.Models
{
    public class FileQuery
    {
        /// <summary>
        /// 类型
        /// </summary>
        public FileType Type { get; set; }

        /// <summary>
        /// 分类
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string Name { get; set; }
    }
}
