﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    /// <summary>
    /// 运费模板
    /// </summary>
    public class FreightTemplateEditor
    {
        /// <summary>
        ///模板ID
        /// </summary>
        public int Id
        {
            get; set;
        }
        /// <summary>
        /// 模板名称
        /// </summary>
        public string Name
        {
            get; set;
        }

        /// <summary>
        /// 计费方式
        /// </summary>
        public FreightMethod Method
        {
            get; set;
        }

        /// <summary>
        /// 分组
        /// </summary>
        public List<FreightTemplateGroupEditor> Groups
        {
            get; set;
        }

        /// <summary>
        /// 商品数
        /// </summary>
        public int ProductNum
        {
            get; set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyDate
        {
            get; set;
        }

        /// <summary>
        /// 排除的地区
        /// </summary>
        public List<int> ExcludeRegions { get; set; }

        /// <summary>
        /// 排序地区明文
        /// </summary>
        public string ExcludeRegionText { get; set; }

        public class FreightTemplateGroupEditor
        {
            /// <summary>
            /// 分组ID
            /// </summary>
            public int Id
            {
                get; set;
            }
            /// <summary>
            /// 运费模板Id
            /// </summary>
            public int TemplateId
            {
                get; set;
            }

            /// <summary>
            /// 首件/重
            /// </summary>
            public decimal DefaultNumber
            {
                get; set;
            }

            /// <summary>
            /// 首费
            /// </summary>
            public decimal DefaultPrice
            {
                get; set;
            }

            /// <summary>
            /// 续件/重
            /// </summary>
            public decimal AddNumber
            {
                get; set;
            }

            /// <summary>
            /// 续费
            /// </summary>
            public decimal AddPrice
            {
                get; set;
            }

            /// <summary>
            /// 分组包含的区域
            /// </summary>
            public List<int> Regions
            {
                get; set;
            }

            /// <summary>
            /// 区域文本
            /// </summary>

            public string RegionText

            {

                get; set;

            }
        }

    
    }
}
