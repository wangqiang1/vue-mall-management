﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions;
using Wq.Core.FileService;
using Wq.Core.Model;
using Wq.Core.PlatAdminService;
using Wq.Core.PlatAdminService.Models;
using Wq.Core.ProductService;
using Wq.Core.ProductService.Models;

namespace Wq.Core.Application.Product
{
    /// <summary>
    /// 运费模板 
    /// </summary>
    public class FreightTemplateApplication : BaseApplication
    {
        private readonly FreightTemplateService service;
        private readonly Wq.Core.ProductService.ProductService productservice;
        private readonly RegionService regionService;
        private readonly LogService logService;
        public FreightTemplateApplication(FreightTemplateService service, RegionService regionService, ProductService.ProductService productservice, LogService logService)
        {
            this.service = service;
            this.regionService = regionService;
            this.productservice = productservice;
            this.logService = logService;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public MessageModel List(PageModel<FreightTemplateQuery> pageModel)
        {
            int count = 0;
            var list = service.list(pageModel, ref count);
            var regions =  regionService.list(3);
            var ids = list.Select(p => p.Id).Distinct().ToList();
            var freightTemplateDictionary = productservice.GetFreightTemplateDictionary(ids);
            list.ForEach(item => {

                if (freightTemplateDictionary.ContainsKey(item.Id.ToString()))
                    item.ProductNum = int.Parse(freightTemplateDictionary[item.Id.ToString()].ToString());
                else
                    item.ProductNum = 0;

                item.Groups.ForEach(group => {
                   var itemRegions =  regions.Where(p => group.Regions.Contains(p.id)).Select(p => p.name).ToList();
                    group.RegionText = String.Join(" ", itemRegions);
                });
                if (item.ExcludeRegions?.Count > 0)
                {
                    var itemRegions = regions.Where(p => item.ExcludeRegions.Contains(p.id)).Select(p => p.name).ToList();
                    item.ExcludeRegionText = String.Join(" ", itemRegions);
                }
            });

            return SuccessResult(list);
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="tag"></param>
        public MessageModel Save(FreightTemplateEditor freightTemplate)
        {
            Verify(freightTemplate);
            var old = service.Details(freightTemplate.Id);
            if (freightTemplate.Id == 0)
                service.Create(freightTemplate);
            else
            {
                service.Save(freightTemplate);
            }
            Log(old, freightTemplate);

            return SuccessResult();
        }
        private void Verify(FreightTemplateEditor freightTemplate)
        {
            if (string.IsNullOrEmpty(freightTemplate.Name))
                throw new MyException("模板名称不能为空");
            
        }
        /// <summary>
        /// 属性日志
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="oldAttribute"></param>
        public void Log(FreightTemplateEditor old, FreightTemplateEditor freightTemplate = null, bool isDelete = false)
        {
            string msg = string.Empty;
            if (isDelete)
                msg = $"删除商品运费模板【{old.Name}】";
            else
            {

                if (freightTemplate.Id == 0)
                    msg = $"新增运费模板【{freightTemplate.Name}】";
                else
                {
                    msg = $"修改运费模板【{freightTemplate.Name}】";
                    if (freightTemplate.Name != old.Name)
                        msg += $" 名称由【{old.Name}】变为【{freightTemplate.Name}】";

                   

                }
            }

            logService.Create(LogModule.ProductFreightTemplate, msg);
        }

        public MessageModel Remove(int id)
        {
            if (productservice.Count(p => p.FreightTemplateId == id && !p.IsDelete) > 0)
                throw new MyException("该模板绑定了商品不能删除");
            var old = service.Details(id);
            service.DeleteById(id);
            Log(old, isDelete: true);
            service.RemoveCache();
            return SuccessResult();
        }
        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MessageModel Details(int id)
        {
            var info = service.Details(id);
            var regions = regionService.list(3);
            info.Groups.ForEach(group => {
                var itemRegions = regions.Where(p => group.Regions.Contains(p.id)).Select(p => p.name).ToList();
                group.RegionText = String.Join(" ", itemRegions);
            });
            if (info.ExcludeRegions?.Count > 0)
            {
                var itemRegions = regions.Where(p => info.ExcludeRegions.Contains(p.id)).Select(p => p.name).ToList();
                info.ExcludeRegionText = String.Join(" ", itemRegions);
            }

            return SuccessResult(info);

        }

        /// <summary>
        /// 获取运费模板
        /// </summary>
        /// <returns></returns>
        public MessageModel FreightTemplates()
        {
            var list = service.FreightTemplates().Select(p => new { Key = int.Parse(p.Key),p.Value}).ToList();

            return SuccessResult(list);

        }
    }
}
