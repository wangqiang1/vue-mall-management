﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    /// <summary>
    /// 商品规格
    /// </summary>
    public class SkuSpec
    {
		/// <summary>
		/// id
		/// </summary>
        public long Id { get; set; }
		/// <summary>
		/// 规格Id
		/// </summary>

		public long SpecId { get; set; }
		/// <summary>
		/// 规格项Id
		/// </summary>

		public long SpecValueId { get; set; }
		/// <summary>
		/// 规格名
		/// </summary>

		public string Name { get; set; }
		/// <summary>
		/// 规格值
		/// </summary>

		public string Value { get; set; }
		/// <summary>
		/// 附加项
		/// </summary>

		public string Data { get; set; }
		/// <summary>
		/// 展示形式
		/// </summary>
		public SpecType TypeId { get; set; }
	}
}
