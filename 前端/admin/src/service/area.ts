import request from '@/utils/request'

//获取下级
export const Subs = (data:any): any => request({ url: 'file/region/subs', data, method: 'GET' })
//获取下级
export const GetList = (data:any): any => request({ url: 'file/region/list', data, method: 'GET' })
///添加
export const CreateArea= (data:any): any => request({ url: 'file/region/create', data,  method: 'GET' })
///编辑
export const SaveArea= (data:any): any => request({ url: 'file/region/save', data,  method: 'GET' })
///删除
export const RemoveArea= (data:any): any => request({ url: 'file/region/remove', data,  method: 'GET' })

//获取地区文件
export const AreaFile = (): any => request({ url: 'file/region/file', method: 'GET' })