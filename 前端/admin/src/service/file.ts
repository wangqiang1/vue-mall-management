import request from '@/utils/request'

//分类列表
export const Categorys = (data:any): any => request({ url: 'file/oss/category/list',data, method: 'GET' })
//编辑分类
export const CategorySave = (data: any): any => request({ url: 'file/oss/category/save', data, method: 'POST' })
//删除分类
export const RemoveCategory = (data:any) :any => request({ url: 'file/oss/category/remove',data, method: 'GET' })
// 上传图片
export const UploadImage = (data:any) :any => request({ url: 'file/oss/image/upload',data, method: 'post', contentType: 'file' })
//列表
export const FileList = (data: any): any => request({ url: 'file/oss/list', data, method: 'POST' })
//修改名称
export const SetName = (data: any): any => request({ url: 'file/oss/setname', data, method: 'GET' })
//编辑分组
export const Setgroup = (data: any): any => request({ url: 'file/oss/setgroup', data, method: 'POST' })
//删除文件
export const RemoveFile = (data: any): any => request({ url: 'file/oss/remove', data, method: 'POST' })

// 上传视频
export const UploadVideo = (data:any) :any => request({ url: 'file/oss/video/upload',data, method: 'post', contentType: 'file' })