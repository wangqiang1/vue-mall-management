﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlSugar;
using SqlSugar.IOC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Common;

namespace Wq.Core.Extensions.ServiceExtensions
{
    /// <summary>
    /// SqlSugar注册
    /// </summary>
    public static class SqlSugarSetup
    {
        public static void AddSqlSugar(this IServiceCollection services)
        {
            var list = Appsettings.app<string[]>("ConnectionStrings:Entities");

            foreach (var item in list)
            {
                services.AddSqlSugar(new IocConfig()
                {
                    ConfigId = item[0],
                    ConnectionString = item[1],
                    DbType = IocDbType.MySql,
                    IsAutoCloseConnection = true
                });
            }

            services.ConfigurationSugar(db =>
            {
                foreach (var item in list)
                {
                    db.GetConnection(item[0]).Aop.OnLogExecuting = (sql, p) =>
                    {
                        Console.WriteLine(sql);
                    };

                }
            });
        }
    }
}
