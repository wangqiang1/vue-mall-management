﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.Extensions.Assembly.SqlSugar;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;

namespace Wq.Core.ProductService
{
    public class CategoryService : BaseRepository<CategoryInfo>, IRegister
    {

        private readonly BaseRepository redisRepository;

        public CategoryService(BaseRepository redisRepository) => this.redisRepository = redisRepository;

        /// <summary>
        /// 编辑 
        /// </summary>
        /// <param name="category"></param>
        public void Save(CategoryEditor category) => Context.Updateable<CategoryInfo>().SetColumns(p => p.Name == category.Name).SetColumns(p => p.Icon == category.Icon).SetColumns(p => p.Sequence == category.Sequence).Where(p => p.Id == category.Id).ExecuteCommand();


        /// <summary>
        /// 修改是否显示
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="display">是否显示</param>
        public void Display(long id, bool display)
        {
            Context.Updateable<CategoryInfo>().SetColumns(p => p.Display == display).Where(p => p.Id == id).ExecuteCommand();
            RemoveCache();
        }

        /// <summary>
        /// 修改排序
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sequence"></param>
        public void Sequence(long id, int sequence)
        {
            Context.Updateable<CategoryInfo>().SetColumns(p => p.Sequence == sequence).Where(p => p.Id == id).ExecuteCommand();
            RemoveCache();
        }

        /// <summary>
        /// 全部分类
        /// </summary>
        /// <returns></returns>
        public List<Category> List() => redisRepository.Get(CacheKey.Categories,() => Context.Queryable<CategoryInfo>().OrderBy(p => p.Sequence, SqlSugar.OrderByType.Desc).Select<Category>().ToList()).Result ;


        /// <summary>
        /// 查询详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Category Details(long id) => Context.Queryable<CategoryInfo>().Select<Category>().First(p => p.Id == id);

        /// <summary>
        /// 通过id集合获取名称
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public string GetName(List<long> ids)
        {
           var names = List().Where(p => ids.Contains(p.Id)).Select(p => p.Name).ToList();
            return string.Join(",", names);
        }
        /// <summary>
        /// 通过id获取名称
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public string GetName(long id) => List().Where(p => p.Id == id).FirstOrDefault()?.Name;

        /// <summary>
        /// 移除缓存
        /// </summary>
        /// <param name="type"></param>
        public void RemoveCache() => redisRepository.Remove(CacheKey.Categories);
    }
}
