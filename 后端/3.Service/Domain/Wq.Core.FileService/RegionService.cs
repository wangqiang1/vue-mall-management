﻿using SqlSugar;
using System.Collections.Generic;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.Extensions.Assembly.SqlSugar;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.FileService.Models;
using Wq.Core.Model;

namespace Wq.Core.FileService
{
    /// <summary>
    /// 地区管理
    /// </summary>
    public class RegionService : BaseRepository<RegionInfo>, IRegister
    {
        private readonly BaseRepository redisRepository;
        public RegionService(BaseRepository redisRepository) => this.redisRepository = redisRepository;

        /// <summary>
        /// 获取全部数据
        /// </summary>
        public List<RegionData> list(int maxLevel = 4) => redisRepository.Get(Model.CacheKey.Regions(maxLevel), () => Context.Queryable<RegionInfo>().Where(p => p.Level <= maxLevel).Select<RegionData>().ToList()).Result;

        /// <summary>
        /// 获取下级集合
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public List<RegionData> Subs(int parentId) => 
            Context.Queryable<RegionInfo>()
            .Where(p => p.ParentId == parentId).Select<RegionData>()
            .Mapper(p => { 
                p.hasChildren = Context.Queryable<RegionInfo>().Where(r => r.ParentId == p.id).Count() > 0;
            }).ToList();


        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parentId"></param>
        public void Create(string name, int parentId)
        {
            var info = Context.Queryable<RegionInfo>().First(p => p.Id == parentId);
            Context.Insertable(new RegionInfo { Name = name, ParentId = parentId, Level = info?.Level ?? 1 }).ExecuteCommand();
            RemoveCache();
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public void Save(int id, string name)
        {
            Context.Updateable<RegionInfo>().SetColumns(p => p.Name == name).Where(p => p.Id == id).ExecuteCommand();
            RemoveCache();

        }
        /// <summary>
        /// 移除缓存
        /// </summary>
        public async void RemoveCache()
        {
            for (int i = 1; i <= 4; i++)
            {
                await redisRepository.Remove(Model.CacheKey.Regions(i));
                await redisRepository.Remove(Model.CacheKey.RegionToLevel(i));
            }
        }

       
    }
}
