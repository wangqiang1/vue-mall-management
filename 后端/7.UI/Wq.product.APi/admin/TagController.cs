﻿using Framework;
using Microsoft.AspNetCore.Mvc;
using Wq.Core.Application.Product;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;

namespace Wq.product.APi.admin
{
    /// <summary>
    /// 商品标签
    /// </summary>
    [Route("admin/product/tag")]
    public class TagController : ProductAdminController
    {
        private readonly TagApplication tagApplication;
        /// <summary>
        /// 商品标签
        /// </summary>
        public TagController(TagApplication tagApplication)
        {
            this.tagApplication = tagApplication;
        }


        /// <summary>
        /// 商品标签列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        [HttpPost("list")]
        public MessageModel List(PageModel<TagQuery> pageModel) => tagApplication.List(pageModel);

        /// <summary>
        /// 添加商品标签
        /// </summary>
        /// <param name="tag">标签</param>
        /// <returns></returns>
        [HttpPost("save")]
        public MessageModel Save(TagEditor tag) => tagApplication.Save(tag);

        /// <summary>
        /// 删除商品标签
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("remove")]
        public MessageModel Remove(long id) => tagApplication.Remove(id);

        /// <summary>
        /// 修改序号
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sequence"></param>
        /// <returns></returns>
        [HttpGet("sequence")]
        public MessageModel UpdateSequence(long id, long sequence) => tagApplication.UpdateSequence(id, sequence);

        /// <summary>
        /// 全部标签
        /// </summary>
        /// <returns></returns>
        [HttpGet("tags")]
        public MessageModel Tags() => tagApplication.Tags();

    }
}
