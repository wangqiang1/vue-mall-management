# vue商城管理

#### 介绍
个人学习

#### 软件架构
软件架构说明


#### 安装教程

1.  无需任何其他安装，下载开箱即用
2.  前端代码安装和运行在在前端有单独说明

#### 使用说明

1.  注意如果要用到Rabbit的延迟队列，需要先去Rabbit官网下载dll并更新到对应文件夹，Rabbit默认安装时不带延迟队列的，下载地址：https://github.com/rabbitmq/rabbitmq-delayed-message-exchange/releases （注意要和Rabbit版本对应上）
2.  使用 CAP + RabbitMQ + MySql 完成事件的发布与订阅，可以查看面板 （注意对应的数据库需要新建，空的就行 数据名称看appsettings.json文件）
![输入图片说明](%E6%88%AA%E5%9B%BE/CAP.png)
3.由Quart改成Hangfire可视化任务管理
![输入图片说明](%E6%88%AA%E5%9B%BE/Hangfire.png)

测试站点：http://43.139.248.61/admin/ （CentOS 7.6 64bit，docker-compose 部署）
测试账号：admin 密码：123456
部分截图
![输入图片说明](%E6%88%AA%E5%9B%BE/%E5%AF%8C%E6%96%87%E6%9C%AC%E6%A1%86.png)
![输入图片说明](%E6%88%AA%E5%9B%BE/%E5%AF%8C%E6%96%87%E6%9C%AC%E6%A1%86%E9%80%89%E6%8B%A9%E5%9B%BE%E7%89%87.png)
![输入图片说明](%E6%88%AA%E5%9B%BE/%E5%95%86%E5%93%81%E5%88%86%E7%B1%BB.png)
![输入图片说明](%E6%88%AA%E5%9B%BE/%E6%9D%83%E9%99%90%E7%BB%84.png)
![输入图片说明](%E6%88%AA%E5%9B%BE/%E6%A6%82%E5%86%B5.png)
![输入图片说明](%E6%88%AA%E5%9B%BE/%E7%B4%A0%E6%9D%90.png)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
