﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Common.Helper;
using Wq.Core.Extensions.ServiceExtensions.Framework;

namespace Wq.Core.Extensions.ServiceExtensions
{
   public static class FrameworkSetup
    {
        /// <summary>
        /// 注册服务
        /// </summary>
        /// <param name="services"></param>
        public static void AddFramework(this IServiceCollection services)
        {
            services.AddModule();
        }
    }
}
