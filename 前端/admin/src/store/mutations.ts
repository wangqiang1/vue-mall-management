import { MutationTree } from "vuex"

const Mutations: MutationTree<any> = {
  SET_TOKEN(state: any, data: any): void {
    localStorage.token = data.accessToken
    state.token = data.accessToken
    localStorage.refreshToken = data.refreshToken
    state.refreshToken = data.refreshToken
    localStorage.userName = data.userName
    state.userName = data.userName
  },
  SET_QUIT(state: any): void {
    localStorage.token = ''
    state.token = ''
    localStorage.refreshToken = ''
    state.refreshToken = ''
    localStorage.userName = ''
    state.userName = ''
  },
  SET_MEUNS(state: any, menus: any): void {
    state.menus = menus
  },
  SET_LOADING(state: any, loading: boolean): void {
    state.loading = loading
  },
  SET_CURRENTPAGE(state: any, currentPage: any): void {
    state.currentPage = currentPage
  },
  SET_SETTING(state: any, setting: any): void {
    state.setting = setting
  },
}

export default Mutations