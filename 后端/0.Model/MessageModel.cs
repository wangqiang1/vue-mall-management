﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.Model
{
   public class MessageModel
    {
        /// <summary>
        /// 操作是否成功
        /// </summary>
        public bool Success { get; set; } = true;
        /// <summary>
        /// 返回信息
        /// </summary>
        public string Msg { get; set; } 
        /// <summary>
        /// 返回数据集合
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// 条数
        /// </summary>
        public int? Count { get; set; }

    }
}
