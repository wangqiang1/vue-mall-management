﻿using SqlSugar;
using SqlSugar.IOC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.Extensions.Assembly.SqlSugar
{
   public class BaseRepository<T> : SimpleClient<T> where T : class, new()
    {
        protected ITenant itenant = null;//多租户事务 
        public BaseRepository(ISqlSugarClient context = null) : base(context)//注意这里要有默认值等于null
        {
            var configId = typeof(T).GetCustomAttribute<TenantAttribute>().configId;
            base.Context =  DbScoped.SugarScope.GetConnectionScope(configId);
            if(base.Context == null)
             base.Context = DbTransient.Sugar.GetConnectionScope(configId);
            itenant = DbScoped.SugarScope;//设置租户接口
        }

    }
}
