﻿using Framework;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Application.Product;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;


namespace Wq.product.APi
{
    /// <summary>
    /// 商品品牌
    /// </summary>
    [Route("admin/product/brand")]
    public class BrandController : ProductAdminController
    {
        private readonly BrandApplication brandApplication;

        /// <summary>
        /// 商品品牌
        /// </summary>
        public BrandController(BrandApplication brandApplication) => this.brandApplication = brandApplication;

        /// <summary>
        /// 添加（编辑）品牌
        /// </summary>
        /// <param name="brand">分类实体</param>
        /// <returns></returns>
        [HttpPost("save")]
        public MessageModel Save(BrandEditor brand) => brandApplication.Save(brand);

        /// <summary>
        /// 删除品牌
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("remove")]
        public MessageModel Remove(long id) => brandApplication.Remove(id);

        /// <summary>
        /// 品牌列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        [HttpPost("list")]
        public MessageModel List(PageModel<BrandQuery> pageModel) => brandApplication.List(pageModel);

        /// <summary>
        /// 修改序号
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sequence"></param>
        /// <returns></returns>
        [HttpGet("sequence")]
        public MessageModel UpdateSequence(long id, long sequence) => brandApplication.UpdateSequence(id, sequence);
    }
}
