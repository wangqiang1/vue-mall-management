// 更新Token倒计时
export const updateTokenExpired = (): void => {
  const interval = setInterval(() => {
    let countdown = parseInt(localStorage.expired)
    console.log(countdown)
    countdown--
    if (countdown <= 0)
      clearInterval(interval)
    else
      localStorage.expired = countdown.toString()
  }, 1000)
}

export function deepClone(source:any) {
  if (!source && typeof source !== 'object') {
    throw new Error('error arguments')
  }
  const targetObj = <any>(source.constructor === Array ? [] : {})
  Object.keys(source).forEach((keys:any) => {
    if (source[keys] && typeof source[keys] === 'object') {
      targetObj[keys] = deepClone(source[keys])
    } else {
      targetObj[keys] = source[keys]
    }
  })
  return targetObj
}

