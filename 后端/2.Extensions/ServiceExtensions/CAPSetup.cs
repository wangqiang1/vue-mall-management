﻿

using Microsoft.Extensions.DependencyInjection;
using Wq.Core.Common;
using Wq.Core.Extensions.Assembly.CAP;

namespace Wq.Core.Extensions.ServiceExtensions
{
    public static class CAPSetup
    {
        public static void AddHCAPSetup(this IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>();

            services.AddCap(x =>
            {
                x.UseEntityFramework<AppDbContext>();
                x.UseRabbitMQ(options =>
                {
                    options.HostName = Appsettings.app("Rabbit:HostName");
                    options.UserName = Appsettings.app("Rabbit:UserName");
                    options.Password = Appsettings.app("Rabbit:Password");
                    options.ExchangeName = Appsettings.app("Rabbit:Exchange");
                });
                x.UseDashboard();
                x.FailedRetryCount = 5;
                x.FailedThresholdCallback = failed =>
                {

                };
            });
        }
    }
}
