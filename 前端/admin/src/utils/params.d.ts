declare namespace params {
  export interface IPageModel {
    orderbyName: string
    isAsc: boolean
    beginDateTime: string
    endDateTime: string
    entity: any
  }
}
