﻿using Microsoft.Extensions.DependencyModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.Common.Helper
{
    public  class RuntimeHelper
    {
        /// <summary>
        /// 获取项目程序集，排除所有的系统程序集(Microsoft.***、System.***等)、Nuget下载包
        /// </summary>
        /// <returns></returns>
        public static IList<Assembly> GetAllAssemblies()
        {
            var list = new List<Assembly>();
            var deps = DependencyContext.Default;
            var libs = deps.CompileLibraries.Where(lib => !lib.Serviceable && lib.Type != "package").ToList();//排除所有的系统程序集、Nuget下载包
            foreach (var lib in libs)
            {
                try
                {
                    var assembly = AssemblyLoadContext.Default.LoadFromAssemblyName(new AssemblyName(lib.Name));
                    list.Add(assembly);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            return list;
        }
         
        private static List<Type> projectTypes;
        public static List<Type> GetProjectTypes()
        {
            if (projectTypes == null)
            {
                projectTypes = new List<Type>();
                foreach (var assembly in GetProjectAssembly())
                {
                    var typeInfos = assembly.DefinedTypes;
                    foreach (var typeInfo in typeInfos)
                    {
                        projectTypes.Add(typeInfo.AsType());
                    }
                }
            }
            return projectTypes;
        }

        /// <summary>
        /// 获取项目程序集
        /// </summary>
        /// <returns></returns>
        public static List<Assembly> GetProjectAssembly(string filter = null)
        {
            var result = new List<Assembly>();
            var projects = DependencyContext.Default.CompileLibraries.Where(p => p.Type == "project").ToList();
            foreach (var project in projects)
            {
                if (!string.IsNullOrEmpty(filter) && !project.Name.Contains(filter))
                    continue;
                var assembly = AssemblyLoadContext.Default.LoadFromAssemblyName(new AssemblyName(project.Name));
                result.Add(assembly);
            }
            return result;
        }

    }
}
