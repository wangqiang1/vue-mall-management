﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    public class AttributeQuery
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
