﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.Extensions.Assembly.SqlSugar;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;
using CacheKey = Wq.Core.Model.CacheKey;

namespace Wq.Core.ProductService
{
    /// <summary>
    /// 规格 
    /// </summary>
    public class SpecService : BaseRepository<SpecInfo>, IRegister
    {

        private readonly BaseRepository redisRepository;

        public SpecService(BaseRepository redisRepository) => this.redisRepository = redisRepository;


        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="item"></param>
        public void Create(SpecEditor spec)
        {
            var info = spec.Map<SpecInfo>();
            Context.Ado.UseTran(() => {
                info.Id = Context.Insertable(info).ExecuteReturnBigIdentity();
                var values = spec.Values.Select(p => new SpecValueInfo { SpecId = info.Id, Value = p }).ToList();
                Context.Insertable(values).ExecuteCommandAsync();
            });
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="item"></param>
        public void Save(SpecEditor spec)
        {
            var info = spec.Map<SpecInfo>();
            Context.Ado.UseTran(() => {
                Context.Updateable(info).ExecuteCommand();
                Context.Deleteable<SpecValueInfo>().Where(p => p.SpecId == info.Id).ExecuteCommand();
                var values = spec.Values.Select(p => new SpecValueInfo { SpecId = info.Id, Value = p }).ToList();
                Context.Insertable(values).ExecuteCommandAsync();
            });
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        public void Remove(long id)
        {
            Context.Ado.UseTran(() => {
                Context.Deleteable<SpecInfo>().Where(p => p.Id == id).ExecuteCommand();
                Context.Deleteable<SpecValueInfo>().Where(p => p.SpecId == id).ExecuteCommand();
            });
        }

        /// <summary>
        /// 删除值
        /// </summary>
        /// <param name="id"></param>
        public void RemoveValue(long id) => Context.Deleteable<SpecValueInfo>().Where(p => p.Id == id).ExecuteCommand();

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public List<SpecView> List(PageModel<TagQuery> pageModel, ref int count) =>
           Context.Queryable<SpecInfo>()
           .WhereIF(!SqlFunc.IsNullOrEmpty(pageModel.Entity.Name), p => p.Name.Contains(pageModel.Entity.Name) || p.Alias.Contains(pageModel.Entity.Name))
           .Select<SpecView>()
           .Mapper(p => {
               p.Values = Context.Queryable<SpecValueInfo>().Where(w => w.SpecId == p.Id).Select(w => new SpecView.SpecValue { Value = w.Value, Id = w.Id }).ToList();
           })
           .OrderBy(pageModel.GetOrderbyString())
           .ToPageList(pageModel.Page.Index, pageModel.Page.Size, ref count);

        /// <summary>
        /// 获取编辑详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SpecEditor Details(long id) => Context.Queryable<SpecInfo>().Where(p => p.Id == id).Select<SpecEditor>()
           .Mapper(p =>
           {
               p.Values = Context.Queryable<SpecValueInfo>().Where(w => w.SpecId == p.Id).Select(w => w.Value).ToList();
           }).First();


        /// <summary>
        /// 全部规格
        /// </summary>
        /// <returns></returns>
        public List<SpecView> Specs() => redisRepository.Get(CacheKey.Specs, () => Context.Queryable<SpecInfo>().Select<SpecView>().ToList()).Result;

        /// <summary>
        /// 获取规格详情
        /// </summary>
        /// <param name="specId"></param>
        /// <returns></returns>
        public List<SpecView.SpecValue> SpecValues(long specId) => Context.Queryable<SpecValueInfo>().Where(p => p.SpecId == specId).Select<SpecView.SpecValue>().ToList();


        /// <summary>
        /// 移除缓存
        /// </summary>
        public void RemoveCache() => redisRepository.Remove(CacheKey.Specs);

        
    }
}
