﻿using Framework;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Application.Product;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;

namespace Wq.product.APi.admin
{
    /// <summary>
    /// 商品规格
    /// </summary>
    [Route("admin/product/spec")]
    public class SpecController : ProductAdminController
    {
        private readonly SpecApplication specApplication;

        /// <summary>
        /// 商品规格
        /// </summary>
        public SpecController(SpecApplication specApplication) => this.specApplication = specApplication;

        /// <summary>
        /// 品牌列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        [HttpPost("list")]
        public MessageModel List(PageModel<TagQuery> pageModel) => specApplication.List(pageModel);

        /// <summary>
        /// 添加（编辑）规格
        /// </summary>
        /// <param name="brand">分类实体</param>
        /// <returns></returns>
        [HttpPost("save")]
        public MessageModel Save(SpecEditor spec) => specApplication.Save(spec);

        /// <summary>
        /// 删除规格
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("remove")]
        public MessageModel Remove(long id) => specApplication.Remove(id);

        /// <summary>
        /// 删除规格值
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("value/remove")]
        public MessageModel RemoveValue(long id) => specApplication.RemoveValue(id);
        /// <summary>
        /// 删除规格
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("details")]
        public MessageModel Details(long id) => specApplication.Details(id);

        /// <summary>
        /// 获取全部规格
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("specs")]
        public MessageModel Specs() => specApplication.Specs();

        /// <summary>
        /// 获取全部规格
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("values")]
        public MessageModel SpecValues(long specId) => specApplication.SpecValues(specId);

        /// <summary>
        /// 添加规格值
        /// </summary>
        /// <param name="specId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet("createvalue")]
        public MessageModel CreateSpecValue(long specId, string value) => specApplication.CreateSpecValue(specId,value);


        /// <summary>
        /// 创建规格
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("createSpec")]
        public MessageModel CreateSpec(string name) => specApplication.CreateSpec(name);
    }
}
