﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    public class Product
    {
        /// <summary>
        /// 商品Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 促销语
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// 分享描述
        /// </summary>
        public string SharingDescription { get; set; }

		/// <summary>
		/// 首张主图(冗余)
		/// </summary>

		public string Thumbnail { get; set; }
		/// <summary>
		/// 主视频
		/// </summary>

		public string Video { get; set; }
		/// <summary>
		/// 主视频封面
		/// </summary>

		public string VideoCover { get; set; }
		/// <summary>
		/// 是否有规格
		/// </summary>

		public bool HasSku { get; set; }
		/// <summary>
		/// 销售价
		/// </summary>

		public decimal SalePrice { get; set; }
		/// <summary>
		/// 划线价
		/// </summary>

		public decimal MarkingPrice { get; set; }
		/// <summary>
		/// 成本价
		/// </summary>

		public decimal CostPrice { get; set; }
		/// <summary>
		/// 商品状态
		/// </summary>

		public int Status { get; set; }
		/// <summary>
		/// 是否启用会员价
		/// </summary>

		public bool IsMemberPrice { get; set; }
		/// <summary>
		/// 运费模板
		/// </summary>

		public int FreightTemplateId { get; set; }
		/// <summary>
		/// 固定运费
		/// </summary>

		public decimal Freight { get; set; }
		/// <summary>
		/// 创建时间
		/// </summary>

		public DateTime CreateTime { get; set; }
		/// <summary>
		/// 商品总库存
		/// </summary>

		public int Stock { get; set; }
		/// <summary>
		/// 真实销量
		/// </summary>

		public int SaleCount { get; set; }
		/// <summary>
		/// 虚拟销量
		/// </summary>

		public int VirtualSaleCount { get; set; }
		/// <summary>
		/// 定时上架时间
		/// </summary>

		public DateTime? OnSaleTime { get; set; }
		/// <summary>
		/// 上架操作
		/// </summary>

		public OnSaleOperation Operation { get; set; }
		/// <summary>
		/// 排序序号
		/// </summary>

		public int Sequence { get; set; }
		/// <summary>
		/// 是否已删除
		/// </summary>

		public bool IsDelete { get; set; }
		/// <summary>
		/// 属性模板
		/// </summary>

		public long AttributeId { get; set; }
		/// <summary>
		/// 单位
		/// </summary>

		public string Unit { get; set; }
		
		/// <summary>
		/// 限购类型 0 不限购 1常期限购 2 短期限购
		/// </summary>
		public LimitType LimitType { get; set; }
		/// <summary>
		/// 限购数量
		/// </summary>

		public int LimitNum { get; set; }
		/// <summary>
		/// 最小购买数量
		/// </summary>

		public int MinBuy { get; set; }
		/// <summary>
		/// 定时下架时间
		/// </summary>

		public DateTime? OffSaleTime { get; set; }
	}
}
