﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SqlSugar;
using Wq.Core.Common.Helper;
using Wq.Core.Extensions;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.Extensions.Assembly.SqlSugar;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;


namespace Wq.Core.ProductService
{
    public class AttributeItemService : BaseRepository<AttributeItemInfo>, IRegister
    {
        /// <summary>
        /// 创建属性项 
        /// </summary>
        /// <param name="manager"></param>
        public void Create(AttributeItemEditor item)
        {
            var info = item.Map<AttributeItemInfo>();
            Context.Ado.UseTran(() => {
                info.Id = Context.Insertable(info).ExecuteReturnIdentity();
                if(item.Values?.Count > 0)
                {
                    var values =  item.Values.Select(p => new AttributeValueInfo { AttributeId = info.AttributeId, ItemId = info.Id, Value = p }).ToList();
                    Context.Insertable(values).ExecuteCommand();
                }
            });
        }

        /// <summary>
        /// 修改属性项
        /// </summary>
        /// <param name="item"></param>
        public void Save(AttributeItemEditor item)
        {
            var info = item.Map<AttributeItemInfo>();
            Context.Ado.UseTran(() => {
                Context.Updateable(info).ExecuteCommand();
                if (item.Values?.Count > 0)
                {
                   var oldValues = Context.Queryable<AttributeValueInfo>().Where(p => p.ItemId == item.Id).ToList(); 
                   var addValues = item.Values.Where(p => !oldValues.Select(p => p.Value).Contains(p)).ToList();
                   var deleteValues = oldValues.Where(p => !item.Values.Contains(p.Value)).ToList();

                    if (deleteValues.Count > 0)
                        Context.Deleteable(deleteValues).ExecuteCommand();

                    if (addValues.Count > 0)
                    {
                       var adds = addValues.Select(p => new AttributeValueInfo { AttributeId = info.AttributeId, ItemId = info.Id, Value = p }).ToList();
                        Context.Insertable(adds).ExecuteCommand();
                    }
                }
            });
        }

        public void Delete(long id)
        {
            Context.Ado.UseTran(() => {
                Context.Deleteable<AttributeItemInfo>().Where(p => p.Id == id).ExecuteCommand();
                Context.Deleteable<AttributeValueInfo>().Where(p => p.ItemId == id).ExecuteCommand();
            });
        }

        /// <summary>
        /// 属性项列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public List<AttributeItemView> List(PageModel<AttributeItemQuery> pageModel, ref int count)
        {
           return Context.Queryable<AttributeItemInfo>().Where(p => p.AttributeId == pageModel.Entity.AttributeId)
                 .WhereIF(!SqlFunc.IsNullOrEmpty(pageModel.Entity.Name), p => p.Name.Contains(pageModel.Entity.Name))
                 .Select<AttributeItemView>()
                 .Mapper(p => {
                     p.Values = Context.Queryable<AttributeValueInfo>().Where(v => v.ItemId == p.Id).Select(p => p.Value).ToList();
                 })
                 .OrderBy(pageModel.GetOrderbyString())
                 .ToPageList(pageModel.Page.Index, pageModel.Page.Size, ref count);
        }

        /// <summary>
        /// 获取详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public AttributeItemView Details(long id) => Context.Queryable<AttributeItemInfo>().Where(p => p.Id == id).Select<AttributeItemView>().Mapper(p => { p.Values = Context.Queryable<AttributeValueInfo>().Where(v => v.ItemId == p.Id).Select(p => p.Value).ToList(); }).First();

        /// <summary>
        /// 修改排序
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Sequence"></param>
        public void UpdateSequence(long id, long Sequence) => Context.Updateable<AttributeItemInfo>().SetColumns(p => p.Sequence == Sequence).Where(p => p.Id == id).ExecuteCommand();

        /// <summary>
        /// 删除规格值
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="value"></param>
        public void DeleteValue(long itemId, string value) => Context.Deleteable<AttributeValueInfo>().Where(p => p.ItemId == itemId && p.Value == value).ExecuteCommand();

        public string GetValue(AttributeType typeId, string value)
        {
            if (typeId == AttributeType.Text) return value;
            else
            {
                var valueIds = value.Split(',').Select(p => long.Parse(p)).ToList();
               var values = Context.Queryable<AttributeValueInfo>().Where(p => valueIds.Contains(p.Id)).Select(p => p.Value).ToList();
                return string.Join(",", values);
            }
        }

        public string GetItemName(List<long> itemIds)
        {
            var list = Context.Queryable<AttributeItemInfo>().Where(p => itemIds.Contains(p.Id)).Select(p => p.Name).ToList();
            return String.Join(",", list);
        }


        /// <summary>
        /// 通过属性Id获取属性列表
        /// </summary>
        /// <param name="attributeId"></param>
        /// <returns></returns>
        public List<ProductAttribute> AttributeItems(long attributeId)
        {
          var values = Context.Queryable<AttributeValueInfo>().Where(p => p.AttributeId == attributeId).ToList();

          return  Context.Queryable<AttributeItemInfo>().Where(p => p.AttributeId == attributeId).OrderByDescending(p => p.Sequence).Select<ProductAttribute>(p =>  new ProductAttribute { AttributeId  = p.AttributeId, AttributeItemId  = p.Id, TypeId = (AttributeType)p.TypeId, Name = p.Name })
           .Mapper(p => {
               p.Values = values.Where(v => v.ItemId == p.AttributeItemId).Select(p => p.Value).ToList();
           })
           .ToList();
        }
         
    }
}
