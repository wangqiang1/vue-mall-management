import request from '@/utils/request'

//管理员列表
export const ManagerList = (data: any): any => request({ url: 'manager/list', data, method: 'POST' })
//编辑管理员
export const SaveManager = (data: any): any => request({ url: 'manager/save', data, method: 'POST' })
//删除管理员
export const RemoveManager = (data: any): any => request({ url: 'manager/remove', data, method: 'GET' })
