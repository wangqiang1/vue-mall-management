﻿using Framework.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SqlSugar.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions.Assembly.Jwt;

namespace Framework
{
    [Authorize("Manager")]
    [ApiExplorerSettings(GroupName = "manager")]
    public abstract class PlatAdminController : AdminController
    {
        
    } 
}
