import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus'
import locale from "element-plus/lib/locale/lang/zh-cn"//需要新加的代码
import Grid from '@/components/Grid.vue'
import 'element-plus/dist/index.css'
import '@/assets/styles/index.scss'
import '@/assets/styles/iconfont.css'
import Permission from '@/utils/permission'


const app = createApp(App)
app.config.globalProperties.$filters = Permission

app.use(ElementPlus, { locale })
app.use(store)
app.use(router).mount('#app')
app.component('Grid', Grid)