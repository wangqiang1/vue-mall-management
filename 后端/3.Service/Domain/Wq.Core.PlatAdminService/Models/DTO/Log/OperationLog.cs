﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.PlatAdminService.Models
{
    /// <summary>
    /// 日志
    /// </summary>
    public class OperationLog
    {
        /// <summary>
        /// 操作人标识号
        /// </summary>
        public long Operator
        {
            get; set;
        }
        /// <summary>
        /// 操作人名
        /// </summary>
        public string OperatorName
        {
            get; set;
        }
        /// <summary>
        /// 模块
        /// </summary>
        public LogModule Module
        {
            get; set;
        }
        /// <summary>
        /// 消息
        /// </summary>
        public string Message
        {
            get; set;
        }
        public string OperatorAddress
        {
            get;
            set;
        }
    }
}
