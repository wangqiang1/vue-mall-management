﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.Common.Helper
{

    /// <summary>
    /// 加密帮助
    /// </summary>
    public class EncryptionHelper
    {
        /// <summary>
        /// MD5散列
        /// </summary>
        public static string MD5(string inputStr)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            var hashByte = md5.ComputeHash(Encoding.UTF8.GetBytes(inputStr));
            var sb = new StringBuilder();
            foreach (var item in hashByte)
                sb.Append(item.ToString("x").PadLeft(2, '0'));
            return sb.ToString();
        }

        /// <summary>
        /// 生成 密码盐
        /// </summary>
        /// <returns></returns>
        public static string BuildSalt() => Guid.NewGuid().ToString().Replace("-", string.Empty);

        /// <summary>
        /// 获取 密码二次加密 密文
        /// </summary>
        public static string GetPasswrodWithTwiceEncode(string password, string salt)
        {
            var encryptedPassword = MD5(password); 
            var encryptedWithSaltPassword = MD5(encryptedPassword + salt); 
            return encryptedWithSaltPassword;


        }
    }
}
