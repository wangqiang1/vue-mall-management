﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.PlatAdminService.Models
{
    public class PrivilegeView
    {

        /// <summary>
        /// 页面Id
        /// </summary>
        public int Route { get; set; }

        /// <summary>
        /// 权限Id
        /// </summary>
        public int Privilege { get; set; }
    }
}
