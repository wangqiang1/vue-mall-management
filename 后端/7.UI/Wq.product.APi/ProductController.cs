﻿using Framework;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Application.Manager;
using Wq.Core.Model;

namespace Wq.product.APi
{
    /// <summary>
    /// 商品管理  
    /// </summary>
    [Route("product")]
    public class ProductController : ProductAdminController
    {
        private readonly ManagerApplication managerApplication;
        public ProductController(ManagerApplication managerApplication)
        {
            this.managerApplication = managerApplication;
        }

        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("remove")]
        public MessageModel Delete(long id) => managerApplication.RemoveManager(id);
    }
}
