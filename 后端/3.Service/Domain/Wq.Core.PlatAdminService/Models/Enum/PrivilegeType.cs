﻿using System.ComponentModel;


namespace Wq.Core.PlatAdminService.Models
{
    /// <summary>
    /// 操作项
    /// </summary>
    public enum PrivilegeType
    {
        /// <summary>
        /// 查看
        /// </summary>
        [Description("查看")]
        View = 1001,
        /// <summary>
        /// 添加
        /// </summary>
        [Description("添加")]
        Add = 1002,
        /// <summary>
        /// 编辑
        /// </summary>
        [Description("编辑")]
        Edit = 1003,
        /// <summary>
        /// 删除
        /// </summary>
        [Description("删除")]
        Delete = 1004,
        /// <summary>
        /// 管理
        /// </summary>
        [Description("管理")]
        Administer = 1005,
        


    }
}
