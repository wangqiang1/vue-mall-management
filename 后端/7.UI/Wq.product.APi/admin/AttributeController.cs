﻿using Framework;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Application.Product;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;


namespace Wq.product.APi
{
    /// <summary>
    /// 商品属性
    /// </summary>
    [Route("admin/product/attribute")]
    public class AttributeController : ProductAdminController
    {
        private readonly AttributeApplication attributeApplication;

        /// <summary>
        /// 商品属性
        /// </summary>
        public AttributeController(AttributeApplication attributeApplication) => this.attributeApplication = attributeApplication;

        /// <summary>
        /// 添加属性
        /// </summary>
        /// <param name="attribute">属性类</param>
        /// <returns></returns>
        [HttpPost("save")]
        public MessageModel Save(AttributeEditor attribute) => attributeApplication.Save(attribute);

        /// <summary>
        /// 删除属性
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("remove")]
        public MessageModel Remove(long id) => attributeApplication.Remove(id);

        /// <summary>
        /// 获取权限组
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("details")]
        public MessageModel Details(long id) => attributeApplication.Details(id);

        /// <summary>
        /// 属性列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        [HttpPost("list")]
        public MessageModel List(PageModel<AttributeQuery> pageModel) => attributeApplication.List(pageModel);

        /// <summary>
        /// 修改序号
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sequence"></param>
        /// <returns></returns>
        [HttpGet("sequence")]
        public MessageModel UpdateSequence(long id, long sequence) => attributeApplication.UpdateSequence(id, sequence);

        /// <summary>
        /// 添加属性项
        /// </summary>
        /// <param name="attribute">属性项类</param>
        /// <returns></returns>
        [HttpPost("item/save")]
        public MessageModel Save(AttributeItemEditor attribute) => attributeApplication.Save(attribute);

        /// <summary>
        /// 删除属性
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("item/remove")]
        public MessageModel DeleteItem(long id) => attributeApplication.DeleteItem(id);

        /// <summary>
        /// 属性列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        [HttpPost("item/list")]
        public MessageModel List(PageModel<AttributeItemQuery> pageModel) => attributeApplication.List(pageModel);

        /// <summary>
        /// 获取详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("item/details")]
        public MessageModel ItemDetails(long id) => attributeApplication.ItemDetails(id);

       /// <summary>
       /// 删除规格值
       /// </summary>
       /// <param name="itemId"></param>
       /// <param name="value"></param>
       /// <returns></returns>
        [HttpGet("item/remove/Value")]
        public MessageModel DeleteValue(long itemId, string value) => attributeApplication.DeleteValue(itemId, value);

        /// <summary>
        /// 获取属性列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("attributes")]
        public MessageModel Attributes() => attributeApplication.Attributes();

        /// <summary>
        /// 通过属性Id获取属性列表
        /// </summary>
        /// <param name="attributeId"></param>
        /// <returns></returns>
        [HttpGet("attributeItems")]
        public MessageModel AttributeItems(long attributeId) => attributeApplication.AttributeItems(attributeId);
    }
}
