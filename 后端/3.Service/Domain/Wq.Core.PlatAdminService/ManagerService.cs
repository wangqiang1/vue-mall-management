﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SqlSugar;
using Wq.Core.Common.Helper;
using Wq.Core.Extensions;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.Extensions.Assembly.SqlSugar;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.Model;
using Wq.Core.PlatAdminService.Models;

namespace Wq.Core.PlatAdminService
{
    public  class ManagerService : BaseRepository<ManagerInfo>, IRegister
    {
        /// <summary>
        /// 创建管理员
        /// </summary>
        /// <param name="manager"></param>
        public void Create(ManagerEditor manager)
        {
            manager.PasswordSalt = EncryptionHelper.BuildSalt();
            manager.Password = EncryptionHelper.GetPasswrodWithTwiceEncode(manager.Password, manager.PasswordSalt);
          
            var info = manager.Map<ManagerInfo>();
            info.CreateDate = DateTime.Now;

            var result = Context.Ado.UseTran(() => {
                info.Id = Context.Insertable(info).ExecuteReturnIdentity();
                Context.Updateable<RoleInfo>().SetColumns(p => p.Number == p.Number + 1).Where(p => p.Id == manager.RoleId).ExecuteCommand();
            });
            //result.IsSuccess;
        }
        /// <summary>
        /// 修改管理员
        /// </summary>
        /// <param name="manager"></param>
        public void Save(ManagerEditor manager)
        {
            var old = Context.Queryable<ManagerInfo>().First(p => p.Id == manager.Id);
            var dt = Context.Updateable<ManagerInfo>().SetColumns(p => p.RoleId == manager.RoleId).Where(p => p.Id == manager.Id);
            if (!string.IsNullOrEmpty(manager.Password))
            {
                manager.PasswordSalt = EncryptionHelper.BuildSalt();
                manager.Password = EncryptionHelper.GetPasswrodWithTwiceEncode(manager.Password, manager.PasswordSalt);
                dt.SetColumns(p => p.PasswordSalt == manager.PasswordSalt);
                dt.SetColumns(p => p.Password == manager.Password);

                Context.Updateable<RoleInfo>().SetColumns(p => p.Number == p.Number + 1).Where(p => p.Id == manager.RoleId).ExecuteCommand();
                Context.Updateable<RoleInfo>().SetColumns(p => p.Number == p.Number - 1).Where(p => p.Id == old.RoleId).ExecuteCommand();
            }
            dt.ExecuteCommand();
        }

        /// <summary>
        /// 删除管理员
        /// </summary>
        /// <param name="id"></param>

        public void Delete(long id)
        {
           var manager = Context.Queryable<ManagerInfo>().First(p => p.Id == id);
            Context.Ado.UseTran(() => {
                Context.Deleteable(manager).ExecuteCommand();
                Context.Updateable<RoleInfo>().SetColumns(p => p.Number == p.Number - 1).Where(p => p.Id == manager.RoleId).ExecuteCommand();
            });
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public List<ManagerView> List(PageModel<ManagerQuery> pageModel, ref int count)
        {
          return  Context.Queryable<ManagerInfo>().LeftJoin<RoleInfo>((p ,r) => p.RoleId == r.Id)
                .WhereIF(!SqlFunc.IsNullOrEmpty(pageModel.Entity.UserName), p => p.UserName.Contains(pageModel.Entity.UserName))
                .WhereIF(SqlFunc.HasNumber(pageModel.Entity.RoleId), P => P.RoleId == pageModel.Entity.RoleId.Value)
                .Select<ManagerView>()
                .Mapper(c => {
                    if (c.RoleId == 0) c.Name = "超级管理员";
                })
                .ToPageList(pageModel.Page.Index, pageModel.Page.Size, ref count);
        }

        public ManagerEditor Details(long id)=>
             Context.Queryable<ManagerInfo>().Where(p => p.Id == id).Select<ManagerEditor>().Mapper(p => {
                 if (p.RoleId == 0)
                     p.RoleName = "超级管理员";
                 else
                     p.RoleName = Context.Queryable<RoleInfo>().Where(c => c.Id == p.RoleId).First().Name;
             }).First();


    }
}
