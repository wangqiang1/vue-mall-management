﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    /// <summary>
    /// 品牌查询
    /// </summary>
    public class BrandQuery
    {
        /// <summary>
        /// 属性名称
        /// </summary>
        public string Name { get; set; }
    }
}
