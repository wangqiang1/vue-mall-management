﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    /// <summary>
    /// 添加分类
    /// </summary>
    public class CategoryEditor
    {
        /// <summary>
        /// 分类Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 上级
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sequence { get; set; }
    }
}
