﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Extensions.Assembly.SqlSugar;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;

namespace Wq.Core.ProductService
{
    public class ProductService : BaseRepository<ProductInfo>, IRegister
    {
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="spec"></param>
        public void Create(ProductEditor product)
        {
            var info = product.Map<ProductInfo>();
            Context.Ado.UseTran(() => {
                #region 商品信息
                info.Thumbnail = product.Thumbnails[0];
                product.Id = info.Id = Context.Insertable(info).ExecuteReturnBigIdentity();
                #endregion

                #region 商品标签
                if (product.Tags?.Count > 0)
                {
                    var tags = product.Tags.Select(p => new ProductTagInfo { ProductId = product.Id, TagId = p }).ToList();
                    Context.Insertable(tags).ExecuteCommand();
                }
                #endregion

                #region 商品分类
                if (product.Categorys?.Count > 0)
                {
                    var categorys = product.Categorys.Select(p => new ProductCategoryInfo { ProductId = product.Id, CategoryId = p }).ToList();
                    Context.Insertable(categorys).ExecuteCommand();
                }
                #endregion

                #region 商品图片
                var thumbnails = product.Thumbnails.Select(p => new ProductThumbnailInfo() { ProductId = info.Id, FilePath = p }).ToList();
                Context.Insertable(thumbnails).ExecuteCommand();
                #endregion

                #region 商品规格与规格值
                List<SkuSpecInfo> addspecs = new List<SkuSpecInfo>();
                product.Skus.ForEach(sku => {
                    sku.Id = product.Id.ToString();
                    if (sku.Spec.Count > 0)
                    {
                        sku.Spec.ForEach(item =>
                        {
                            sku.Id += $"_{item.SpecValueId}";
                            if (item.TypeId == SpecType.Image || item.TypeId == SpecType.TextAndImage)
                                sku.Thumbnail = item.Data;
                            else
                                sku.Thumbnail = string.Empty;

                        });
                        #region 商品规格值信息
                        var specs = sku.Spec.Map<SkuSpecInfo>();
                        specs.ForEach(item => { item.ProductId = product.Id; item.SkuId = sku.Id; });
                        addspecs.AddRange(specs);
                        #endregion

                    }
                    else
                        sku.Id = $"{sku.Id}_0_0_0";
                });

                var skus = product.Skus.Map<ProductSkuInfo>();
                skus.ForEach(sku => sku.ProductId = product.Id);

                Context.Insertable(info).ExecuteCommand();
                if(addspecs.Count > 0)
                Context.Insertable(addspecs).ExecuteCommand();
                #endregion

                #region 商品属性
                if (product.Attributes.Count > 0)
                {
                    var attributes = product.Attributes.Map<ProductAttributeInfo>();
                    attributes.ForEach(attribute => attribute.ProductId = product.Id);
                    Context.Insertable(attributes).ExecuteCommand();
                }
                #endregion

                #region 商品服务
                if (product.Services.Count > 0)
                {
                  var services = product.Services.Select(p => new ProductServiceInfo { ProductId = product.Id, ServiceId = p }).ToList();
                    Context.Insertable(services).ExecuteCommand();
                }
                #endregion

            });
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="product"></param>
        public void Save(ProductEditor product)
        {
            var info = product.Map<ProductInfo>();
            Context.Ado.UseTran(() => {
                #region 商品信息
                info.Thumbnail = product.Thumbnails[0];
                Context.Updateable(info).ExecuteCommand();
                #endregion

                #region 删除信息
                Context.Deleteable<ProductThumbnailInfo>().Where(p => p.ProductId == product.Id).ExecuteCommand();
                Context.Deleteable<SkuSpecInfo>().Where(p => p.ProductId == product.Id).ExecuteCommand();
                Context.Deleteable<ProductSkuInfo>().Where(p => p.ProductId == product.Id).ExecuteCommand();
                Context.Deleteable<ProductServiceInfo>().Where(p => p.ProductId == product.Id).ExecuteCommand();
                Context.Deleteable<ProductTagInfo>().Where(p => p.ProductId == product.Id).ExecuteCommand();
                Context.Deleteable<ProductCategoryInfo>().Where(p => p.ProductId == product.Id).ExecuteCommand();
                #endregion

                #region 商品标签
                if (product.Tags?.Count > 0)
                {
                    var tags = product.Tags.Select(p => new ProductTagInfo { ProductId = product.Id, TagId = p }).ToList();
                    Context.Insertable(tags).ExecuteCommand();
                }
                #endregion

                #region 商品分类
                if (product.Categorys?.Count > 0)
                {
                    var categorys = product.Categorys.Select(p => new ProductCategoryInfo { ProductId = product.Id, CategoryId = p }).ToList();
                    Context.Insertable(categorys).ExecuteCommand();
                }
                #endregion

                #region 商品图片
                var thumbnails = product.Thumbnails.Select(p => new ProductThumbnailInfo() { ProductId = info.Id }).ToList();
                Context.Insertable(thumbnails).ExecuteCommand();
                #endregion

                #region 商品规格与规格值
                List<SkuSpecInfo> addspecs = new List<SkuSpecInfo>();
                product.Skus.ForEach(sku => {
                    sku.Id = product.Id.ToString();
                    if (sku.Spec.Count > 0)
                    {
                        sku.Spec.ForEach(item =>
                        {
                            sku.Id += $"_{item.SpecValueId}";
                            if (item.TypeId == SpecType.Image || item.TypeId == SpecType.TextAndImage)
                                sku.Thumbnail = item.Data;
                            else
                                sku.Thumbnail = string.Empty;

                        });
                        #region 商品规格值信息
                        var specs = sku.Spec.Map<SkuSpecInfo>();
                        specs.ForEach(item => { item.ProductId = product.Id; item.SkuId = sku.Id; });
                        addspecs.AddRange(specs);
                        #endregion

                    }
                    else
                        sku.Id = $"{sku.Id}_0_0_0";
                });

                var skus = product.Skus.Map<ProductSkuInfo>();
                skus.ForEach(sku => sku.ProductId = product.Id);

                Context.Insertable(info).ExecuteCommand();
                if (addspecs.Count > 0)
                    Context.Insertable(addspecs).ExecuteCommand();
                #endregion

                #region 商品属性
                if (product.Attributes.Count > 0)
                {
                    var attributes = product.Attributes.Map<ProductAttributeInfo>();
                    attributes.ForEach(attribute => attribute.ProductId = product.Id);
                    Context.Insertable(attributes).ExecuteCommand();
                }
                #endregion

                #region 商品服务
                if (product.Services.Count > 0)
                {
                    var services = product.Services.Select(p => new ProductServiceInfo { ProductId = product.Id, ServiceId = p }).ToList();
                    Context.Insertable(services).ExecuteCommand();
                }
                #endregion
            });
        }

        /// <summary>
        /// 商品详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProductEditor Details(long id)
        {
           var info = Context.Queryable<ProductInfo>().Select<ProductEditor>().First(p => p.Id == id);
            //商品图片
            info.Thumbnails = Context.Queryable<ProductThumbnailInfo>().Where(p => p.ProductId == id).Select(p => p.FilePath).ToList();

            var skuSpecs = Context.Queryable<SkuSpecInfo>().Where(p => p.ProductId == id).ToList();

            //商品规格
            info.Skus = Context.Queryable<ProductSkuInfo>().Where(p => p.ProductId == id).Select<SkuEditor>()
                .Mapper(p => {
                    if (skuSpecs.Count > 0)
                    {
                        var specs = skuSpecs.Where(s => s.SkuId == p.Id).ToList();
                        p.Spec = specs.Map<SkuSpec>();
                        if (specs.Count > 0)
                        {
                           var names = specs.Select(p => new { name = p.Name + p.Value }).ToList();
                            p.SkuDesc = string.Join(";", names);
                        }
                    }
                  
                })
                .ToList();

            //商品属性
            info.Attributes = Context.Queryable<ProductAttributeInfo>().Where(p => p.ProductId == id).Select<ProductAttribute>().ToList();
            
            //商品服务
            info.Services = Context.Queryable<ProductServiceInfo>().Where(p => p.ProductId == id).Select(p => p.ServiceId).ToList();

            if (skuSpecs.Count > 0)
            {
                var specGroup = skuSpecs.GroupBy(p => p.SpecId).ToList();
                List<ProductEditor.ProductSpecInfo> specs = new List<ProductEditor.ProductSpecInfo>();
                foreach (var spec in specGroup)
                {
                    var specIds = spec.ToList().Select(p => p.SpecId).ToList();
                    var specList = Context.Queryable<SpecInfo>().Where(p => specIds.Contains(p.Id)).ToList();

                    var productSpec = new ProductEditor.ProductSpecInfo { SpecId = spec.Key, ValueIds = spec.Select(p => p.SpecValueId).ToList(),
                        Values = spec.ToList().Select(item => new ProductEditor.ProductSpecInfo.ProductSpecValueInfo
                        {
                            Key = item.SpecValueId,
                            SpecId = item.SpecId,
                            SpecName = specList.Where(p => p.Id == item.SpecId).First().Name,
                            TypeId = (SpecType)item.TypeId,
                            Value = item.Value,
                            Data = item.Data
                        }).ToList()
                    };
                    specs.Add(productSpec);
                }
                info.Specs = specs;
            }

            return info;
        }

        public Dictionary<string, object> GetFreightTemplateDictionary(List<int> freightTemplates) =>
        
            Context.Queryable<ProductInfo>().Where(p => freightTemplates.Contains(p.FreightTemplateId) && !p.IsDelete)
                .GroupBy(p => p.FreightTemplateId)
               .ToDictionary(p => p.FreightTemplateId, p => SqlFunc.AggregateCount(p.FreightTemplateId));

        /// <summary>
        /// 商品列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public List<Product> List(PageModel<ProductQuery> pageModel, ref int count)
        {

            return Context.Queryable<ProductInfo>()
            .WhereIF(!SqlFunc.IsNullOrEmpty(pageModel.Entity.Name), p => p.Name.Contains(pageModel.Entity.Name))
             .Select<Product>()
             .OrderBy(pageModel.GetOrderbyString())
             .ToPageList(pageModel.Page.Index, pageModel.Page.Size, ref count);
        }
            

    }
}
