﻿using Framework;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Application.Product;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;

namespace Wq.product.APi.admin
{
    /// <summary>
    /// 商品分类
    /// </summary>
    [Route("admin/product/category")]
    public class CategoryController : ProductAdminController
    {
        private readonly CategoryApplication categoryApplication;

        /// <summary>
        /// 商品分类
        /// </summary>
        public CategoryController(CategoryApplication categoryApplication)
        {
            this.categoryApplication = categoryApplication;
        }

        /// <summary>
        /// 添加商品分类
        /// </summary>
        /// <param name="category">分类</param>
        /// <returns></returns>
        [HttpPost("save")]
        public MessageModel Save(CategoryEditor category) => categoryApplication.Save(category);

        /// <summary>
        /// 删除商品分类
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("remove")]
        public MessageModel Remove(long id) => categoryApplication.Remove(id);


         /// <summary>
         /// 商品分类列表
         /// </summary>
         /// <param name="categorId">分类Id</param>
         /// <param name="depth">深度</param>
         /// <returns></returns>
        [HttpGet("list")]
        public MessageModel List(long categorId = 0, int depth = int.MaxValue) => categoryApplication.List(categorId, depth);

        /// <summary>
        /// 修改序号
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sequence">序号</param>
        /// <returns></returns>
        [HttpGet("sequence")]
        public MessageModel UpdateSequence(long id, int sequence) => categoryApplication.Sequence(id, sequence);

        /// <summary>
        /// 修改序号
        /// </summary>
        /// <param name="id"></param>
        /// <param name="display">是否隐藏</param>
        /// <returns></returns>
        [HttpGet("display")]
        public MessageModel UpdateDisplay(long id, bool display) => categoryApplication.Display(id, display);
    }
}
