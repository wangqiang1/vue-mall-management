import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import settingRouter from './modules/setting'
import productRouter from './modules/product'
import shopRouter from './modules/shop'
const routes: Array<RouteRecordRaw> = [
  {
    path: '/home',
    name: 'Home',
    component: () => import('../views/Home.vue'),
    meta: {
      title: '概况'
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/login/Login.vue'),
    meta: {
      title: '登录'
    }
  }
]
routes.push(...settingRouter)
routes.push(...productRouter)
routes.push(...shopRouter)

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  NProgress.start() // 进度条开始
  next()
})
 
router.afterEach(() => {
  NProgress.done() // 进度条结束
})

export default router
