﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Model;

namespace Wq.Core.PlatAdminService.Models
{
    public class Menu
    {
        /// <summary>
        /// 权限
        /// </summary>
        public int Route { get; set;}

        /// <summary>
        /// 菜单名
        /// </summary>
        public string Name{ get; set;}

        /// <summary>
        /// 路径
        /// </summary>
        public string Path{get; set;}

        /// <summary>
        /// 图标
        /// </summary>
        public string Icon{ get; set;}

        /// <summary>
        /// 权限组
        /// </summary>
        public List<Privilege> Permissions { get; set; }

        /// <summary>
        /// 子级页面
        /// </summary>
        public List<Menu> Children{ get; set;}

      

        public class Privilege
        {
            /// <summary>
            /// 操作Id
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// 操作名称
            /// </summary>
            public string Name { get; set; }

        }
    }

  
}
