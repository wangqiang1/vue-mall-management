﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.PlatAdminService.Models
{
    public class ManagerView
    {
		
		public long Id { get; set; }

		/// <summary>
		/// 权限组
		/// </summary>

		public string Name { get; set; }

		/// <summary>
		/// 权限组Id
		/// </summary>
		public long RoleId { get; set; }

		/// <summary>
		/// 管理员名称
		/// </summary>
		public string UserName { get; set; }

		/// <summary>
		/// 密码
		/// </summary>
		public string Password { get; set; }

		/// <summary>
		/// 创建时间
		/// </summary>

		public DateTime CreateDate { get; set; }
	}
}
