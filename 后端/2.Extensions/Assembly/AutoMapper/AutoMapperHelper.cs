﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.Extensions.Assembly.AutoMapper
{
    public static class AutoMapperHelper
    {
        private static IMapper _mapper;

        public static void SetMapper(IMapper mapper) =>
            _mapper = mapper;

        public static TDestination Map<TDestination>(this object source)
        {
            if (source == null)
                return default;
            return _mapper.Map<TDestination>(source);
        }

        public static List<TDestination> Map<TDestination>(this IEnumerable<object> source)
        {
            if (source == null)
                return new List<TDestination>();
            return _mapper.Map<List<TDestination>>(source);
        }
    }
}
