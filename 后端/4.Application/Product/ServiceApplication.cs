﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Model;
using Wq.Core.PlatAdminService;
using Wq.Core.PlatAdminService.Models;
using Wq.Core.ProductService;
using Wq.Core.ProductService.Models;

namespace Wq.Core.Application.Product
{
    public class ServiceApplication : BaseApplication
    {

        private readonly ServiceService service;
        private readonly LogService logService;
        public ServiceApplication(ServiceService service, LogService logService)
        {
            this.service = service;
            this.logService = logService;
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        public MessageModel List(PageModel<ServiceQuery> pageModel)
        {
            int count = 0;
            return SuccessResult(service.List(pageModel, ref count), count);
        }


        /// <summary>
        /// 验证
        /// </summary>
        /// <param name="tag"></param>
        public void Verify(ServiceEditor tag)
        {
            if (string.IsNullOrWhiteSpace(tag.Name))
                throw new MyException("服务名称不能为空");
            if (service.Count(p => p.Name == tag.Name && p.Id != tag.Id) > 0)
                throw new MyException("已经存在了相同名称的服务");
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="tag"></param>
        public MessageModel Save(ServiceEditor tag)
        {
            Verify(tag);
            var old = service.GetById(tag.Id);
            if (tag.Id > 0)
                service.Save(tag);
            else
            {
                var info = tag.Map<ServiceInfo>();
                info.CreateTime = DateTime.Now;
                service.Insert(info);
                service.RemoveCache();
            }
            Log(old, tag);

            return SuccessResult();
        }

        /// <summary>
        /// 删除标签
        /// </summary>
        /// <param name="id"></param>
        public MessageModel Remove(long id)
        {
            var old = service.GetById(id);
            service.DeleteById(id);
            service.RemoveCache();
            Log(old, isDelete: true);
            return SuccessResult();
        }

        /// <summary>
        /// 属性日志
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="oldAttribute"></param>
        public void Log(ServiceInfo oldtag, ServiceEditor tag = null, bool isDelete = false)
        {
            string msg = string.Empty;
            if (isDelete)
                msg = $"删除商品服务【{oldtag.Name}】";
            else
            {

                if (tag.Id == 0)
                    msg = $"新增商品服务【{tag.Name}】";
                else
                {
                    msg = $"修改商品服务【{tag.Name}】";
                    if (tag.Name != oldtag.Name)
                        msg += $" 名称由【{oldtag.Name}】变为【{tag.Name}】";
                }
            }

            logService.Create(LogModule.ProductService, msg);
        }

        /// <summary>
        /// 获取全部服务
        /// </summary>
        /// <returns></returns>
        public MessageModel Services() => SuccessResult(service.Services());
    }
}

