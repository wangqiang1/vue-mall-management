export  enum  attributeType
{
    //单选
    Radio =1,
    //多选
    Checkbox = 2,
    //文本
    Text = 3,
    //自定义
    Custom = 4
}

export  enum  FileType
{
    //图片
    Image =1,
    //视频
    Video = 2
}


export  enum  SpecType
{
    // 文字
    Text =1,
    // 图片
    Image = 2,
    // 颜色
    Color = 3,
    // 文字 + 图片
    TextAndImage = 4,
    // 文字 + 颜色
    TextAndColor = 5
}