﻿using System.ComponentModel;

namespace Wq.Core.ProductService.Models
{
    /// <summary>
    /// 属性类型
    /// </summary>
    public enum AttributeType
    {
        /// <summary>
        /// 单选
        /// </summary>
        [Description("单选")]
        Radio =1,
        /// <summary>
        /// 多选
        /// </summary>
        [Description("多选")]
        Checkbox = 2,
        /// <summary>
        /// 文本
        /// </summary>
        [Description("文本")]
        Text = 3,
        /// <summary>
        /// 自定义
        /// </summary>
        [Description("自定义")]
        Custom = 4
    }
}
