﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.FileService
{
    public class OssManagement
    {
        public static string Prefix = string.Empty;

        /// <summary>
        /// 上传图片地址
        /// </summary>
        public static string File_Image(string identity, string extension) => $"{Prefix}image/{DateTime.Now.ToString("yyyy-MM")}/{identity}{extension}";

        /// <summary>
        /// 上传视频地址
        /// </summary>
        public static string File_Video(string identity, string extension) => $"{Prefix}video/{DateTime.Now.ToString("yyyy-MM")}/{identity}{extension}";

        /// <summary>
        /// 区域资源文件
        /// </summary>
        public static string REGION_SOURCE => $"{Prefix}static/common/regions2.json";


    }
}
