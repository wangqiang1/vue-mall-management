﻿using System;
using System.Collections.Generic;
using Framework;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Wq.Core.Application.Manager;
using Wq.Core.Extensions;
using Wq.Core.Model;
using Wq.Core.PlatAdminService.Models;
using Wq.Core.Application;
using Wq.Core.FileService.Models.DTO;
using Wq.Core.FileService.Models;

namespace Wq.File.APi
{
    /// <summary>
    /// oss文件管理
    /// </summary>
    [ApiExplorerSettings(GroupName = "file")]
    [Route("admin/file/oss")]
    public class OssFileController : FileAdminController
    {
        private readonly FileApplication fileApplication;

        /// <summary>
        /// oss文件管理
        /// </summary>
        /// <param name="fileApplication"></param>
        public OssFileController(FileApplication fileApplication) => this.fileApplication = fileApplication;

        /// <summary>
        /// 图片视频列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        [HttpPost("list")]
        public MessageModel List(PageModel<FileQuery> pageModel)
        {
          
            return fileApplication.List(pageModel);
        }

        /// <summary>
        /// 上传图片文件
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>

        [HttpPost("image/upload")]
        [RequestSizeLimit(30_000_000)]
        public MessageModel UploadImage([FromForm] UploadImageCommand command) => fileApplication.UploadImage(command);

        /// <summary>
        /// 上传视频文件
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("video/upload")]
        [RequestSizeLimit(100_000_000)]
        public MessageModel UploadVideo([FromForm] UploadVideoCommand command) => fileApplication.UploadVideo(command);


        /// <summary>
        /// 修改文件名称
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        [HttpGet("setname")]
        public MessageModel SetImageName(long id, string name) => fileApplication.SetImageName(id , name);

        /// <summary>
        /// 批量分组
        /// </summary>
        /// <param name="edior"></param>
        [HttpPost("setgroup")]
        public MessageModel SetGroup(BatchEdior edior) => fileApplication.SetGroup(edior);  

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="id"></param>
        [HttpPost("remove")]
        public MessageModel RemoveImage(BatchEdior edior) => fileApplication.RemoveImage(edior);

        #region 图片分类

        /// <summary>
        /// 分类
        /// </summary>
        /// <returns></returns>
        [HttpGet("category/list")]
        public MessageModel CategoryList(FileType type) => fileApplication.CategoryList(type);

        /// <summary>
        /// 编辑文件分类
        /// </summary>
        /// <param name="editor"></param>
        /// <returns></returns>
        [HttpPost("category/save")]
        public MessageModel CategorySave(CategoryEditor category) => fileApplication.CategorySave(category);



        /// <summary>
        /// 修改文件分类名称
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("category/setname")]
        public MessageModel SetCategoryName(long id, string name) => fileApplication.SetCategoryName(id, name);

        /// <summary>
        /// 删除文件分类
        /// </summary>
        /// <param name="id"></param>
        [HttpGet("category/remove")]
        public MessageModel RemoveCategory(long id) => fileApplication.RemoveCategory(id);
        #endregion


    }

}
