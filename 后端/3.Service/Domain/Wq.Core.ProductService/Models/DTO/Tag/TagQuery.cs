﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    /// <summary>
    /// 查询实体
    /// </summary>
    public class TagQuery
    {
        /// <summary>
        /// 标签名称
        /// </summary>
        public string Name { get; set; }
    }
}
