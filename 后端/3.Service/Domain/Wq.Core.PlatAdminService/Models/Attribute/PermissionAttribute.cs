﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Model;

namespace Wq.Core.PlatAdminService.Models
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
    internal class PermissionAttribute: Attribute
    {
        public PermissionAttribute(params PrivilegeType[] privileges)
        {

            Privileges = privileges;
        }

        public PrivilegeType[] Privileges
        {
            get; set;
        }
    }
}
