﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    public class ProductEditor : Product
    {
        /// <summary>
        /// 商品图片
        /// </summary>
        public List<string> Thumbnails { get; set; }

        /// <summary>
        /// 规格信息
        /// </summary>
        public List<SkuEditor> Skus { get; set; }

        /// <summary>
        /// 属性信息
        /// </summary>
        public List<ProductAttribute> Attributes { get; set; }

        /// <summary>
        /// 服务集合
        /// </summary>
        public List<long> Services { get; set; }

        /// <summary>
        /// 规格集合
        /// </summary>
        public List<ProductSpecInfo> Specs { get; set; }

        public class ProductSpecInfo

        {
            /// <summary>
            /// 规格Id
            /// </summary>
            public long SpecId { get; set; }

            /// <summary>
            /// 规格名称
            /// </summary>
            public string SpecName { get; set; }

            /// <summary>
            /// 规格值集合
            /// </summary>
            public List<long> ValueIds { get; set; }

            /// <summary>
            /// 规格集合值
            /// </summary>
            public List<ProductSpecValueInfo> Values { get; set; }

            /// <summary>
            /// 展示形式
            /// </summary>
            public SpecType TypeId { get; set; }

            public class ProductSpecValueInfo
            {
                /// <summary>
                /// 规格值Id
                /// </summary>
                public long Key { get; set; }
                /// <summary>
                /// 规格Id
                /// </summary>
                public long SpecId { get; set; }

                /// <summary>
                /// 规格名称
                /// </summary>
                public string SpecName { get; set; }

                /// <summary>
                /// 展示形式
                /// </summary>
                public SpecType TypeId { get; set; }

                /// <summary>
                /// 值
                /// </summary>
                public string Value { get; set; }

                /// <summary>
                /// 辅助项 颜色 图片
                /// </summary>
                public string Data { get; set; }
            }
        }

        /// <summary>
        /// 标签集合
        /// </summary>
        public List<long> Tags { get; set; }

        /// <summary>
        /// 分类集合
        /// </summary>
        public List<long> Categorys { get; set; }

    }

    /// <summary>
    /// sku编辑
    /// </summary>
    public class SkuEditor : Sku
    {
        /// <summary>
        /// 规格信息
        /// </summary>
        public List<SkuSpec> Spec { get; set; }
    }
}
