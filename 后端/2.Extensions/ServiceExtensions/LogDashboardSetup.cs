﻿using LogDashboard;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Common;

namespace Wq.Core.Extensions.ServiceExtensions
{
    /// <summary>
    /// 可视化日志
    /// </summary>
    public static class LogDashboardSetup
    {
        public static void AddLogDashboardSetup(this IServiceCollection services)
        {
            services.AddLogDashboard();
        }

        public static void UseLogMyDashboard(this IApplicationBuilder app)
        {
            app.UseLogDashboard();
        }

    }
}
