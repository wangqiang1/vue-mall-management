﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    /// <summary>
    /// 商品服务  
    /// </summary>
    public class ServiceEditor
    {
        /// <summary>
        /// 商品Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 商品标签
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        public string Description { get; set; }
    }
}
