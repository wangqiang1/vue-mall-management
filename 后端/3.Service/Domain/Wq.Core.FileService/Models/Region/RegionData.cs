﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.FileService.Models
{
    /// <summary>
    /// 行政区域 数据 
    /// </summary>
    public class RegionData
    {
        /// <summary>
        /// id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// /名称
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 上级
        /// </summary>
        public int parentId { get; set; }

        /// <summary>
        /// 等级
        /// </summary>
        public int level { get; set; }
        /// <summary>
        /// 下级
        /// </summary>
        public List<RegionData> items { get; set; }

        /// <summary>
        /// 是否存在下级
        /// </summary>
        public bool hasChildren { get; set; }

    }
}
