﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Common.Helper;

namespace Wq.Core.Extensions.ServiceExtensions.Framework
{
  public static class ModuleRegister
    {
        public static void AddModule(this IServiceCollection services)
        {
            //List<string> _noList = new List<string>();
            //_noList.Add("SqlSugarBase");
            //_noList.Add("SqlSugarRepostitory");
            //_noList.Add("UnitOfWork");
            //_noList.Add("ExceptionHandlerMiddleware");
            //_noList.Add("AccessForbiddenException");
            //_noList.Add("MyException");
            //_noList.Add("UnAuthorizedException");
            //_noList.Add("RabbitAttribute");
            //_noList.Add("IHttpContextAccessor");


            var serviceTypes = RuntimeHelper.GetProjectTypes()
                .Where(p => !p.IsInterface && !p.IsAbstract && !p.IsSealed)
                .Where(p => typeof(IRegister).IsAssignableFrom(p))
                .ToList();

            foreach (var serviceType in serviceTypes)
                services.AddSingleton(serviceType);
        }
    }
}
