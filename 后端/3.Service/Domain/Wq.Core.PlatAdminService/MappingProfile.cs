﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.PlatAdminService.Models;

namespace Wq.Core.PlatAdminService
{
    class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<RoleEditor, RoleInfo>().ReverseMap();
            CreateMap<RoleEditor.RolePrivilege, RolePrivilegeInfo>().ReverseMap();
            CreateMap<ManagerEditor, ManagerInfo>().ReverseMap();
            CreateMap<OperationLog, OperationLogInfo>().ReverseMap();
        }
    }
}
