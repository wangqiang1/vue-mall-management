﻿using System;
using System.IO;
using Wq.Core.Common;
using Wq.Core.Extensions;
using Wq.Core.Extensions.Assembly.Aliyun;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.FileService;
using Wq.Core.FileService.Models;
using Wq.Core.FileService.Models.DTO;
using Wq.Core.Model;

namespace Wq.Core.Application
{
    /// <summary>
    /// 系统配置
    /// </summary>
    public class SettingsApplication : BaseApplication
    {
        public MessageModel GetSetting()
        {
            string fileDomain = Appsettings.app("AliyunOss:FileDomain");

            return SuccessResult(new {
                FileDomain =$"https://{fileDomain}/" 
            });
        }
    }
}
