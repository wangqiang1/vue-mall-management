﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.PlatAdminService.Models
{
    /// <summary>
    /// 日志模块
    /// </summary>
    public enum LogModule
    {
        /// <summary>
        /// 管理员
        /// </summary>
        [Description("管理员")]
        Manager = 1,


        /// <summary>
        /// 商品分类
        /// </summary>
        [Description("商品分类")]
        ProductCategory = 2,

        /// <summary>
        /// 商品属性
        /// </summary>
        [Description("商品品牌")]
        ProductBrand = 3,

        /// <summary>
        /// 商品属性
        /// </summary>
        [Description("商品属性")]
        ProductAttribute = 4,

        /// <summary>
        /// 商品属性项
        /// </summary>
        [Description("商品属性项")]
        ProductAttributeItem = 5,


        /// <summary>
        /// 商品标签
        /// </summary>
        [Description("商品标签")]
        ProductTag = 6,

        /// <summary>
        /// 商品服务
        /// </summary>
        [Description("商品服务")]
        ProductService = 7,

        /// <summary>
        /// 商品规格
        /// </summary>
        [Description("商品规格")]
        ProductSpec = 8,

        /// <summary>
        /// 商品服务
        /// </summary>
        [Description("商品运费模板")]
        ProductFreightTemplate = 9,

    }
}
