/**
 * 配置编译环境和线上环境接口请求
 * hostURL: 域名地址（仅用于打包后线上请求）
 * baseURL：请求地址前缀
 */
 const host = window.location.host
 const hostURL = window.location.href.split(host)[0] + host + '/' // 改为自动获取当前访问地址，不使用手动配置
 
 let baseURL = ''
 let apiUrl = '/admin/'
 
 if (process.env.NODE_ENV === 'production') {
   baseURL = hostURL
 } else {
   apiUrl = 'admin/'
 } 
 
 export {
   baseURL,
   apiUrl 
 }