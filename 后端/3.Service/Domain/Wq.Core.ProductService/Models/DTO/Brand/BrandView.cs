﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    /// <summary>
    /// 品牌实体
    /// </summary>
    public class BrandView
    {
        /// <summary>
        /// 属性Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 属性名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// 分类Id
        /// </summary>
        public long CategoryId { get; set; }

        /// <summary>
        /// logo
        /// </summary>
        public string Icon { get; set; }
    }
}
