﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.Common.Helper
{
    public static class DefaultContainer
    {
        private static IServiceProvider rootService;
        

        public static void SetRoot(IServiceProvider applicationServices)
        {
            rootService = applicationServices;
           
        }

        
        /// <summary>
        /// Root提供
        /// </summary>
        public static T GetService<T>() =>
            (T)rootService.GetService(typeof(T));
    }
}
