import request from '@/utils/request'

///商品属性详情
export const AttributeDetails = (data: any): any => request({ url: 'product/attribute/details', data, method: 'GET' })
//商品属性项详情
export const GetattributeItems = (data: any): any => request({ url: 'product/attribute/attributeItems', data, method: 'GET' })

//添加商品属性
export const SaveAttribute = (data: any): any => request({ url: 'product/attribute/save', data, method: 'POST' })
//删除属性
export const RemoveAttribute = (data: any): any => request({ url: 'product/attribute/remove', data, method: 'GET' })
//修改序号
export const UpdateSequence = (data: any): any => request({ url: 'product/attribute/sequence', data, method: 'GET' })
// 获取全属性
export const GetAttributes = (): any => request({ url: 'product/attribute/attributes', method: 'GET' })

//添加商品属性
export const SaveAttributeItem = (data: any): any => request({ url: 'product/attribute/item/save', data, method: 'POST' })
//删除属性值
export const RemoveAttributeItem = (data: any): any => request({ url: 'product/attribute/item/remove', data, method: 'GET' })
export const GetAttributeItem = (data: any): any => request({ url: 'product/attribute/item/details', data, method: 'GET' })
//修改序号
export const UpdateSequenceItem = (data: any): any => request({ url: 'product/attribute/item/sequence', data, method: 'GET' })
//删除规格值-项
export const RemoveAttributeItemValue = (data: any): any => request({ url: 'product/attribute/item/remove/Value', data, method: 'GET' })

//添加商品分类
export const SaveCategory = (data: any): any => request({ url: 'product/category/save', data, method: 'POST' })
//删除分类
export const RemoveCategory = (data: any): any => request({ url: 'product/category/remove', data, method: 'GET' })
//修改序号
export const CategorySequence = (data: any): any => request({ url: 'product/category/sequence', data, method: 'GET' })
//修改是否显示
export const CategoryDisplay = (data: any): any => request({ url: 'product/category/Display', data, method: 'GET' })
//分类列表
export const Categorys = (): any => request({ url: 'product/category/list', method: 'GET' })

//添加标签
export const SaveTag = (data: any): any => request({ url: 'product/tag/save', data, method: 'POST' })
//删除标签
export const RemoveTag = (data: any): any => request({ url: 'product/tag/remove', data, method: 'GET' })
//修改标签序号
export const UpdateTagSequence = (data: any): any => request({ url: 'product/tag/sequence', data, method: 'GET' })
//全部标签
export const tags = (): any => request({ url: 'product/tag/tags', method: 'GET' })

//添加商品服务
export const SaveService = (data: any): any => request({ url: 'product/service/save', data, method: 'POST' })
//删除商品服务
export const RemoveService = (data: any): any => request({ url: 'product/service/remove', data, method: 'GET' })
//删除商品服务
export const GetServices = (): any => request({ url: 'product/service/services', method: 'GET' })


//添加商品品牌
export const SaveBrand = (data: any): any => request({ url: 'product/brand/save', data, method: 'POST' })
//修改品牌序号
export const UpdateBrandSequence = (data: any): any => request({ url: 'product/brand/sequence', data, method: 'GET' })
//删除商品服务
export const RemoveBrand = (data: any): any => request({ url: 'product/brand/remove', data, method: 'GET' })



//编辑规格
export const SpecSave = (data: any): any => request({ url: 'product/spec/save', data, method: 'POST' })
//删除规格
export const SpecRemove = (data: any): any => request({ url: 'product/spec/remove', data, method: 'GET' })
//删除规格值
export const SpecRemoveValue = (data: any): any => request({ url: 'product/spec/value/remove', data, method: 'GET' })
//规格详情
export const SpecDetails = (data: any): any => request({ url: 'product/spec/details', data, method: 'GET' })
//全部规格
export const Specs = (): any => request({ url: 'product/spec/specs', method: 'GET' })
//获取规格值
export const SpecValues = (data: any): any => request({ url: 'product/spec/values',data, method: 'GET' })

//获取规格
export const CreateSpec = (data: any): any => request({ url: 'product/spec/createSpec',data, method: 'GET' })
//创建规格值
export const CreateValue = (data: any): any => request({ url: 'product/spec/createvalue',data, method: 'GET' })


//运费模板详情
export const FreightTemplateDetails = (data: any): any => request({ url: 'product/FreightTemplate/details', data, method: 'GET' })

//编辑运费模板
export const SaveFreightTemplate = (data: any): any => request({ url: 'product/freightTemplate/save', data, method: 'POST' })

//删除运费模板
export const RemoveFreightTemplate = (data: any): any => request({ url: 'product/FreightTemplate/remove', data, method: 'GET' })
//全部运费模板
export const GetFreightTemplates = (): any => request({ url: 'product/freightTemplate/FreightTemplates', method: 'GET' })

//编辑商品
export const SaveProduct = (data: any): any => request({ url: 'product/save', data, method: 'POST' })