﻿using System.ComponentModel;

namespace Wq.Core.ProductService.Models
{
    /// <summary>
    /// 规格类型
    /// </summary>
    public enum SpecType
    {
        /// <summary>
        /// 文字
        /// </summary>
        [Description("文字")]
        Text = 1,

        /// <summary>
        /// 图片
        /// </summary>
        [Description("图片")]
        Image = 2,

        /// <summary>
        /// 颜色
        /// </summary>
        [Description("颜色")]
        Color = 3,

        /// <summary>
        /// 文字 + 图片
        /// </summary>
        [Description("文字 + 图片")]
        TextAndImage = 4,

        /// <summary>
        /// 文字 + 颜色
        /// </summary>
        [Description("文字 + 颜色")]
        TextAndColor = 5,
    }
}
