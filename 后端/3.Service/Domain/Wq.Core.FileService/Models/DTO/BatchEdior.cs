﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.FileService.Models
{
    /// <summary>
    /// 修改分组
    /// </summary>
    public class BatchEdior
    {
        /// <summary>
        /// 分类
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// id集合
        /// </summary>
        public List<long> Ids { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public FileType Type { get; set; }
    }
}
