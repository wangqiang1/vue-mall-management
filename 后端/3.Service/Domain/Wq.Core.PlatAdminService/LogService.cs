﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SqlSugar;
using Wq.Core.Common.Helper;
using Wq.Core.Extensions;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Extensions.Assembly.Jwt;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.Extensions.Assembly.SqlSugar;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.Model;
using Wq.Core.PlatAdminService.Models;

namespace Wq.Core.PlatAdminService
{
    public class LogService : BaseRepository<OperationLogInfo>, IRegister
    {
        private IHttpContextAccessor accessor;
        private JwtUserModel manager = new JwtUserModel();
        private string ipAddress;

        public LogService(IHttpContextAccessor accessor)
        {
            this.accessor = accessor;
        }
        /// <summary>
        /// 添加日志
        /// </summary>
        /// <param name="log"></param>
        public void Create(LogModule module, string message)
        {
            manager = JWTHelper.SerializeJwt(accessor.HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer ", ""));
            ipAddress = accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            OperationLog log = new OperationLog
            {
                Module = module,
                Operator = manager.UserId.ObjToInt(),
                OperatorName = manager.UserName,
                OperatorAddress = ipAddress,
                Message = message,
            };
            var info = log.Map<OperationLogInfo>();
            info.OperatorAddress = ipAddress;
            info.Time = DateTime.Now;
            Context.Insertable(info).ExecuteCommand();

        }

        /// <summary>
        /// 添加日志
        /// </summary>
        /// <param name="log"></param>
        public void Create(List<OperationLog> logs)
        {
            manager = JWTHelper.SerializeJwt(accessor.HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer ", ""));
            ipAddress = accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            var list = logs.Map<OperationLogInfo>();
            list.ForEach(item => {
                item.Operator = manager.UserId.ObjToInt();
                item.OperatorName = manager.UserName;
                item.OperatorAddress = ipAddress;
                item.Time = DateTime.Now;
            });
            Context.Insertable(list).ExecuteCommand();
        }
    }
}
