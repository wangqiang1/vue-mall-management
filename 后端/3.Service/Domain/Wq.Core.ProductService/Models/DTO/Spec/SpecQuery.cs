﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models.DTO.Spec
{
    public class SpecQuery
    {
        /// <summary>
        /// 名称或者别名
        /// </summary>
        public string Name { get; set; }
    }
}
