﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Model;
using Wq.Core.PlatAdminService;
using Wq.Core.PlatAdminService.Models;
using Wq.Core.ProductService;
using Wq.Core.ProductService.Models;

namespace Wq.Core.Application.Product
{
    /// <summary>
    /// 商品规格
    /// </summary>
    public class SpecApplication : BaseApplication
    {
      
        private readonly SpecService specService;
        private readonly LogService logService;
        private readonly ProductSpecService productSpecService;
        private readonly SpecValueService specValueService;
        public SpecApplication(SpecService specService, LogService logService, ProductSpecService productSpecService, SpecValueService specValueService)
        {
            this.specService = specService;
            this.logService = logService;
            this.productSpecService = productSpecService;
            this.specValueService = specValueService;
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        public MessageModel List(PageModel<TagQuery> pageModel)
        {
            int count = 0;
            return SuccessResult(specService.List(pageModel, ref count), count);
        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MessageModel Details(long id) => SuccessResult(specService.Details(id));
       

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="tag"></param>
        public MessageModel Save(SpecEditor spec)
        {
            Verify(spec);
            var old = specService.Details(spec.Id);
          
            if (spec.Id > 0)
                specService.Save(spec);
            else
                specService.Create(spec);
            Log(old, spec);
            specService.RemoveCache();
            return SuccessResult();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MessageModel Remove(long id)
        {
            if (productSpecService.ExistBySpec(id))
                throw new MyException("该规格下面绑定了商品不能进行删除");
            var old = specService.Details(id);
            specService.Remove(id);
            Log(old, isDelete: true);
            specService.RemoveCache();
            return SuccessResult();
        }

        public MessageModel RemoveValue(long id)
        {
            if (productSpecService.ExistBySpecValue(id))
                throw new MyException("该规格值下面绑定了商品不能进行删除");
            var old = specValueService.GetById(id);
            var specOld = specService.GetById(old.SpecId);
            specService.RemoveValue(id);
            specService.RemoveCache();
            logService.Create(LogModule.ProductSpec, $"删除商品规格【{specOld.Name}】的规格值【{old.Value}】");
            return SuccessResult();
        }

        /// <summary>
        /// 验证
        /// </summary>
        /// <param name="spec"></param>
        public void Verify(SpecEditor spec)
        {
            if (string.IsNullOrEmpty(spec.Name))
                throw new MyException("请输入规格名称");
            if (string.IsNullOrEmpty(spec.Alias))
                throw new MyException("请输入规格别名");
            if (spec.Values.Count == 0)
                throw new MyException("规格值不能位空");
            if(spec.Values.Count != spec.Values.Distinct().Count())
                throw new MyException("规格值不能重复");
        }

        /// <summary>
        /// 日志
        /// </summary>
        /// <param name="oldSpec"></param>
        /// <param name="spec"></param>
        /// <param name="isDelete"></param>
        public void Log(SpecEditor oldSpec, SpecEditor spec = null, bool isDelete = false)
        {
            string msg = string.Empty;
             if (isDelete)
                msg = $"删除商品规格【{oldSpec.Name}】";
            else
            {
                if (spec.Id == 0)
                    msg = $"新增商品规格【{spec.Name}】,规格值【{ string.Join(",", spec.Values) }】";
                else
                {
                    msg = $"修改商品规格【{oldSpec.Name}】";
                    if (oldSpec.Name != spec.Name)
                        msg += $" 名称由【{oldSpec.Name}】变为【{spec.Name}】";
                    if (string.Join(",", oldSpec.Values) != string.Join(",", spec.Values))
                        msg += $" 规格值由【{string.Join(",", oldSpec.Values)}】变为【{string.Join(",", spec.Values)}】";
                }
            }
            logService.Create(LogModule.ProductSpec, msg);
        }

        /// <summary>
        /// 全部标签
        /// </summary>
        /// <returns></returns>
        public MessageModel Specs()
        {
           var specs = specService.Specs();
            return SuccessResult(specs.Select(p => new { key = p.Id, value = p.Name }).ToList());
        }

        /// <summary>
        /// 获取规格详情
        /// </summary>
        /// <param name="specId"></param>
        /// <returns></returns>
        public MessageModel SpecValues(long specId)
        {
            var specValues = specService.SpecValues(specId);
            return SuccessResult(specValues.Select(p => new { key = p.Id, value = p.Value }).ToList());
        }

        /// <summary>
        /// 添加规格值
        /// </summary>
        /// <param name="specId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public MessageModel CreateSpecValue(long specId, string value)
        {
            if (specValueService.Count(p => p.SpecId == specId && p.Value == value)  > 0)
                throw new MyException("规格值已经存在");
            var id = specValueService.InsertReturnBigIdentity(new SpecValueInfo { SpecId = specId, Value = value });
            var old = specService.Details(specId);
            logService.Create(LogModule.ProductSpec, $"商品规格【{old.Name}】,新增规格值【{value}】");
            specService.RemoveCache();
            return SuccessResult(id);
        }

        /// <summary>
        /// 创建规格
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public MessageModel CreateSpec(string name)
        {
            if (specService.Count(p => p.Name == name) > 0)
                throw new MyException("规格已经存在");
            var id = specService.InsertReturnBigIdentity(new SpecInfo { Name = name,Alias =name,ProductCount = 0 });
            specService.RemoveCache();
            return SuccessResult(id);
        }
    }
}
