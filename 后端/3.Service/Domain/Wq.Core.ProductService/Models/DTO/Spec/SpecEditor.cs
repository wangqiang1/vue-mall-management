﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    /// <summary>
    /// 规格编辑
    /// </summary>
    public class SpecEditor
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }    

        /// <summary>
        /// 规格名称
        /// </summary>
        public string Name { get; set; }    

        /// <summary>
        /// 别名
        /// </summary>
        public string Alias { get; set; }

      

        /// <summary>
        /// 值
        /// </summary>
        public List<string> Values { get; set; }
    }
}
