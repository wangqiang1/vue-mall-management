﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.FileService.Models.DTO
{
    public class UploadVideoCommand
    {
        /// <summary>
        /// 视频文件
        /// </summary>
        public IFormFile Video {get; set;}
        /// <summary>
        /// 封面图文件
        /// </summary>
        public IFormFile Cover {get; set;}
        /// <summary>
        /// 分类
        /// </summary>
        public int Category { get; set; }
    }
}
