﻿using DotNetCore.CAP;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions;
using Wq.Core.Extensions.Assembly.Aliyun;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.FileService;
using Wq.Core.FileService.Models;
using Wq.Core.Model;

namespace Wq.Core.Application
{
    /// <summary>
    /// 地区管理
    /// </summary>
    public class RegionApplication : BaseApplication
    {
        private readonly RegionService service;
        private readonly ICapPublisher capBus;
        private readonly BaseRepository redisRepository;
        public RegionApplication(RegionService service,  ICapPublisher capBus, BaseRepository redisRepository)
        {
            this.service = service;
            this.capBus = capBus;
            this.redisRepository = redisRepository;
        }

        /// <summary>
        /// 获取下级集合
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public MessageModel Subs(int parentId) => SuccessResult(service.Subs(parentId));

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public MessageModel Create(string name, int parentId)
        {
            service.Create(name, parentId);
            capBus.Publish(CAPKey.SaveRegionFile, "生成地址文件");
            // fileHandler.SaveFile();
            return SuccessResult();
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public MessageModel Save(int id, string name)
        {
            service.Save(id, name);
            capBus.Publish(CAPKey.SaveRegionFile, "生成地址文件");
            return SuccessResult();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MessageModel Remove(long id)
        {
            if (service.Count(p => p.ParentId == id) > 0)
                throw new MyException("存在下级地区不能删除");
            service.DeleteById(id);
            capBus.Publish(CAPKey.SaveRegionFile, "生成地址文件");
            return SuccessResult();
        }

        /// <summary>
        /// 获取文件目录
        /// </summary>
        public MessageModel GetFile() => SuccessResult(OssManagement.REGION_SOURCE);


        /// <summary>
        /// 全部地区
        /// </summary>
        public List<RegionData> Data(int maxLevel = 4) => service.list(maxLevel);

        /// <summary>
        /// 获取层级关系地址
        /// </summary>
        /// <param name="data"></param>
        /// <param name="id"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        private List<RegionData> Regions(List<RegionData> data, long id = 0, int level = 4)
        {

            var list = data.Where(p => p.parentId == id).ToList();
            foreach (var item in list)
            {
                if (item.level < level)
                {
                    if (data.Where(p => p.parentId == item.id).Count() > 0)
                        item.items = Regions(data, item.id, level);
                }
            }
            return list;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public MessageModel List(long id = 0, int level = 4) => redisRepository.Get(Model.CacheKey.RegionToLevel(level),() => {
            var data = Data(level);
            return SuccessResult(Regions(data,id,level));
        }).Result;



        /// <summary>
        /// 创建文件
        /// </summary>

        public void SaveFile()
        {
            var source = Regions(Data(4));
            var json = JsonConvert.SerializeObject(source);
            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(json));
            AliyunOSSHelper.PutObject(OssManagement.REGION_SOURCE, stream);
        }
    }
}
