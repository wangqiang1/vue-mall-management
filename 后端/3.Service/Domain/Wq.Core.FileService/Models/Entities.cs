﻿

using System;
using SqlSugar;

namespace Wq.Core.FileService.Models
{
	/// <summary>
	/// Wq_Category
	/// </summary>
	[TenantAttribute("file")]
	[SugarTable("Wq_Category")]
	public class CategoryInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 文件类型 1 Image 2 视频
		/// </summary>
	    public int  Type {get; set;}
		/// <summary>
		/// 分类名称
		/// </summary>
	    public string  Name {get; set;}
		/// <summary>
		/// 文件数量
		/// </summary>
	    public int  FileCount {get; set;}
		/// <summary>
		/// 供应商Id
		/// </summary>
	    public long  SupplierId {get; set;}
	}

	/// <summary>
	/// Wq_File
	/// </summary>
	[TenantAttribute("file")]
	[SugarTable("Wq_File")]
	public class FileInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public long  Id {get; set;}
		/// <summary>
		/// 文件分类
		/// </summary>
	    public int  CategoryId {get; set;}
		/// <summary>
		/// 封面
		/// </summary>
	    public string  Cover {get; set;}
		/// <summary>
		/// 文件名
		/// </summary>
	    public string  Name {get; set;}
		/// <summary>
		/// 文件类型
		/// </summary>
	    public int  Type {get; set;}
		/// <summary>
		/// 文件大小
		/// </summary>
	    public long  Size {get; set;}
		/// <summary>
		/// Tag
		/// </summary>
	    public string  Tag {get; set;}
		/// <summary>
		/// OSS服务唯一ID
		/// </summary>
	    public string  ETag {get; set;}
		/// <summary>
		/// 文件路径
		/// </summary>
	    public string  Path {get; set;}
		/// <summary>
		/// 上传时间
		/// </summary>
	    public DateTime  CreateTime {get; set;}
		/// <summary>
		/// 供应商Id  0 表示主站
		/// </summary>
	    public long  SupplierId {get; set;}
	}

	/// <summary>
	/// Wq_Region
	/// </summary>
	[TenantAttribute("file")]
	[SugarTable("Wq_Region")]
	public class RegionInfo
	{
		/// <summary>
		/// Id
		/// </summary>
		[SugarColumn(IsPrimaryKey = true , IsIdentity = true)]
	    public int  Id {get; set;}
		/// <summary>
		/// 名称
		/// </summary>
	    public string  Name {get; set;}
		/// <summary>
		/// 等级
		/// </summary>
	    public int  Level {get; set;}
		/// <summary>
		/// 父级ID
		/// </summary>
	    public int  ParentId {get; set;}
	}

}
