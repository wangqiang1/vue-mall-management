﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.Extensions.Assembly.SqlSugar;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;

namespace Wq.Core.ProductService
{
    /// <summary>
    /// 运费模板 
    /// </summary>
    public class FreightTemplateService : BaseRepository<FreightTemplateInfo>, IRegister
    {
        private readonly BaseRepository redisRepository;

        public FreightTemplateService(BaseRepository redisRepository) => this.redisRepository = redisRepository;
        /// <summary>
        /// 创建运费模板
        /// </summary>
        /// <param name="product"></param>
        public void Create(FreightTemplateEditor freightTemplate)
        {
            Context.Ado.UseTran(() => {
               var info = freightTemplate.Map<FreightTemplateInfo>();
                info.ModifyDate = DateTime.Now;
                info.Id = Context.Insertable(info).ExecuteReturnIdentity();
                freightTemplate.Groups.ForEach(item => {
                    var group = item.Map<FreightGroupInfo>();
                    group.TemplateId = info.Id;
                    group.Id = Context.Insertable(group).ExecuteReturnIdentity();
                  var regions =  item.Regions.Select(p => new FreightGroupRegionInfo { GroupId = group.Id, RegionId = p, TemplateId = info.Id, Exclude = false }).ToList();
                    Context.Insertable(regions).ExecuteCommand();
                });

                if (freightTemplate.ExcludeRegions?.Count > 0)
                {
                    var regions = freightTemplate.ExcludeRegions.Select(p => new FreightGroupRegionInfo { GroupId = 0, Exclude = true, TemplateId = info.Id,RegionId = p }).ToList();
                    Context.Insertable(regions).ExecuteCommand();
                }
                RemoveCache();
            });
        }

 

        /// <summary>
        /// 保存运费模板
        /// </summary>
        /// <param name="freightTemplate"></param>
        public void Save(FreightTemplateEditor freightTemplate)
        {
            Context.Ado.UseTran(() => {
                var info = freightTemplate.Map<FreightTemplateInfo>();
                info.ModifyDate = DateTime.Now;
                Context.Updateable(info).ExecuteCommand();

                Context.Deleteable<FreightGroupInfo>().Where(p => p.TemplateId == freightTemplate.Id).ExecuteCommand();
                Context.Deleteable<FreightGroupRegionInfo>().Where(p => p.TemplateId == freightTemplate.Id).ExecuteCommand();
                Context.Deleteable<FreightGroupRegionInfo>().Where(p => p.TemplateId == freightTemplate.Id).ExecuteCommand();

                freightTemplate.Groups.ForEach(item => {
                    var group = item.Map<FreightGroupInfo>();
                    group.TemplateId = info.Id;
                    group.Id = Context.Insertable(group).ExecuteReturnIdentity();
                    var regions = item.Regions.Select(p => new FreightGroupRegionInfo { GroupId = group.Id, RegionId = p, TemplateId = info.Id, Exclude = false }).ToList();
                    Context.Insertable(regions).ExecuteCommand();
                });

                if (freightTemplate.ExcludeRegions?.Count > 0)
                {
                    var regions = freightTemplate.ExcludeRegions.Select(p => new FreightGroupRegionInfo { GroupId = 0, Exclude = true, TemplateId = info.Id, RegionId = p }).ToList();
                    Context.Insertable(regions).ExecuteCommand();
                }
                RemoveCache();
            });
        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public FreightTemplateEditor Details(int Id) =>
         Context.Queryable<FreightTemplateInfo>().Select<FreightTemplateEditor>().Where(p => p.Id == Id)
                .Mapper(p => {
                    p.Groups = Context.Queryable<FreightGroupInfo>().Where(g => g.TemplateId == p.Id).Select<FreightTemplateEditor.FreightTemplateGroupEditor>().ToList();
                    p.Groups.ForEach(item => {
                        item.Regions = Context.Queryable<FreightGroupRegionInfo>().Where(fr => fr.GroupId == item.Id && fr.Exclude == false).Select(fr => fr.RegionId).ToList();
                    });
                    p.ExcludeRegions = Context.Queryable<FreightGroupRegionInfo>().Where(fr => fr.TemplateId == Id  && fr.Exclude == true).Select(fr => fr.RegionId).ToList();
                })
                .First();

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public List<FreightTemplateEditor> list(PageModel<FreightTemplateQuery> pageModel, ref int count) => Context.Queryable<FreightTemplateInfo>().Select<FreightTemplateEditor>()
                .WhereIF(!string.IsNullOrEmpty(pageModel.Entity.Name), p => p.Name.Contains(pageModel.Entity.Name))
                .Mapper(p =>
                {
                    p.Groups = Context.Queryable<FreightGroupInfo>().Where(g => g.TemplateId == p.Id).Select<FreightTemplateEditor.FreightTemplateGroupEditor>().ToList();
                    p.Groups.ForEach(item =>
                    {
                        item.Regions = Context.Queryable<FreightGroupRegionInfo>().Where(fr => fr.GroupId == item.Id && fr.Exclude == false).Select(fr => fr.RegionId).ToList();
                    });
                    p.ExcludeRegions = Context.Queryable<FreightGroupRegionInfo>().Where(fr => fr.TemplateId == p.Id && fr.Exclude == true).Select(fr => fr.RegionId).ToList();
                })
                .ToPageList(pageModel.Page.Index, pageModel.Page.Size, ref count);

        /// <summary>
        /// 获取运费模板
        /// </summary>
        /// <returns></returns>
        public Dictionary<string ,object> FreightTemplates() => redisRepository.Get(CacheKey.FreightTemplate, () => Context.Queryable<FreightTemplateInfo>().ToDictionary(p => p.Id, t => t.Name)).Result;
        
        /// <summary>
        /// 清缓存
        /// </summary>
        public void RemoveCache() => redisRepository.Remove(CacheKey.FreightTemplate);
    }
}
