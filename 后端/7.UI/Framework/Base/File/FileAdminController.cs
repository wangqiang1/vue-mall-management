﻿using Framework.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SqlSugar.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;

namespace Framework
{
    [Authorize("Manager")]
    [ApiExplorerSettings(GroupName = "file")]
    public class FileAdminController : AdminController
    {
    }
}
