﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.FileService.Models
{
    /// <summary>
    /// 文件类型
    /// </summary>
    public enum FileType
    {
        /// <summary>
        /// 图片
        /// </summary>
        Image = 1,
        /// <summary>
        /// 视频
        /// </summary>
        Video = 2,
        /// <summary>
        /// 可视化模板
        /// </summary>
        VisualTemplate = 3,
        /// <summary>
        /// 音频
        /// </summary>
        Audio = 4,
    }
}
