﻿
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.Extensions.Assembly.SqlSugar;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;
using Wq.Core.ProductService.Models.DTO.Attribute;

namespace Wq.Core.ProductService
{
    /// <summary>
    /// 商品属性领域 
    /// </summary>
    public class AttributeService : BaseRepository<AttributeInfo>, IRegister
    {
        private readonly BaseRepository redisRepository;

        public AttributeService(BaseRepository redisRepository)=> this.redisRepository = redisRepository;

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="attribute"></param>
        public void Create(AttributeEditor attribute)
        {
            Context.Ado.UseTran(() => {
                var info = attribute.Map<AttributeInfo>();
                info.Id =  Context.Insertable(info).ExecuteReturnBigIdentity();

                if (attribute.Categorys.Count > 0)
                {
                    var attributeCategorys = attribute.Categorys.Select(p => new AttributeCategoryInfo { AttributeId = info.Id, CategoryId = p }).ToList();
                    Context.Insertable(attributeCategorys).ExecuteCommand();
                }
                RemoveCache();
            });
        }
        public void Save(AttributeEditor attribute)
        {
            Context.Ado.UseTran(() => {
                Context.Updateable<AttributeInfo>()
                   .SetColumns(p => p.Name == attribute.Name)
                   .SetColumns(p => p.Sequence == attribute.Sequence)
                   .Where(p => p.Id == attribute.Id).ExecuteCommand();
                if (attribute.Categorys.Count > 0)
                {
                    var old = Context.Queryable<AttributeCategoryInfo>().Where(p => p.AttributeId == attribute.Id).Select(p => p.CategoryId).ToList();
                    var addIds = attribute.Categorys.Where(p => !old.Contains(p)).ToList();
                    var deleteIds = old.Where(p => !attribute.Categorys.Contains(p)).ToList();
                    if (addIds.Count > 0)
                    {
                        var addCategorys = addIds.Select(p => new AttributeCategoryInfo { AttributeId = attribute.Id, CategoryId = p }).ToList();
                        Context.Insertable(addCategorys).ExecuteCommand();
                    }
                    if(deleteIds.Count > 0)
                        Context.Deleteable<AttributeCategoryInfo>().Where(p => deleteIds.Contains(p.CategoryId) && p.AttributeId == attribute.Id).ExecuteCommand();
                }
                else 
                    Context.Deleteable<AttributeCategoryInfo>().Where(p => p.AttributeId == attribute.Id).ExecuteCommand();

                RemoveCache();
            });
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id"></param>
        public void Delete(long Id)
        {
            Context.Ado.UseTran(() => {
                Context.Deleteable<AttributeInfo>().Where(p => p.Id == Id).ExecuteCommand();
                Context.Deleteable<AttributeItemInfo>().Where(p => p.AttributeId == Id).ExecuteCommand();
                Context.Deleteable<AttributeValueInfo>().Where(p => p.AttributeId == Id).ExecuteCommand();
                RemoveCache();
            });
        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public AttributeEditor Details(long id) => 
            Context.Queryable<AttributeInfo>()
            .Select<AttributeEditor>()
            .Mapper(p => {
                p.Categorys = Context.Queryable<AttributeCategoryInfo>().Where(p => p.AttributeId == id).Select(p => p.CategoryId).ToList();
            })
            .First(p => p.Id == id);


        /// <summary>
        /// 属性列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public List<AttributeView> List(PageModel<AttributeQuery> pageModel, ref int count)
        {
           return Context.Queryable<AttributeInfo>()
                .WhereIF(!SqlFunc.IsNullOrEmpty(pageModel.Entity.Name), p => p.Name.Contains(pageModel.Entity.Name))
                .Select<AttributeView>()
                .OrderBy(pageModel.GetOrderbyString())
                .ToPageList(pageModel.Page.Index, pageModel.Page.Size, ref count);
        }

        /// <summary>
        /// 修改排序
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Sequence"></param>
        public void UpdateSequence(long id, long Sequence) => Context.Updateable<AttributeInfo>().SetColumns(p => p.Sequence == Sequence).Where(p => p.Id == id).ExecuteCommand();

        /// <summary>
        /// 获取全部属性
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<long ,string>> Attributes() => redisRepository.Get(Model.CacheKey.Attribute, () => base.GetList().OrderByDescending(p => p.Sequence).ToDictionary(p => p.Id, v => v.Name).ToList()).Result;


        /// <summary>
        /// 清缓存
        /// </summary>
        public void RemoveCache() => redisRepository.Remove(Model.CacheKey.Attribute);



    }
}
