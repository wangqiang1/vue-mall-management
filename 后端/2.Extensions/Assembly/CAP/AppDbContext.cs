﻿using Microsoft.EntityFrameworkCore;
using Wq.Core.Common;

namespace Wq.Core.Extensions.Assembly.CAP
{
    public class Person
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public override string ToString()
        {
            return $"Name:{Name}, Id:{Id}";
        }
    }


    public class AppDbContext : DbContext
    {
        public string ConnectionString = Appsettings.app("ConnectionStrings:Dashboard");

        public DbSet<Person> Persons { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(ConnectionString, ServerVersion.AutoDetect(ConnectionString));
        }
    }
}
