import request from '@/utils/request'

//管理员登录
export const OauthLogin = (data: { name: string; password: string }): any => request({ url: 'accessToken/gettoken', data, loading: true })
//获取管理员可访问导航列表
export const RoleMenus = (): any => request({ url: 'role/menus' })
//刷新token
export const RefreshToken = (data: { token: string }): any => request({ url: 'accessToken/refreshToken', data })
//post请求
export const PostList = (url: string, data: any): any => request({ url: url, data, method: 'POST' })
//Get请求
export const GetList = (url: string, data: any): any => request({ url: url, data, method: 'GET' })
export const GetSetting = (): any => request({ url: 'setting' })

