﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    public class ProductAttribute
    {
        /// <summary>
        /// id
        /// </summary>
        public int Id { get; set; }

		/// <summary>
		/// 属性Id
		/// </summary>

		public long AttributeId { get; set; }
		/// <summary>
		/// 属性Id
		/// </summary>

		public long AttributeItemId { get; set; }
		/// <summary>
		/// 类型 单选 多选 文本
		/// </summary>

		public AttributeType TypeId { get; set; }
		/// <summary>
		/// 值
		/// </summary>

		public string Value { get; set; }
		/// <summary>
		/// 序号
		/// </summary>

		public int Sequence { get; set; }

		/// <summary>
		/// 规格项名称
		/// </summary>
		public string Name { get; set; }

	   /// <summary>
	   /// 规格
	   /// </summary>
		public List<string> Values { get; set; }
}
}
