﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions.Assembly.SqlSugar;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;

namespace Wq.Core.ProductService
{
    /// <summary>
    /// 商品品牌
    /// </summary>
    public class BrandService : BaseRepository<BrandInfo>, IRegister
    {
        /// <summary>
        /// 品牌列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public List<BrandView> List(PageModel<BrandQuery> pageModel, ref int count)
        {
            return Context.Queryable<BrandInfo>()
                 .WhereIF(!SqlFunc.IsNullOrEmpty(pageModel.Entity.Name), p => p.Name.Contains(pageModel.Entity.Name))
                 .Select<BrandView>()
                 .OrderBy(pageModel.GetOrderbyString())
                 .ToPageList(pageModel.Page.Index, pageModel.Page.Size, ref count);
        }

        /// <summary>
        /// 修改排序
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Sequence"></param>
        public void UpdateSequence(long id, long Sequence) => Context.Updateable<BrandInfo>().SetColumns(p => p.Sequence == Sequence).Where(p => p.Id == id).ExecuteCommand();
    }
}
