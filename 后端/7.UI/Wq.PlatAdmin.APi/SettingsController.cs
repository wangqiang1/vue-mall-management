﻿using System;
using System.Collections.Generic;
using Framework.Base;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Wq.Core.Application.Manager;
using Wq.Core.Extensions;
using Wq.Core.Model;
using Wq.Core.PlatAdminService.Models;
using Wq.Core.Extensions.Assembly.Jwt;
using Framework;
using Wq.Core.Application;

namespace Wq.PlatAdmin.APi
{
    /// <summary>
    /// 系统配置 
    /// </summary>
    [Route("admin")]
    public class SettingsController : PlatAdminController
    {
        private readonly SettingsApplication settingsApplication;
        /// <summary>
        /// 系统配置
        /// </summary>
        /// <param name="settingsApplication"></param>

        public SettingsController(SettingsApplication settingsApplication) => this.settingsApplication = settingsApplication;
        
        /// <summary>
        /// 获取站点配置
        /// </summary>
        /// <returns></returns>

        [HttpGet("setting")]
        public MessageModel GetSetting() => settingsApplication.GetSetting();


    }


}
