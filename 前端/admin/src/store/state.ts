export interface State {
  token: string //当前koken
  refreshToken: string//刷新token凭证
  menus: any //全部路由
  loading: boolean //显示加载
  currentPage: '', //当前页面
  userName:'',//用户名
  setting:{}
}
const token = localStorage.token || ''
const refreshToken = localStorage.refreshToken || ''
const resetTime = localStorage.resetTime || ''
const currentPage = localStorage.currentPage || ''
const userName = localStorage.userName || ''
const state: State = {
  token: token,
  refreshToken: refreshToken,
  menus: [],
  loading: false,
  currentPage: currentPage,
  userName :userName,
  setting:{}
}

export default state
