﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.ProductService.Models
{
    public class SpecView
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 规格名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 别名
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// 展示形式
        /// </summary>
        public SpecType TypeId { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public List<SpecValue> Values { get; set; }
        
        /// <summary>
        /// 规格值
        /// </summary>
        public class SpecValue
        { 
            /// <summary>
            /// Id
            /// </summary>
            public long Id  { get; set; }   

            /// <summary>
            /// 值
            /// </summary>
            public string Value { get; set; }
        }
    }
}
