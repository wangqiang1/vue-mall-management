﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.FileService.Models.DTO
{
    /// <summary>
    /// 上传图片类
    /// </summary>
    public class UploadImageCommand
    {
        /// <summary>
        /// 文件
        /// </summary>
        public IFormFile File { get; set; }

        /// <summary>
        /// 分类
        /// </summary>
        public int Category { get; set; }
    }
}
