﻿using Framework.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Framework
{
    /// <summary>
    /// 商品前端接口
    /// </summary>
    [ApiExplorerSettings(GroupName = "product")]
    public class ProductController : BaseController
    {
    }
}
