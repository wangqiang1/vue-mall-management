﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Extensions.Assembly.SqlSugar;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;

namespace Wq.Core.ProductService
{
    /// <summary>
    /// 商品绑定的规格
    /// </summary>
    public class ProductSpecService : BaseRepository<SkuSpecInfo>, IRegister
    {

        /// <summary>
        /// 判断是否还存在对于规格的商品 
        /// </summary>
        /// <param name="specId"></param>
        /// <returns></returns>
        public bool ExistBySpec(long specId)
        {
           var info =  Context.Queryable<SkuSpecInfo>().First(p => p.SpecId == specId);
            if (info != null)
                return Context.Queryable<ProductInfo>().Count(p => p.Id == info.ProductId && p.IsDelete == false) > 0;
            return false;
        }

        /// <summary>
        /// 判断是否还存在对于规格值的商品
        /// </summary>
        /// <param name="specId"></param>
        /// <returns></returns>
        public bool ExistBySpecValue(long specValueId)
        {
            var info = Context.Queryable<SkuSpecInfo>().First(p => p.SpecValueId == specValueId);
            if (info != null)
                return Context.Queryable<ProductInfo>().Count(p => p.Id == info.ProductId && p.IsDelete == false) > 0;
            return false;
        }
    }
}
