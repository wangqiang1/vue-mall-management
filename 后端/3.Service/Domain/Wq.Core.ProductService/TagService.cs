﻿
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.Extensions.Assembly.SqlSugar;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;
using CacheKey = Wq.Core.Model.CacheKey;

namespace Wq.Core.ProductService
{
    /// <summary>
    /// 商品标签 
    /// </summary>
    public class TagService : BaseRepository<TagInfo>, IRegister
    {

        private readonly BaseRepository redisRepository;

        public TagService(BaseRepository redisRepository) => this.redisRepository = redisRepository;


        /// <summary>
        /// 编辑标签
        /// </summary>
        /// <param name="tag"></param>
        public void Save(TagEditor tag) => Context.Updateable<TagInfo>().SetColumns(p => p.Name == tag.Name).Where(p => p.Id == tag.Id).ExecuteCommand();
       

        /// <summary>
        /// 标签列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public List<TagView> List(PageModel<TagQuery> pageModel, ref int count) =>
            Context.Queryable<TagInfo>()
            .WhereIF(SqlFunc.IsNullOrEmpty(pageModel.Entity.Name), p => p.Name.Contains(pageModel.Entity.Name))
            .Mapper(p => { })
            .Select<TagView>()
            .OrderBy(pageModel.GetOrderbyString())
            .ToPageList(pageModel.Page.Index, pageModel.Page.Size, ref count);

        /// <summary>
        /// 全部标签
        /// </summary>
        /// <returns></returns>
        public List<TagView> Tags() => redisRepository.Get(CacheKey.Tags, () => Context.Queryable<TagInfo>().OrderBy(p => p.Sequence, SqlSugar.OrderByType.Desc).Select<TagView>().ToList()).Result;

        /// <summary>
        /// 修改排序
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Sequence"></param>
        public void UpdateSequence(long id, long Sequence) => Context.Updateable<TagInfo>().SetColumns(p => p.Sequence == Sequence).Where(p => p.Id == id).ExecuteCommand();

        /// <summary>
        /// 移除缓存
        /// </summary>
        public void RemoveCache() => redisRepository.Remove(CacheKey.Tags);
    }
}
