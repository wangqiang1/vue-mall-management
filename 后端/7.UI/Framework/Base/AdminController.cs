﻿using Microsoft.AspNetCore.Mvc;
using SqlSugar.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Extensions.Assembly.Jwt;
using Wq.Core.Model;

namespace Framework.Base
{
    public class AdminController : BaseController
    {
        /// <summary>
        /// 管理员Id
        /// </summary>
        public long CurrentManager => JWTHelper.SerializeJwt(HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer ", "")).UserId.ObjToInt();

    }
}
