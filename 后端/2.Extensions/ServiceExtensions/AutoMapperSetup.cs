﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Common.Helper;
using Wq.Core.Extensions.Assembly.AutoMapper;
namespace Wq.Core.Extensions.ServiceExtensions
{
    /// <summary>
    /// Automapper 启动服务
    /// </summary>
    public static class AutoMapperSetup
    {
        public static void AddAutoMapperSetup(this IServiceCollection services)
        {
            var profiles = RuntimeHelper.GetProjectTypes()
                 .Where(p => !p.IsInterface && !p.IsAbstract && !p.IsSealed)
                 .Where(p => typeof(Profile).IsAssignableFrom(p))
                 .ToList();

            var expriseeion = new MapperConfiguration(config =>
            {
                foreach (var profile in profiles)
                    config.AddProfile(profile);
            });
            var mapper = new Mapper(expriseeion);
            AutoMapperHelper.SetMapper(mapper);
            services.AddSingleton<IMapper>(mapper);
        }
    }
}
