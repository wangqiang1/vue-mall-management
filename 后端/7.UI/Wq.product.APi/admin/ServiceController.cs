﻿using Framework;
using Microsoft.AspNetCore.Mvc;
using Wq.Core.Application.Product;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;

namespace Wq.product.APi.admin
{
    /// <summary>
    /// 商品服务
    /// </summary>
    [Route("admin/product/service")]
    public class ServiceController : ProductAdminController
    {
        private readonly ServiceApplication application;
        /// <summary>
        /// 商品服务
        /// </summary>
        public ServiceController(ServiceApplication application)
        {
            this.application = application;
        }

        /// <summary>
        /// 商品标签列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        [HttpPost("list")]
        public MessageModel List(PageModel<ServiceQuery> pageModel) => application.List(pageModel);

        /// <summary>
        /// 添加商品标签
        /// </summary>
        /// <param name="tag">标签</param>
        /// <returns></returns>
        [HttpPost("save")]
        public MessageModel Save(ServiceEditor tag) => application.Save(tag);

        /// <summary>
        /// 删除商品标签
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("remove")]
        public MessageModel Remove(long id) => application.Remove(id);

        /// <summary>
        /// 获取全部服务
        /// </summary>
        /// <returns></returns>
        [HttpGet("services")]
        public MessageModel Services() => application.Services();
    }
}
