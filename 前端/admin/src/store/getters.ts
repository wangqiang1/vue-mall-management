import { GetterTree } from "vuex";
import { State } from './state'

const getters: GetterTree<State, any> = {
  token(state: any): void { return state.token },
  userName(state: any): void { return state.userName },
  refreshToken(state: any): void { return state.refreshToken },
  resetTime(state: any): void { return state.resetTime },
  menus(state: any): void { return state.menus },
  loading(state: any): void { return state.loading },
  currentPage(state: any): void { return state.currentPage },
  setting(state: any): void { return state.setting },
}
export default getters