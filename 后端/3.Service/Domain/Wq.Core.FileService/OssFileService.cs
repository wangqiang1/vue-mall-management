﻿using SqlSugar;
using System;
using System.Collections.Generic;
using Wq.Core.Extensions.Assembly.AutoMapper;
using Wq.Core.Extensions.Assembly.Redis;
using Wq.Core.Extensions.Assembly.SqlSugar;
using Wq.Core.Extensions.ServiceExtensions.Framework;
using Wq.Core.FileService.Models;
using Wq.Core.Model;
using CacheKey = Wq.Core.Model.CacheKey;

namespace Wq.Core.FileService
{
    public class OssFileService : BaseRepository<FileInfo>, IRegister
    {

        private readonly BaseRepository redisRepository;
        public OssFileService(BaseRepository redisRepository)
        {
            this.redisRepository = redisRepository;
        }
        /// <summary>
        /// 获取分页列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public List<FileView> List(PageModel<FileQuery> pageModel, ref int count) =>
                 Context.Queryable<FileInfo>()
                 .Where(p => p.Type == (int)pageModel.Entity.Type)
                 .Where(p => p.CategoryId == pageModel.Entity.CategoryId)
                 .WhereIF(!SqlFunc.IsNullOrEmpty(pageModel.Entity.Name), p => p.Name.Contains(pageModel.Entity.Name))
                 .Select<FileView>().ToPageList(pageModel.Page.Index, pageModel.Page.Size, ref count);

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="file"></param>

        public void Create(FileEditor file)
        {
            Context.Ado.UseTran(() => {
                var info = file.Map<Wq.Core.FileService.Models.FileInfo>();
                info.CreateTime = DateTime.Now;
                Context.Insertable(info).ExecuteCommand();
                RemoveCache(file.Type);

            });
           
        }

        /// <summary>
        /// 更新文件名称
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public void SetName(long id, string name) => Context.Updateable<FileInfo>().SetColumns(p => p.Name == name).Where(p => p.Id == id).ExecuteCommand();


        /// <summary>
        /// 批量分组
        /// </summary>
        /// <param name="edior"></param>
        public void SetGroup(BatchEdior edior)
        {
            Context.Updateable<FileInfo>().SetColumns(p => p.CategoryId == edior.CategoryId).Where(p => edior.Ids.Contains(p.Id)).ExecuteCommand();
            RemoveCache(edior.Type);
        }

        /// <summary>
        /// 移除
        /// </summary>
        /// <param name="id"></param>
        public void Remove(BatchEdior edior)
        {
            Context.Deleteable<FileInfo>().Where(p => edior.Ids.Contains(p.Id)).ExecuteCommand();
            RemoveCache(edior.Type);
        }

        /// <summary>
        /// 移除缓存
        /// </summary>
        /// <param name="type"></param>
        public void RemoveCache(FileType type) => redisRepository.Remove(CacheKey.FileCategory((int)type));



    }
}
