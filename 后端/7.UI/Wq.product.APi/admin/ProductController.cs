﻿using Framework;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Application.Product;
using Wq.Core.Model;
using Wq.Core.ProductService.Models;

namespace Wq.product.APi.admin
{
    /// <summary>
    /// 商品 
    /// </summary>
    [Route("admin/product")]
    public class ProductController : ProductAdminController
    {
        private readonly ProductApplication application;
        /// <summary>
        /// 商品
        /// </summary>
        public ProductController(ProductApplication application)
        {
          this.application = application;
        }

        /// <summary>
        /// 添加编辑商品
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPost("save")]
        public MessageModel Save(ProductEditor product) => application.Save(product);

        /// <summary>
        /// 商品列表
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        [HttpPost("list")]
        public MessageModel List(PageModel<ProductQuery> pageModel) => application.List(pageModel);

    }
}
