﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wq.Core.Model;

namespace Wq.Core.PlatAdminService.Models
{
    public enum PrivilegeDescriptor
    {

        [Menu("概览", "home", Icon = "icon-dashboard", Permission = true)] 
        [Permission(PrivilegeType.View)]
        GeneralView = 10000,

        [Menu("店铺", "shop", Icon = "icon-shop")]
        [Menu("店铺", "店铺信息", "setting", Permission = true)]
        [Permission(PrivilegeType.View, PrivilegeType.Edit)]
        SiteSetting = 20000,

        [Menu("店铺", "手机端", "mobile")]
        [Menu("店铺", "手机端", "微页面", "microPage", Permission = true)]
        [Permission(PrivilegeType.View, PrivilegeType.Add, PrivilegeType.Edit, PrivilegeType.Delete)] 
        MobileTopic = 20001,

        [Menu("店铺", "手机端", "分类页面", "category", Permission = true)]
        [Permission(PrivilegeType.View, PrivilegeType.Add, PrivilegeType.Edit, PrivilegeType.Delete)]
        MobileCategory = 20002,

        [Menu("店铺", "PC端", "pc")]
        [Menu("店铺", "PC端", "分类页面", "microPage", Permission = true)]
        [Permission(PrivilegeType.View, PrivilegeType.Add, PrivilegeType.Edit, PrivilegeType.Delete)]
        PCTopic = 20003,

        [Menu("店铺", "素材中心", "library", Permission = true)]
        [Permission(PrivilegeType.View, PrivilegeType.Add, PrivilegeType.Edit, PrivilegeType.Delete)]
        Library = 20004,

        [Menu("商品", "product", Icon = "icon-product")]
        [Menu("商品", "商品列表", "list", Permission = true)]
        [Permission(PrivilegeType.View, PrivilegeType.Add, PrivilegeType.Edit, PrivilegeType.Delete)]
        Product = 30000,
        [Menu("商品", "商品分类", "category", Permission = true)]
        [Permission(PrivilegeType.View, PrivilegeType.Add, PrivilegeType.Edit, PrivilegeType.Delete)]
        ProductCategory = 30001,

        [Menu("商品", "商品品牌", "brand", Permission = true)]
        [Permission(PrivilegeType.View, PrivilegeType.Add, PrivilegeType.Edit, PrivilegeType.Delete)]
        ProductBrand = 30002,

        [Menu("商品", "商品属性", "attribute", Permission = true)]
        [Permission(PrivilegeType.View,PrivilegeType.Add, PrivilegeType.Edit, PrivilegeType.Delete, PrivilegeType.Administer)]
        ProductAttribute = 30003,

        [Menu("商品", "商品规格", "spec", Permission = true)]
        [Permission(PrivilegeType.View, PrivilegeType.Add, PrivilegeType.Edit, PrivilegeType.Delete, PrivilegeType.Administer)]
        ProductSpec = 30004,

        [Menu("商品", "商品标签", "tag", Permission = true)]
        [Permission(PrivilegeType.View, PrivilegeType.Add, PrivilegeType.Edit, PrivilegeType.Delete)]
        ProductTag = 30005,

        [Menu("商品", "商品服务", "service", Permission = true)]
        [Permission(PrivilegeType.View, PrivilegeType.Add, PrivilegeType.Edit, PrivilegeType.Delete)]
        ServiceTag = 30006,

        [Menu("商品", "运费模板", "freight", Permission = true)]
        [Permission(PrivilegeType.View, PrivilegeType.Add, PrivilegeType.Edit, PrivilegeType.Delete)]
        FreightTemplate = 30007,

        [Menu("订单", "order", Icon = "icon-order")]
        [Menu("订单", "订单管理", "list", Permission = true)]
        [Permission(PrivilegeType.View)]
        Order = 40001,
        [Menu("订单", "售后维权", "refund", Permission = true)]
        [Permission(PrivilegeType.View)]
        Refund = 40002,

        [Menu("设置", "setting", Icon = "icon-Setting")]
        [Menu("设置", "系统设置", "system")]
        [Menu("设置", "系统设置", "角色设置", "role", Permission = true)]
        [Permission(PrivilegeType.View, PrivilegeType.Add, PrivilegeType.Edit, PrivilegeType.Delete)]
        RoleSetting = 50000,

       
        [Menu("设置", "系统设置", "管理员", "manager", Permission = true)]
        [Permission(PrivilegeType.View, PrivilegeType.Add, PrivilegeType.Edit, PrivilegeType.Delete)]
        ManagerSetting = 50001,

        [Menu("设置", "系统设置", "地区管理", "area", Permission = true)]
        [Permission(PrivilegeType.View, PrivilegeType.Add, PrivilegeType.Edit, PrivilegeType.Delete)]
        AreaSetting = 50002,

    }
}
