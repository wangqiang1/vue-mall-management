﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wq.Core.Extensions
{
    public class AccessForbiddenException : MyException
    {
        public AccessForbiddenException() : base("forbidden", "无权限访问。")
        {

        }
    }
}
